screen spiritHallway:
    add "bgs/spiritHallway.png" at right

label exploreLabel:
    if mission7 == 7:
        $ searchForHanger += 1
    if mission7 == 10:
        $ mission7 = 11
        play music "audio/music/explore.mp3"
        y "Now to find a good spot to place these explosives."
        scene bgExplore2 with fade
        play sound "audio/sfx/exploreFootsteps.mp3"
        pause 0.5
        scene black with fade
        "You spend the rest of the day placing explosives in non-vital spots in the station."
        y "............."
        y "There! That should do it!"
        y "I can trigger these by going to sector #6 and setting them off."
        y "....................."
        y "Not sure why... but I get a very 'finalé' feeling from all this...."
        jump jobReport
        
        
    play music "audio/music/explore.mp3"
    scene black with fade
    play sound "audio/sfx/exploreFootsteps.mp3"
    "You spend the rest of the day wandering the endless halls of the space station."
    $ randomExploreBG = renpy.random.randint(1, 3)
    if randomExploreBG == 1:
        scene bgExplore with fade
    if randomExploreBG == 2:
        scene bgExplore2 with fade
    if randomExploreBG == 3:
        scene bgExplore3 with fade
    
    if mission13 == 11:
        define puzzleSelect1 = 0
        define puzzleSelect2 = 0
        define puzzleSelect3 = 0
        define puzzleSelect4 = 0
        "For hours you search in vain for the lost Padawan."
        y "........................"
        y "I could bring an army down here and it would still take weeks to explore it all."
        play sound "audio/sfx/dropJaw.mp3"
        "{b}*Cling*{/b}"
        y ".....?"
        "You hear something get knocked over nearby."
        y "............................"
        menu:
            "Investigate":
                "???" "{color=#ec79a2}You will stop your search for me...{/color}"
                y "I... want to stop searching........ for the mysterious voice..."
                "???" "Yes goo-...."
                y "But.... I'll keep.... looking for Ahsoka...."
                "???" "....................."
                "???" "{color=#ec79a2}You will stop searching for Ahsoka...{/color}"
                y "I will... stop... searching for Ahsoka..."
                "???" "Good...."
                y "For today..."
                "???" "What? No! Stop searching for me!"
                y "Goodbye mysterious voice... I'll be... back... tomorrow...."
                "???" "{b}*Groans*{/b}"
                jump jobReport
            "Turn around the other corner":
                $ mission13 = 12
                "Deciding against it, you turn around another corner and keep walking."
                ".................................{w}....................................{w}............................."
                y "Nothing here..."
                y "Maybe I should've gone to investigate that sound afterall..."
                y "Hm? What's this?"
                "A large steel door blocks your passage. On the door are four switches."
                label toPuzzle:
                    if puzzleSelect1 == 1 and puzzleSelect2 == 1 and puzzleSelect3 == 1 and puzzleSelect4 == 1:
                        $ mission13 = 12
                        hide screen puzzle
                        scene bgExplore3
                        y "....................."
                        y "Hey... What's this...?"
                        play sound "audio/sfx/itemGot.mp3"
                        "You found {color=#FFE653}Ahsoka's Headdress.{/color}"
                        y "I'm on the right path."
                        "You spend the rest of the day exploring the station, but don't manage to track her down."
                        jump jobReport
                    else:
                        pass
                call screen puzzle
                with d3
                
                label puzzleSelect1:
                    $ puzzleSelect1 = 1
                    jump toPuzzle
                    
                label puzzleSelect2:
                    if puzzleSelect1 == 1:
                        $ puzzleSelect2 = 1
                    else:
                        $ puzzleSelect1 = 0
                        $ puzzleSelect2 = 0
                        $ puzzleSelect3 = 0
                        $ puzzleSelect4 = 0
                    jump toPuzzle
                    
                label puzzleSelect3:
                    if puzzleSelect1 == 1 and puzzleSelect2 == 1:
                        $ puzzleSelect3 = 1
                    else:
                        $ puzzleSelect1 = 0
                        $ puzzleSelect2 = 0
                        $ puzzleSelect3 = 0
                        $ puzzleSelect4 = 0
                    jump toPuzzle
                    
                label puzzleSelect4:
                    if puzzleSelect1 == 1 and puzzleSelect2 == 1 and puzzleSelect3 == 1:
                        $ puzzleSelect4 = 1
                    else:
                        $ puzzleSelect1 = 0
                        $ puzzleSelect2 = 0
                        $ puzzleSelect3 = 0
                        $ puzzleSelect4 = 0
                    jump toPuzzle
                    
    if mission13 == 12:
        "Motivated by your recent find, you decide to head back out into the old station."
        "You quickly make your way back to where you found the Padawan's headdress."
        y "Now then... Where to look next...?"
        label exploreDecision:
            pass
        menu:
            "Left":
                "..............................."
                play sound "audio/sfx/exploreFootsteps.mp3"
                "You hear footsteps!"
                menu:
                    "Check them out":
                        y "Ahsoka...? Is that you?"
                        show screen battle_droid1
                        with d3
                        "Lost Droid" "Roger, roger...?"
                        y "Ah!"
                        play sound "audio/sfx/blasterFire.mp3"
                        show screen scene_red
                        with d2
                        hide screen scene_red
                        with d2
                        hide screen battle_droid1
                        with d2
                        if gunslinger >= 6:
                            "You managed to shoot the lost droid before he could cause any harm."
                        else:
                            with hpunch
                            $ playerHitPoints -= 1
                            y "Ow! Crap!"
                            "Just before you destroy the droid, he managed to hit you."
                            hide screen battle_droid1
                            with d3
                        "Unfortunately, you lost any trace of Ahsoka and decide to turn back for the night."
                        jump jobReport
                    "Explore a different hallway":
                        "You backtrace your steps."
                        jump exploreDecision
            "Right":
                "..............................."
                y "...?"
                "There's somebody here!"
                menu:
                    "Call out":
                        y "Whoever you are, I can tell that you're here."
                        show screen shin_main
                        with d3
                        y "Shin?"
                        s "{b}*Yawns*{/b} Master...?"
                        y "I thought we were taking shifts searching for Ahsoka."
                        s "We are,,, Wait... how long have I been down here...?"
                        hide screen shin_main
                        with d3
                        "You send Shin back up to bed. Try as you might, you don't manage to find any more clues of Ahsoka's whereabouts."
                        jump jobReport
                    "Fire blindly into the darkness":
                        y "AHHHHHH!"
                        play sound "audio/sfx/blasterFire.mp3"
                        show screen scene_red
                        with d2
                        hide screen scene_red
                        with d2
                        show screen scene_red
                        with d2
                        hide screen scene_red
                        with d2
                        "???" "{b}*Eep!*{/b}"
                        "You see a lightsaber switch on as the blaster shoots are deflected back at you!"
                        "You manage to duck and cover just in time. As you look up, you see Shin'na standing over you."
                        show screen shin_main
                        s "Master?! Are you hurt?"
                        y "Shin? What are you doing here? I thought we were taking shifts!"
                        s "We are... Wait... how long have I been down here...?"
                        hide screen shin_main
                        with d3
                        "You send Shin back up to bed. Try as you might, you don't manage to find any more clues of Ahsoka's whereabouts."
                        jump jobReport
            "Straight":
                $ mission13 = 13
                scene bgExplore4
                with d5
                "..............................."
                "In the distance you see a faintly glowing red light."
                show screen scene_red
                with d3
                play sound "audio/sfx/horrorSoft.mp3"
                y "Uh oh..."
                "With the Force Suppressors still offline, you manage to get closer than before."
                "???" "........................................."
                y "Is anyone there...?"
                y ".............................."
                "Carefully to walk closer and closer to the light..."
                show screen spirit
                play sound "audio/sfx/horror.mp3"
                scene bgExplore3Dis
                with dHorror
                hide screen spirit
                scene black
                stop music
                hide screen scene_red
                a "[playerName]!!! {p=0.5} {nw}"
                $ renpy.pause(3.5, hard='True')
                "......................................................................................................."
                "Master...?"
                y "Ahsoka...?"
                $ shinOutfit = 1
                $ shinExpression = 31
                $ accessoriesShin = 1
                scene bgExplore3
                with d5
                pause 1.0
                show screen scene_darkening
                with d3
                pause 0.5
                show screen shin_main
                with d3
                s "Master, are you all right?!"
                y "Shin...?"
                y "Yeah... I'm okay.{w} Had another little run-in with our friend."
                $ shinExpression = 21
                s "I can sense him..."
                s "Master, we need to leave. The Dark Side... It's overwhelming..."
                s "I've never felt anything like it. It's terrifying."
                y "Wait...! I swear I could hear Ahsoka's voice before passing out."
                $ shinExpression = 13
                s "!!!"
                s "Ahsoka... {w}Is here?"
                $ shinExpression = 15
                s "............................."
                y "That's bad, isn't it?"
                s "I don't know... I've never felt anything like this before in my life."
                $ shinExpression = 14
                s "It feels... It feels like staring straight into the eyes of a predator."
                s "A feeling of imminent death."
                s "My every instinct tell me to run. Run as fast as I can."
                y ".............................."
                $ shinExpression = 21
                s "Let's return tomorrow. We know where to look now..."
                y "Will Ahsoka be okay?"
                s "I...{w} I don't know...."
                s "But we won't be much help to her in our current condition."
                "Picking yourself up, you decide to return to the upper levels of the station."
                hide screen scene_darkening
                hide screen shin_main
                with d3
                jump jobReport
                
    if mission13 == 13:
        jump mission13

    
    if 1 <= day <= 10:
        $ randomExplore = renpy.random.randint(1, 5)
        if 1 <= randomExplore <= 2:
            "....................................................."
            "You couldn't find anything interesting."
            jump endExplore
        if randomExplore == 3:
            "....................................................."
            "After exploring for a while you come into an empty room with wires hanging from the ceiling."
            y "(Nothing interesting here....)"
            play sound "audio/sfx/dropJaw.mp3"
            "{b}*Cling*{/b}"
            y "Hm?"
            "You walk over to the corner of the room where the sound came from. There's a small machine here randomly producing items."
            menu:
                "Press some buttons":
                    $ randomExploreItem = renpy.random.randint(1, 2)
                    if randomExploreItem == 1:
                        "{b}Brrrrrrrr~....{/b}"
                        "{b}*Sputter*~.... *Spark*{/b}"
                        y "Well that doesn't sound very go-..."
                        play sound "audio/sfx/explosion1.mp3"
                        with hpunch
                        y "AH!"
                        "The machine explodes and with a whining sound powers done."
                        y "I think that's enough exploring for one day..."
                        "You return to your quarters."
                        jump endExplore
                    if randomExploreItem == 2:
                        $ chocolate += 1
                        "{b}Brrrrrrrr~....{/b}"
                        "{b}Brrrrrrrr~.... *Ping*!{/b}"
                        play sound "audio/sfx/itemGot.mp3"
                        "You got {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
                        "As soon as the machine gives you the item, it sputters and shuts down."
                        y "That's all for today, I think."
                        "You return to your quarters."
                        jump endExplore
                "Take it apart for Hypermatter":
                    $ hypermatter += 15
                    "{b}*Sputter*~.... *Spark*{/b}"
                    "The machine sputters in protest as you take it apart for Hypermatter."
                    play sound "audio/sfx/itemGot.mp3"
                    "You got {color=#FFE653}15 Hypermatter{/color}!"
                    "You return to your quarters."
                    jump endExplore
                    
        if randomExplore == 4:
            "....................................................."
            "After exploring for a while you come across a small room filled with metal crates."
            menu:
                "Open up the crates":
                    $ hypermatter += 10
                    "You open up the crates."
                    play sound "audio/sfx/itemGot.mp3"
                    "Inside the crates you find: {color=#FFE653}10 Hypermatter!{/color}"
                    "You continue exploring for a while before turning back."
                    jump endExplore
                "Leave the crates alone":
                    "You decided to leave the crates alone."
                    "..............................................................."
                    "You couldn't find anything else interesting and decided to head back."
                    jump endExplore
        if randomExplore == 5:
            "....................................................."
            "After walking around for a while, you come across a room with droids."
            show screen droid_main
            with d5
            pause 0.5
            y "Oh! Sorry, I didn't know you guys were working.... {w}here....?"
            "On closer inspection you see that all of them are frozen in place."
            y "Well that's strange....."
            "Walking closer to the droids you feel a faint buzzing rising back in the back of your mind."
            y "Perhaps I should leave these be for now...."
            hide screen droid_main
            with d3
            "You decide to try and find your way back."
            jump endExplore
            
            
    ########################## Triggered Explore Events ###############################
    
    define encounterSpirit = True
    if day >= 20 and encounterSpirit == True and mission7 == 0:
        $ encounterSpirit = False
        "....................................................."
        jump mission7            
    
    if holocronActive == False and ahsokaSlut >= 25:
        $ holocronActive = True
        "....................................................."
        "After exploring for a while you come large empty room filled with beds."
        show screen scene_darkening
        with d3
        y "Another medbay?"
        y "No wait...."
        "Carefully you step into the room as you notice a pile of clothes lying in the corner of the room."
        "Picking up one of the shirts, you notice it has a hole burned through it. So does the next... and the next."
        "You quickly finish with the pile of roasted clothes and concluded that every piece of fabric has a similair hole in it."
        y "Well that's strange...."
        y ".....?"
        "You spot a robe lying on the floor. It seems to be in okay condition."
        y "What's this...?"
        play sound "audio/sfx/itemGot.mp3"
        "Inside the robe you find: {color=#FFE653}LOCKED HOLOCRON!{/color}"
        y "Well this is interesting."
        y "Let's see if we can crack this baby open!"
        y "Ngh~.......{w}................{w} Ngh!!" with hpunch
        "No matter how hard you tug and pull on it. The Holocron doesn't budge."
        y "Phew.... tough little box."
        y "I'll keep it on me for now. Might come in handy in the future."
        hide screen scene_darkening
        with d5
        "With holocron in hand you begin to make your way back to the upper levels."
        jump endExplore
        
    if mission11 == 1 and shinActive == True:
        jump mission11
        
    ########################## Random Explore Events ###############################
        
    else:
        $ randomExplore = renpy.random.randint(1, 15)
        if randomExplore == 1:
            "After a while you come to a fork in the road."
            menu:
                "Go right":
                    "You decide to head right and spend the next few hours roaming the empty halls of the Station when finally you come across a dead end."
                    y "Well that was a waste of time..... It's getting late. I'd best head back......"
                    y "................"
                    y "Where did I come from again......?"
                    "You decide to turn around and try to find your way back."
                    jump endExplore
                "Go left":
                    "You decide to head left until you come across a old and worn down robot."
                    "Droid" "...................~"
                    y "Are you operational?"
                    "Droid" "...................~"
                    "The droid doesn't seem to respond."
                    menu:
                        "Salvage droid for parts.":
                            y "Well you won't be needing this anymore."
                            $ randomHypermatter = renpy.random.randint(10, 20)
                            $ hypermatter += randomHypermatter
                            play sound "audio/sfx/itemGot.mp3"
                            "You salvaged {color=#FFE653}[randomHypermatter] hypermatter{/color} from the droid as it slums down and goes offline."
                            "After salvaging the robot, you begin making your way back to the upper levels and turn in for the night."
                            jump endExplore
                        "Leave it alone":
                            y "That thing sounds like it may explode at any minute. Best to leave it alone."
                            "The rest of your journey is uneventful.."
                            jump endExplore
        if randomExplore == 2:
            "As you pass through the abandoned halls you see the husks of long powered down droids. Their corpse like figures slumped over and silently staring off into the distance."
            "After walking for a while you suddenly see a door open as you pass by......"
            "You enter into the room as you see a machine flicker to life. It seems to be a varient on the replicators that you've come to know from the Foundry."
            "A label on the machine reads: 'Insert Command'."
            menu:
                "Craft a gift for Ahsoka":
                    label randomSFGift:
                        $ randomGift = renpy.random.randint(1, 13)
                        if randomGift == 1:
                            if colar1Active == False:
                                $ colar1Active = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find:{color=#FFE653}AHSOKA SLAVE COLAR #1!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 2:
                            if colar2Active == False:
                                $ colar2Active = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}AHSOKA SLAVE COLAR #2!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 3:
                            if colar3Active == False:
                                $ colar3Active = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}AHSOKA SLAVE COLAR #3!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 4:
                            if colar4Active == False:
                                $ colar4Active = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}AHSOKA SLAVE COLAR #4!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                        if randomGift == 5:
                            if plushieBantha == False:
                                $ plushieToy = 1
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}GIANT PLUSHIE BANTHA!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 6:
                            if republicPosters == False:
                                $ republicPosters = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}REPUBLIC PROPAGANDA POSTERS!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 7:
                            jump randomSFGift
                                
                        if randomGift == 8:
                            if dancingPole == False:
                                $ dancingPole = True
                                "You insert the command into the machine as it begins to sputter."
                                play sound "audio/sfx/itemGot.mp3"
                                "After a moment the machine opens and inside you find: {color=#FFE653}DANCING POLE!{/color}"
                                "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                                "Perhaps you should come back some other time."
                            else:
                                jump randomSFGift
                                
                        if randomGift == 9:
                            "You insert a command into the machine as it begins to sputter."
                            $ chocolate += 1
                            play sound "audio/sfx/itemGot.mp3"
                            "After a moment the machine opens and inside you find: {color=#FFE653}REPUBLIC SPICED CHOCOLATE!{/color}"
                            "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                            "Perhaps you should come back some other time."
                            
                        if randomGift == 10:
                            "You insert a command into the machine as it begins to sputter."
                            $ actionFigure += 1
                            play sound "audio/sfx/itemGot.mp3"
                            "After a moment the machine opens and inside you find: {color=#FFE653}ASTROMECH DROID ACTION FIGURE!{/color}"
                            "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                            "Perhaps you should come back some other time."
                            
                        if randomGift == 11:
                            "You insert a command into the machine as it begins to sputter."
                            $ holotape += 1
                            play sound "audio/sfx/itemGot.mp3"
                            "After a moment the machine opens and inside you find: {color=#FFE653}HOLOTAPE!{/color}"
                            "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                            "Perhaps you should come back some other time."
                            
                        if randomGift == 12:
                            "You insert a command into the machine as it begins to sputter."
                            $ wine += 1
                            play sound "audio/sfx/itemGot.mp3"
                            "After a moment the machine opens and inside you find: {color=#FFE653}Zelosian WINE!{/color}"
                            "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                            "Perhaps you should come back some other time."
                            
                        if randomGift == 13:
                            "You insert a command into the machine as it begins to sputter."
                            $ aurinTailActive = True
                            show screen itemBackground
                            with d5
                            show screen acc04Tail
                            with d5
                            play sound "audio/sfx/itemGot.mp3"
                            "After a moment the machine opens and inside you find: {color=#FFE653}AURIN TAIL Buttplug{/color}!"
                            hide screen itemBackground
                            hide screen acc04Tail
                            with d3
                            "Once the machine is done working it switches itself off. The panel reads 'Recharging'."
                            "Perhaps you should come back some other time."
                            
                    "You make your way out of the room and start heading back to your quarters."
                    jump endExplore
                "Ask it to give up some Hypermatter":
                    $ hypermatter += 15
                    "{b}Rgnnnnnn~.....{/b}"
                    "{b}*Ping!*{/b}"
                    "A tray opens with a small cube of Hypermatter."
                    play sound "audio/sfx/itemGot.mp3"
                    "You got {color=#FFE653}15 Hypermatter{/color}!"
                    "You make your way out of the room and start heading back to your quarters."
                    jump endExplore
                "Ignore it and head back":
                    "Hoping to avoid trouble you decide to call it a day and return to your room."
                    jump endExplore
                    
        if randomExplore == 3:
            if productionBonus <= 3:
                $ productionBonus += 1
                "For hours on end you stumble through the darkness and just when you're about to head back to the hub you notice a large empty hall."
                "At first you believe it to be a simple docking bay, but as you look up you see hundreds of tentacle like robotis arms hanging from the ceiling."
                "You've stumbled upon another Foundry!"
                play sound "audio/sfx/itemGot.mp3"
                "Productivity of the Foundry has increased."
                "Satisfied with your results you decide to head back."
                jump endExplore
            else:
                jump randomSFGift
            
        if randomExplore == 4:
            "You spend hours digging through abandoned junk and avoiding pitfalls."
            "Eventually you come across a communication terminal. The display reading: 'Send Message'?"
            menu:
                "Boost Influence":
                    $ influence += 2
                    "You put the word out there that you are building a smuggler haven."
                    "Your influence was boosted slightly."
                    "Wrapping things up, you head back to your quarters."
                    jump endExplore
                "Order Pizza":
                    $ pizza += 1
                    "You ordered a pizza!"
                    "................................"
                    y "Oh right... I'm in the middle of unexplored space."
                    y "........."
                    "Your stomach rumbles."
                    "Leaving the machine alone, you continue exploring. After a couple of hours you return back to your quarters."
                    scene bgBedroom with fade
                    pause 1.0
                    "{b}*Ding Dong*{/b}"
                    y "We have a doorbell?"
                    "In the distance you hear an airlock opening."
                    "Moments later Mr. Jason appears."
                    mr "Your pizza has arrived sir."
                    y "!!!"
                    play sound "audio/sfx/itemGot.mp3"
                    "You got {color=#FFE653} Pizza Hutt{/color} x1!"
                    jump endExplore
                "Leave it alone":
                    "You turn away from the console and continue exploring."
                    "After a couple of hours you return to your quarters empty handed."
                    jump endExplore
            
        if randomExplore == 5:
            "After a while you see something blinking in the distance."
            "Upon closer inspection it appears to be a....{w} Vending Machine?"
            y "'Stasis Chips, the chips that stay good all century long.'"
            "You see that one of the bags of chips is stuck half way through the machine."
            menu:
                "Shake the vending machine":
                    $ foundryItem8 = "Stasis Chips - 30 Hypermatter"
                    $ chips += 1
                    y "Free chips!"
                    "You begin to shake and bonk on the machine."
                    play sound "audio/sfx/itemGot.mp3"
                    "You received: {color=#FFE653}STASIS CHIPS!{/color}"
                    "With chips in hand you return to the upper levels and look for your room."
                    jump endExplore
                "Leave it alone":
                    "You decide to leave well enough alone."
                    "It's been a long day of exploring and you decide to head back."
                    jump endExplore
            
        if randomExplore == 6:
            if shipStatus == 4:
                scene bgExplore
                "You find a long staircase heading down into the Station and decide to follow it."
                "At the end you find a number of large open rooms. They appear to be foundries, but they've long been destroyed."
                "Disappointed you begin making your way back until something catches your eye."
                "It appears to be some kind of powerful hyperdrive generator. It is in perfect condition!"
                "You hurry your way back up to the upper levels and visit the Foundry."
                scene black with fade
                play sound "audio/sfx/construction1.mp3"
                pause 2.0
                scene bgExplore with fade
                pause 0.5
                show screen itemBackground
                with d5
                show screen playerShip
                with d5
                pause 1.0
                $ shipStatus = 5
                with d5
                pause
                "With some help from the nearby repair droids, you manage to install the Hyperdrive on your ship."
                y "This should greatly improve my success chances during smuggling missions!"
                play sound "audio/sfx/questComplete.mp3"
                "Your ship is now fully upgraded!"
                hide screen itemBackground
                hide screen playerShip
                with d3
                jump endExplore
            else:
                "You spend the rest of the day wandering the endless halls of the Station."
                "You couldn't find anything interesting."
                jump endExplore
                
            
        if randomExplore == 7:
            if warTrophy == 0:
                $ mission3 = 1
                $ warTrophy = 1
                "After exploring for a while, you enter into a small dark room. It appears to be an old ruined visitor's lodge of some sorts."
                "The room is too covered in scrap to make out who may have lived here, but you notice a footlocker pushed off to the side."
                play sound "audio/sfx/explosion1.mp3"
                with hpunch
                stop music fadeout 3.0
                pause 0.5
                play music "audio/music/action1.mp3" fadein 2.0
                "Suddenly you hear a loud explosion followed by an alarm!"
                y "(That can't be good....)"
                "Alarm" "'Warning! Hull breach detected!'"
                y "!!!"
                "Without a second thought, you race towards  the locker and pull it open!"
                "Without looking you pull out one of the items and speed towards the exit of the room!"
                "Behind you, you can hear the oxygen being sucked out into space as you rush outside!"
                "Closing and locking the door behind you, you hear the sound of metal being torn apart on the other side."
                "Whatever was left in that room is gone now. {w}You examine the item you pulled from the locker and notice that it's a short vibroblade."
                stop music fadeout 5.0
                y "I risked my life for this?"
                y "I'll keep it with me, who knows. Someone might have use for it."
                play sound "audio/sfx/quest.mp3"
                "{color=#f6b60a}Mission: 'Relics of the Past' (Stage I){/color}"
                $ mission3PlaceholderText = "You found some sort of ancient vibroblade. Though you don't know who'd be interested in this."
                "With blade in hand you return to the upper levels of the station and head to your room."
                jump endExplore
            else:
                "You spend the rest of the day wandering the endless halls of the Station."
                "You couldn't find anything interesting."
                jump endExplore
            
        if randomExplore == 8:
            if tattoo2 == False:
                scene bgExplore with fade
                $ tattoo2 = True               #located in UIitems.rpy > Inventory
                "You walk for a while until your foot bumps into something."
                y "What's this...?"
                play sound "audio/sfx/itemGot.mp3"
                "You found {color=#FFE653}Tattoo Pattern #2{/color}!"
                "Use the Surgery Table to change Ahsoka's body Tattoos."
                "You put the pattern away and make your way back to your room."
                jump endExplore
            else:
                "You spend the rest of the day wandering the endless halls of the Station."
                "You couldn't find anything interesting."
                jump endExplore
            
        if randomExplore == 9:
            "After walking for a few hours you enter a dark room."
            "You can barely make out what the room is used for, but you spot a number of faint glowing vials on a table."
            y "Looks like a chemistry lab of sorts."
            menu:
                "Grab a vial":
                    $ vialRandom = renpy.random.randint(1, 3) #Kolto = health, gizka = potencyPotion, liquid happiness = happiness boost
                    if vialRandom == 1:
                        $ greenVial += 1
                        "You pick up the vial with the green glowing liquid. There is a note pasted on it saying: 'Artificial Happiness'."
                        play sound "audio/sfx/itemGot.mp3"
                        "You found {color=#FFE653}Green Glowing Vial{/color} x1!"
                    if vialRandom == 2:
                        $ potencyPotion += 1
                        "You pick up the vial with the red glowing liquid. There is a note pasted on it saying: '''Gizka Pheromone'."
                        play sound "audio/sfx/itemGot.mp3"
                        "You found {color=#FFE653}Red Glowing Vial{/color} x1!"
                    if vialRandom == 3:
                        $ purpleVial += 1
                        "You pick up the vial with the purple glowing liquid. There is a note pasted on it saying: 'Kolto Extract'."
                        play sound "audio/sfx/itemGot.mp3"
                        "You found {color=#FFE653}Purple Glowing Vial{/color} x1!"
                "Leave it alone":
                    "Not wanting to mess with strange chemicals, you choose to leave the room again."
            "The day ends and you head back to your quarters."
            jump endExplore
            
        if 10 <= randomExplore <= 15:
            "....................................................."
            "You couldn't find anything interesting."
            jump endExplore
            
label endExplore:
    if mission7 == 7 and searchForHanger <= 2:
        "Despite looking around, you couldn't find the hangerbay."
    if mission7 == 7 and searchForHanger == 3:
        jump mission7
    jump jobReport
