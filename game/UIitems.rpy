#####################################################################################################################################
############################# HERE YOU WILL FIND ALL THE ITEMS THAT CAN APPEAR IN YOUR INVENTORY ##################################
#####################################################################################################################################


############################# INVENTORY DEFINES ##################################
#Consumable items
define chocolate = 0                  # +5 mood              x  
define pizza = 0                         # +10 mood            x
define actionFigure = 0              # +10 mood            x
define plug = 0                          # +15 mood             x
define holotape = 0                    # +20 mood  
define cards = 0                          # +25 mood  
define magazine = 0                # +20 mood              x
define wine = 0                        # +35 mood               x 
define chips = 0                         # +5 mood               x 
define deathSticks = 0              # +45 mood              x  
define artifact = 0                      # +50 mood                                                 
define art = 0                             # +40 mood
define gum = 0                             # +5 mood
define icecream = 0                 # Player item, restores full HP
define potencyPotion = 0         #Cum more
define greenVial = 0                #Happiness more
define blueVial = 0                   #Health
define cookies = 0                      # +5 mood
define bird = 0                         # mood + 35
define plushieToy = 0

# Halloween - Note halloween outfits can be found in script.rpy
define TreatBag = 0

# Christmas
define candyCane = 0
define candyCaneEvil = 0
define christmasPresent = 0
define coal = 0
define ahsokaXmasHat = False

#define minor cosmetic items
define colar1Active = False                             #From pirate kidnapping event
define colar2Active = False                             #Random replicator while Exploring
define colar3Active = False                             #Random replicator while Exploring
define colar4Active = False                              #Random replicator while Exploring
define gagActive = False                                 #Given by Nemtak
define blindFoldActive = False                        # Blindfolds for all girls
define aurinTailActive = False                         # Wildstar tails
define gag = 0
define bwStockingsAhs = False
define buttPlugActive = False

#define body modifications Ahsoka
define modHalloweenLekkuActive = False
define modXmasLekkuActive = False

# Kit minor cosmetics
define kitHatActive = False                             # Kit Halloween hat
define kitXmasHat = False

# Shin minor cosmetics
define shinHalloweenMaskActive = False     # Shin Halloween mask
define shinXmasHatActive = False                  # Shin Xmas Hat

#TRAINING MANUALS
define training101Active = False
define training102Active = False
define training103Active = False
define training105Active = False

#Unlockable holovids
define ladyHolovid1 = False
define ladyHolovid2 = False
define ladyHolovid3 = False

#SKILL BOOKS
define skillBook1Active = True
define skillBook2Active = True          # Gunslinger raised to 6
define skillBook3Active = True          # Gunslinger raised to 10

#define ahsoka room items
define republicPosters = False
define magazineDecor = False            
define wallMountedDildo = False   
define dancingPole = False
define cuffs = False
define targetingDroid = False
define convorPet = False
define plushieBantha = False                           #After gifting once, turns true and unlocks night spy event

#define clothes
define gearUnderwearActive = True
define underwear2Active = False
define gearBasicActive = True
define gearPJActive = False                             #Got at the start of the 'I know' quest line.
define gearSeason3Active = False                 # Got towards the end of the game.
define gearFoodUniformActive = False           #Job 1 - Free
define gearSlaveActive = False                      #Job 2 - 60 hypermatter
define gearPitUniformActive = False              #Job 3 - 150 hypermatter
define gearEscortDressActive = False           #Job 4 - 500 hypermatter
define gearArmorActive = False                     #Black market outfit - 500 to 550
define gearPirateOutfitActive = False             #Black market outfit
define gearChristmasActive = False              #Christmas ahsoka outfit
define gearHalloweenActive = False              #Halloween bikini ahsoka
define gearTrooper = False                            #Clone Trooper armor, earned by clearing CIS Raids
define haremOutfit = False                             #Unlocked by building a harem room
define gearHalloween2AhsokaActive = False            # Halloween 2 Ahsoka outfit
define communityShinActive = False              # slave dress Shin
define communityKitActive = False                 # Slave dress Kit

#Shin outfits
define gearChristmasShinActive = False       #Christmas Shin
define gearHalloweenShinActive = False       #Halloween bikini Shin

#Kit outfits
define gearChristmasKitActive = False       #Christmas Kit
define gearHalloweenKitActive = False       #Halloween bikini Kit

#define body mods
define tattoo1 = True                                     #Standard skin tattoo's
define tattoo2 = False
define tattoo3 = False
define tattoo4 = False                                  # Halloween Tattoo's
define tattoo5 = False                                  # Christmas Tattoo's

#misc. items
define greenVial = 0
define redVial = 0
define purpleVial = 0
define healthStimm = 1
define holocronActive = False
define bugSpray = False
define mansual3Unlock = False

############################################################
###################### INVENTORY ALL #######################
############################################################

screen invAll:
    ####################### Display ITEMS if they are true #######################
    
    if lootedLightsaber == True and inventoryStageSelect == 0:                                          # location #1
        vbox xpos 20 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/item01LightsaberThumb.png"
                hover "UI/items/thumbnails/item01LightsaberThumb-hover.png"
                action Show("menuLightsaberSelect")
                
    if wine >= 1 and inventoryStageSelect == 0:                                                                 # location #2
        vbox xpos 135 ypos 78:
            imagebutton:
                idle "UI/items/thumbnails/item02wineThumb.png"
                hover "UI/items/thumbnails/item02wineThumb-hover.png"
                action Show("menuWineSelect")
        hbox:
            spacing 10 xpos 226 ypos 158
            text "{size=-1}{color=#FFE653}[wine]{/color}{/size}"
                
    if pizza >= 1 and inventoryStageSelect == 0:                                                                # location #3
        vbox xpos 253 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/item03PizzaHuttThumb.png"
                hover "UI/items/thumbnails/item03PizzaHuttThumb-hover.png"
                action Show("menuPizzaSelect")
        hbox:
            spacing 10 xpos 339 ypos 160
            text "{size=-1}{color=#FFE653}[pizza]{/color}{/size}"
                
    if plug >= 1 and inventoryStageSelect == 0:                                                                # location #4
        vbox xpos 363 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/item05ButtPlugThumb.png"
                hover "UI/items/thumbnails/item05ButtPlugThumb-hover.png"
                action Show("menuPlugSelect")
        hbox:
            spacing 10 xpos 453 ypos 160
            text "{size=-1}{color=#FFE653}[plug]{/color}{/size}"
                
    if magazine >= 1 and inventoryStageSelect == 0:                                                       # location #5
        vbox xpos 20 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item05MagazineThumb.png"
                hover "UI/items/thumbnails/item05MagazineThumb-hover.png"
                action Show("menuMagazineSelect")
        hbox:
            spacing 10 xpos 110 ypos 270
            text "{size=-1}{color=#FFE653}[magazine]{/color}{/size}"
                
    if icecream >= 1 and inventoryStageSelect == 0:                                                       # location #6
        vbox xpos 135 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item06IcecreamThumb.png"
                hover "UI/items/thumbnails/item06IcecreamThumb-hover.png"
                action Show("menuIcecreamSelect")
        hbox:
            spacing 10 xpos 225 ypos 270
            text "{size=-1}{color=#FFE653}[icecream]{/color}{/size}"
                
    if chips >= 1 and inventoryStageSelect == 0:                                                                # location #7
        vbox xpos 253 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item07ChipsThumb.png"
                hover "UI/items/thumbnails/item07ChipsThumb-hover.png"
                action Show("menuChipsSelect")
        hbox:
            spacing 10 xpos 340 ypos 270
            text "{size=-1}{color=#FFE653}[chips]{/color}{/size}"
                
    if cookies >= 1 and inventoryStageSelect == 0:                                                               # location #8
        vbox xpos 368 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item08CookiesThumb.png"
                hover "UI/items/thumbnails/item08CookiesThumb-hover.png"
                action Show("menuCookiesSelect")
        hbox:
            spacing 10 xpos 450 ypos 270
            text "{size=-1}{color=#FFE653}[cookies]{/color}{/size}"
                
    if gum >= 1 and inventoryStageSelect == 0:                                                               # location #9
        vbox xpos 23 ypos 302:
            imagebutton:
                idle "UI/items/thumbnails/item09GumThumb.png"
                hover "UI/items/thumbnails/item09GumThumb-hover.png"
                action Show("menuGumSelect")
        hbox:
            spacing 10 xpos 114 ypos 382
            text "{size=-1}{color=#FFE653}[gum]{/color}{/size}"
                
    if actionFigure >= 1 and inventoryStageSelect == 0:                                                          # location #10
        if randomDroid == 1:
            vbox xpos 137 ypos 302:
                imagebutton:
                    idle "UI/items/thumbnails/item10FigureThumb.png"
                    hover "UI/items/thumbnails/item10FigureThumb-hover.png"
                    action Show("menuFigureSelect")
        if randomDroid == 2:
            vbox xpos 137 ypos 302:
                imagebutton:
                    idle "UI/items/thumbnails/item10Figure2Thumb.png"
                    hover "UI/items/thumbnails/item10Figure2Thumb-hover.png"
                    action Show("menuFigureSelect")
        if randomDroid == 3:
            vbox xpos 137 ypos 302:
                imagebutton:
                    idle "UI/items/thumbnails/item10Figure3Thumb.png"
                    hover "UI/items/thumbnails/item10Figure3Thumb-hover.png"
                    action Show("menuFigureSelect")
        if randomDroid == 4:
            vbox xpos 137 ypos 302:
                imagebutton:
                    idle "UI/items/thumbnails/item10Figure4Thumb.png"
                    hover "UI/items/thumbnails/item10Figure4Thumb-hover.png"
                    action Show("menuFigureSelect")
        hbox:
            spacing 10 xpos 217 ypos 382
            text "{size=-1}{color=#FFE653}[actionFigure]{/color}{/size}"

                
    if redCrystalActive == True and inventoryStageSelect == 0:                                          # location #11
        vbox xpos 250 ypos 301:
            imagebutton:
                idle "UI/items/thumbnails/item11CrystalThumb.png"
                hover "UI/items/thumbnails/item11CrystalThumb-hover.png"
                action Show("menuCrystalSelect")
                
    if chocolate >= 1 and inventoryStageSelect == 0:                                                            # location #12
        vbox xpos 365 ypos 301:
            imagebutton:
                idle "UI/items/thumbnails/item12chocolateThumb.png"
                hover "UI/items/thumbnails/item12chocolateThumb-hover.png"
                action Show("menuChocolateSelect")
        hbox:
            spacing 10 xpos 445 ypos 384
            text "{size=-1}{color=#FFE653}[chocolate]{/color}{/size}" 
                
    if healthStimm >= 1 and inventoryStageSelect == 0:                                                       # location #13
        vbox xpos 26 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item13stimThumb.png"
                hover "UI/items/thumbnails/item13stimThumb-hover.png"
                action Show("menuStimSelect")
        hbox:
            spacing 10 xpos 109 ypos 492
            text "{size=-1}{color=#FFE653}[healthStimm]{/color}{/size}" 
                
    if deathSticks >= 1 and inventoryStageSelect == 0:                                                       # location #14
        vbox xpos 136 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item14sticksThumb.png"
                hover "UI/items/thumbnails/item14sticksThumb-hover.png"
                action Show("menuSticksSelect")
        hbox:
            spacing 10 xpos 232 ypos 492
            text "{size=-1}{color=#FFE653}[deathSticks]{/color}{/size}"
                
    if cards >= 1 and inventoryStageSelect == 0:                                                       # location #15
        vbox xpos 253 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item15CardsThumb.png"
                hover "UI/items/thumbnails/item15CardsThumb-hover.png"
                action Show("menuCardsSelect")
        hbox:
            spacing 10 xpos 343 ypos 492
            text "{size=-1}{color=#FFE653}[cards]{/color}{/size}"
                
    if holotape >= 1 and inventoryStageSelect == 0:                                                             # location #16
        vbox xpos 367 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item15Vid.png"
                hover "UI/items/thumbnails/item15Vid-hover.png"
                action Show("menuHolovidSelect")
        hbox:
            spacing 10 xpos 455 ypos 492
            text "{size=-1}{color=#FFE653}[holotape]{/color}{/size}"
                
    if bird >= 1 and inventoryStageSelect == 1:                                                                      # location #18
        vbox xpos 135 ypos 78:
            imagebutton:
                idle "UI/items/thumbnails/item18birdThumb.png"
                hover "UI/items/thumbnails/item18birdThumb-hover.png"
                action Show("menuitem18birdSelect")
            
    if potencyPotion >= 1 and inventoryStageSelect == 1:                                                       # location #21
        vbox xpos 20 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item21potencyPotionThumb.png"
                hover "UI/items/thumbnails/item21potencyPotionThumb-hover.png"
                action Show("menuitem21potencyPotionSelect")
        hbox:
            spacing 10 xpos 110 ypos 270
            text "{size=-1}{color=#FFE653}[potencyPotion]{/color}{/size}" 
            
    if greenVial >= 1 and inventoryStageSelect == 1:                                                       # location #22
        vbox xpos 140 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item22greenPotionThumb.png"
                hover "UI/items/thumbnails/item22greenPotionThumb-hover.png"
                action Show("menuitem22greenPotionSelect")
        hbox:
            spacing 10 xpos 220 ypos 270
            text "{size=-1}{color=#FFE653}[greenVial]{/color}{/size}" 
            
    if blueVial >= 1 and inventoryStageSelect == 1:                                                       # location #23
        vbox xpos 255 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item23bluePotionThumb.png"
                hover "UI/items/thumbnails/item23bluePotionThumb-hover.png"
                action Show("menuitem23bluePotionSelect")
        hbox:
            spacing 10 xpos 340 ypos 270
            text "{size=-1}{color=#FFE653}[blueVial]{/color}{/size}" 
            
    if art >= 1 and inventoryStageSelect == 1:                                                                      # location # 24
        vbox xpos 362 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item24artThumb.png"
                hover "UI/items/thumbnails/item24artThumb-hover.png"
                action Show("menuitem24artSelect")
        hbox:
            spacing 10 xpos 454 ypos 270
            text "{size=-1}{color=#FFE653}[art]{/color}{/size}"
                
    if artifact >= 1 and inventoryStageSelect == 1:                                                                      # location # 25
        vbox xpos 22 ypos 303:
            imagebutton:
                idle "UI/items/thumbnails/item25artifactThumb.png"
                hover "UI/items/thumbnails/item25artifactThumb-hover.png"
                action Show("menuitem25artifactSelect")
        hbox:
            spacing 10 xpos 113 ypos 383
            text "{size=-1}{color=#FFE653}[artifact]{/color}{/size}"
                
    if plushieToy >= 1 and inventoryStageSelect == 1:                                                     # location # 26
        vbox xpos 141 ypos 303:
            imagebutton:
                idle "UI/items/thumbnails/item26banthaThumb.png"
                hover "UI/items/thumbnails/item26banthaThumb-hover.png"
                action Show("menuitem26banthaSelect")
                
    if candyCaneEvil >= 1 and inventoryStageSelect == 1:                                                       # location # 28
        vbox xpos 363 ypos 299:
            imagebutton:
                idle "UI/items/thumbnails/item28CandyCaneEvilThumb.png"
                hover "UI/items/thumbnails/item28CandyCaneEvilThumb-hover.png"
                action Show("menuCandyCaneEvilSelect")
        hbox:
            spacing 10 xpos 453 ypos 380
            text "{size=-1}{color=#FFE653}[candyCaneEvil]{/color}{/size}" 
                
    if candyCane >= 1 and inventoryStageSelect == 1:                                                       # location # 29
        vbox xpos 22 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item29CandyCaneThumb.png"
                hover "UI/items/thumbnails/item29CandyCaneThumb-hover.png"
                action Show("menuCandyCaneSelect")
        hbox:
            spacing 10 xpos 110 ypos 492
            text "{size=-1}{color=#FFE653}[candyCane]{/color}{/size}" 
                
    if coal >= 1 and inventoryStageSelect == 1:                                                                  # location # 30
        vbox xpos 136 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item30coalThumb.png"
                hover "UI/items/thumbnails/item30coalThumb-hover.png"
                action Show("menuCoalSelect")
        hbox:
            spacing 10 xpos 220 ypos 492
            text "{size=-1}{color=#FFE653}[coal]{/color}{/size}" 
                
    if christmasPresent >= 1 and inventoryStageSelect == 1:                                            # location # 31
        vbox xpos 250 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item31christmasPresentThumb.png"
                hover "UI/items/thumbnails/item31christmasPresentThumb-hover.png"
                action Show("menuChristmasPresentSelect")
        hbox:
            spacing 10 xpos 340 ypos 492
            text "{size=-1}{color=#FFE653}[christmasPresent]{/color}{/size}" 
                
    if TreatBag >= 1 and inventoryStageSelect == 1:                                                             # location # 32
        vbox xpos 370 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/item32TreatBagThumb.png"
                hover "UI/items/thumbnails/item32TreatBagThumb-hover.png"
                action Show("menuTreatBagSelect")
        hbox:
            spacing 10 xpos 455 ypos 492
            text "{size=-1}{color=#FFE653}[TreatBag]{/color}{/size}" 
                
    
    ##############################################################################
    ####################### Display ACCESSORIES if they are true #######################
    ##############################################################################
    
    ####################### ACCESSORIES Ahsoka #######################
    
    if ahsokaXmasHat == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 0:
        vbox xpos 365 ypos 410:
            imagebutton:
                idle "UI/items/thumbnails/acc15AhsokaXmasThumb.png"
                hover "UI/items/thumbnails/acc15AhsokaXmasThumb-hover.png"
                action Show("menuAhsokaXmasHatSelect")
    
###############################################################
######################## Ahsoka -  Collars ########################
###############################################################   

    if colar1Active == True and inventoryStageSelect == 3  and inventoryCurrentCharacter == 0:
        vbox xpos 130 ypos 200:
            imagebutton:
                idle "UI/items/thumbnails/acc05Collar1Thumb.png"
                hover "UI/items/thumbnails/acc05Collar1Thumb-hover.png"
                action Show("menuCollar1Select")
                
    if colar2Active == True and inventoryStageSelect == 3  and inventoryCurrentCharacter == 0:
        vbox xpos 245 ypos 200:
            imagebutton:
                idle "UI/items/thumbnails/acc06Collar2Thumb.png"
                hover "UI/items/thumbnails/acc06Collar2Thumb-hover.png"
                action Show("menuCollar2Select")
                
    if colar3Active == True and inventoryStageSelect == 3  and inventoryCurrentCharacter == 0:
        vbox xpos 360 ypos 200:
            imagebutton:
                idle "UI/items/thumbnails/acc07Collar3Thumb.png"
                hover "UI/items/thumbnails/acc07Collar3Thumb-hover.png"
                action Show("menuCollar3Select")
                
    if colar4Active == True and inventoryStageSelect == 3  and inventoryCurrentCharacter == 0:
        vbox xpos 30 ypos 310:
            imagebutton:
                idle "UI/items/thumbnails/acc08Collar4Thumb.png"
                hover "UI/items/thumbnails/acc08Collar4Thumb-hover.png"
                action Show("menuCollar4Select")
                
    if bwStockingsAhs == True and inventoryStageSelect == 3  and inventoryCurrentCharacter == 0:
        vbox xpos 255 ypos 310:
            imagebutton:
                idle "UI/items/thumbnails/acc08stockings8Thumb.png"
                hover "UI/items/thumbnails/acc08stockings8Thumb-hover.png"
                action Show("menuAhsokaStockingsBWSelect")
                
    if buttPlugActive == True and inventoryStageSelect == 3:
        vbox xpos 365 ypos 310:
            imagebutton:
                idle "UI/items/thumbnails/item04ButtplugThumb.png"
                hover "UI/items/thumbnails/item04ButtplugThumb-hover.png"
                action Show("menuButtplugSelect")
    
    ####################### ACCESSORIES Shin'na #######################
    
    if maskActive == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 1:
        vbox xpos 22 ypos 81:
            imagebutton:
                idle "UI/items/thumbnails/acc01MaskThumb.png"
                hover "UI/items/thumbnails/acc01MaskThumb-hover.png"
                action Show("menuMaskSelect")
                
    if shinActive == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 1:
        vbox xpos 257 ypos 81:
            imagebutton:
                idle "UI/items/thumbnails/acc03HeadbandThumb.png"
                hover "UI/items/thumbnails/acc03HeadbandThumb-hover.png"
                action Show("menuHeadbandSelect")
                
    if shinXmasHatActive == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 1:
        vbox xpos 250 ypos 410:
            imagebutton:
                idle "UI/items/thumbnails/acc15ShinXmasThumb.png"
                hover "UI/items/thumbnails/acc15ShinXmasThumb-hover.png"
                action Show("menuShinXmasHatSelect")
                
    if shinHalloweenMaskActive == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 1:
        vbox xpos 370 ypos 410:
            imagebutton:
                idle "UI/items/thumbnails/acc16ShinHalloweenMaskThumb.png"
                hover "UI/items/thumbnails/acc16ShinHalloweenMaskThumb-hover.png"
                action Show("menuShinHalloweenMaskSelect")
                
    ######################### ACCESSORIES Kit  ##########################
    
    if kitHatActive == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 2:
        vbox xpos 22 ypos 81:
            imagebutton:
                idle "UI/items/thumbnails/acc01KitHatThumb.png"
                hover "UI/items/thumbnails/acc01KitHatThumb-hover.png"
                action Show("menuKitHatSelect")

    if kitXmasHat == True and inventoryStageSelect == 3 and inventoryCurrentCharacter == 2:
        vbox xpos 250 ypos 410:
            imagebutton:
                idle "UI/items/thumbnails/acc15KitXmaxThumb.png"
                hover "UI/items/thumbnails/acc15KitXmaxThumb-hover.png"
                action Show("menuKitXmasHatSelect")
                
    ####################### GLOBAL ACCESSORIES #######################
    
    if blindFoldActive == True and inventoryStageSelect == 3:
        vbox xpos 140 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/acc02BlindfoldThumb.png"
                hover "UI/items/thumbnails/acc02BlindfoldThumb-hover.png"
                action Show("menuBlindfoldSelect")
                
    if aurinTailActive == True and inventoryStageSelect == 3:
        vbox xpos 360 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/acc04tailThumb.png"
                hover "UI/items/thumbnails/acc04tailThumb-hover.png"
                action Show("menuTailSelect")
                
    if gagActive == True and inventoryStageSelect == 3:
        vbox xpos 20 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/item06GagThumb.png"
                hover "UI/items/thumbnails/item06GagThumb-hover.png"
                action Show("menuGagSelect")
                


    ####################### Display Ahsoka OUTFITS if they are true #######################
    
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0:                            #Ahsoka underwear outfit
        vbox xpos 30 ypos 405:
            imagebutton:
                idle "UI/items/thumbnails/outfit00AhsokaThumb.png"
                hover "UI/items/thumbnails/outfit00AhsokaThumb-hover.png"
                action Show("menuAhsokaOutfit0Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and underwear2Active == True:                                #Ahsoka underwear2 outfit
        vbox xpos 155 ypos 405:
            imagebutton:
                idle "UI/items/thumbnails/outfit09AhsokaThumb.png"
                hover "UI/items/thumbnails/outfit09AhsokaThumb-hover.png"
                action Show("menuAhsokaOutfit9Select")
    
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0:                                                                            #Ahsoka basic outfit
        vbox xpos 30 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/outfit01Ahsoka1Thumb.png"
                hover "UI/items/thumbnails/outfit01Ahsoka1Thumb-hover.png"
                action Show("menuAhsokaOutfit1Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearFoodUniformActive ==True:                         #Ahsoka lekku outfit
        vbox xpos 150 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/outfit02Ahsoka2Thumb.png"
                hover "UI/items/thumbnails/outfit02Ahsoka2Thumb-hover.png"
                action Show("menuAhsokaOutfit2Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearSlaveActive == True:                                #Ahsoka slave outfit
        vbox xpos 260 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/outfit03Ahsoka3Thumb.png"
                hover "UI/items/thumbnails/outfit03Ahsoka3Thumb-hover.png"
                action Show("menuAhsokaOutfit3Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearPitUniformActive:                            #Ahsoka pitgirl outfit
        vbox xpos 375 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/outfit04Ahsoka4Thumb.png"
                hover "UI/items/thumbnails/outfit04Ahsoka4Thumb-hover.png"
                action Show("menuAhsokaOutfit4Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearEscortDressActive:                            #Ahsoka escort outfit
        vbox xpos 22 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/outfit05Ahsoka5Thumb.png"
                hover "UI/items/thumbnails/outfit05Ahsoka5Thumb-hover.png"
                action Show("menuAhsokaOutfit5Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearArmorActive:                            #Ahsoka armor outfit
        vbox xpos 140 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/outfit06Ahsoka6.png"
                hover "UI/items/thumbnails/outfit06Ahsoka6-hover.png"
                action Show("menuAhsokaOutfit6Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and mission13 >= 15:                            #Ahsoka season 3 outfit
        vbox xpos 265 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/outfit07ahsoka07Thumb.png"
                hover "UI/items/thumbnails/outfit07ahsoka07Thumb-hover.png"
                action Show("menuAhsokaOutfit7Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and mission7 >= 7:                                  #Ahsoka bikini
        vbox xpos 360 ypos 190:
            imagebutton:
                idle "UI/items/thumbnails/outfit08ahsoka08Thumb.png"
                hover "UI/items/thumbnails/outfit08ahsoka08Thumb-hover.png"
                action Show("menuAhsokaOutfit8Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearTrooperAhsoka == True:                     #Ahsoka Clonetrooper
        vbox xpos 25 ypos 300:
            imagebutton:
                idle "UI/items/thumbnails/outfit09ahsoka09Thumb.png"
                hover "UI/items/thumbnails/outfit09ahsoka09Thumb-hover.png"
                action Show("menuAhsokaOutfit09Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and mission12 >= 1:                                 #Ahsoka slave Dress 2
        vbox xpos 140 ypos 295:
            imagebutton:
                idle "UI/items/thumbnails/outfit10ahsoka10Thumb.png"
                hover "UI/items/thumbnails/outfit10ahsoka10Thumb-hover.png"
                action Show("menuAhsokaOutfit10Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and mission10 >= 8:                                 #Ahsoka nightwear T-shirt 2
        vbox xpos 257 ypos 303:
            imagebutton:
                idle "UI/items/thumbnails/outfit11ahsoka11Thumb.png"
                hover "UI/items/thumbnails/outfit11ahsoka11Thumb-hover.png"
                action Show("menuAhsokaOutfit11Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearHalloween2AhsokaActive == True:   #Ahsoka halloween2
        vbox xpos 377 ypos 303:
            imagebutton:
                idle "UI/items/thumbnails/outfit12ahsoka12Thumb.png"
                hover "UI/items/thumbnails/outfit12ahsoka12Thumb-hover.png"
                action Show("menuAhsokaOutfit12Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearChristmasActive:                            #Ahsoka Xmas outfit
        vbox xpos 250 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/outfit15ahsoka15Thumb.png"
                hover "UI/items/thumbnails/outfit15ahsoka15Thumb-hover.png"
                action Show("menuAhsokaOutfit15Select")
                
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 0 and gearHalloweenActive:                            #Ahsoka halloween outfit
        vbox xpos 370 ypos 412:
            imagebutton:
                idle "UI/items/thumbnails/outfit16Ahsoka16Thumb.png"
                hover "UI/items/thumbnails/outfit16Ahsoka16Thumb-hover.png"
                action Show("menuAhsokaOutfit16Select")
                
    ####################### Display Shin'na OUTFITS if they are true #######################
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True:
        vbox xpos 30 ypos 77:
            imagebutton:
                idle "UI/items/thumbnails/outfit01Shin1Thumb.png"
                hover "UI/items/thumbnails/outfit01Shin1Thumb-hover.png"
                action Show("menuShinOutfit1Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and shinProsOutfit == True:                                                  #Shin'na Prostitution outfit
        vbox xpos 140 ypos 77:
            imagebutton:
                idle "UI/items/thumbnails/outfit03Shin03Thumb.png"
                hover "UI/items/thumbnails/outfit03Shin03Thumb-hover.png"
                action Show("menuShinOutfit2Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and shinBikiniOutfit == True:                                                 #Shin'na Bikini outfit
        vbox xpos 250 ypos 79:
            imagebutton:
                idle "UI/items/thumbnails/outfit04Shin04Thumb.png"
                hover "UI/items/thumbnails/outfit04Shin04Thumb-hover.png"
                action Show("menuShinOutfit3Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and gearTrooperShin == True:                                            #Shin'na Clone Trooper outfit
        vbox xpos 375 ypos 74:
            imagebutton:
                idle "UI/items/thumbnails/outfit05Shin05Thumb.png"
                hover "UI/items/thumbnails/outfit05Shin05Thumb-hover.png"
                action Show("menuShinOutfit4Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and communityShinActive:                                                      #Shin'na Harem Trooper outfit
        vbox xpos 27 ypos 184:
            imagebutton:
                idle "UI/items/thumbnails/outfit06Shin06Thumb.png"
                hover "UI/items/thumbnails/outfit06Shin06Thumb-hover.png"
                action Show("menuShinOutfit5Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and gearChristmasShinActive == True:                                  #Shin'na Christmas outfit
        vbox xpos 255 ypos 410:
            imagebutton:
                idle "UI/items/thumbnails/outfit15Shin15Thumb.png"
                hover "UI/items/thumbnails/outfit15Shin15Thumb-hover.png"
                action Show("menuShinOutfit15Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 1 and shinActive == True and gearHalloweenShinActive == True:                                  #Shin'na halloween outfit
        vbox xpos 366 ypos 415:
            imagebutton:
                idle "UI/items/thumbnails/outfit16Shin16Thumb.png"
                hover "UI/items/thumbnails/outfit16Shin16Thumb-hover.png"
                action Show("menuShinOutfit16Select")
                
    ####################### Display Kit OUTFITS if they are true #######################
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True:
        vbox xpos 24 ypos 80:
            imagebutton:
                idle "UI/items/thumbnails/outfit01Kit1Thumb.png"
                hover "UI/items/thumbnails/outfit01Kit1Thumb-hover.png"
                action Show("menuKitOutfit1Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and kitProsOutfit == True:                                                  #Kit Prostitution outfit
        vbox xpos 140 ypos 77:
            imagebutton:
                idle "UI/items/thumbnails/outfit02Kit2Thumb.png"
                hover "UI/items/thumbnails/outfit02Kit2Thumb-hover.png"
                action Show("menuKitOutfit2Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and mission7 >= 7:                                                            #Kit bikini
        vbox xpos 250 ypos 77:
            imagebutton:
                idle "UI/items/thumbnails/outfit03Kit3Thumb.png"
                hover "UI/items/thumbnails/outfit03Kit3Thumb-hover.png"
                action Show("menuKitOutfit3Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and gearTrooperKit == True:                                            #Kit armor
        vbox xpos 375 ypos 75:
            imagebutton:
                idle "UI/items/thumbnails/outfit04Kit4Thumb.png"
                hover "UI/items/thumbnails/outfit04Kit4Thumb-hover.png"
                action Show("menuKitOutfit4Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and communityKitActive:                                                     #Kit slave
        vbox xpos 26 ypos 185:
            imagebutton:
                idle "UI/items/thumbnails/outfit05Kit5Thumb.png"
                hover "UI/items/thumbnails/outfit05Kit5Thumb-hover.png"
                action Show("menuKitOutfit5Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and gearChristmasKitActive == True:                                  #Kit Christmas outfit
        vbox xpos 250 ypos 415:
            imagebutton:
                idle "UI/items/thumbnails/outfit15Kit15Thumb.png"
                hover "UI/items/thumbnails/outfit15Kit15Thumb-hover.png"
                action Show("menuKitOutfit15Select")
                
    if inventoryStageSelect == 5 and inventoryCurrentCharacter == 2 and kitActive == True and gearHalloweenKitActive == True:                                  #Kit halloween outfit
        vbox xpos 366 ypos 415:
            imagebutton:
                idle "UI/items/thumbnails/outfit16Kit16Thumb.png"
                hover "UI/items/thumbnails/outfit16Kit16Thumb-hover.png"
                action Show("menuKitOutfit16Select")
    
############################################################
######################## #1 LIGHTSABER #####################
############################################################
define lootedLightsaber = False

screen item01Lightsaber:
    add "UI/items/item01Lightsaber.png"
            
screen menuLightsaberSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Inspect":
            clicked Hide("menuLightsaberSelect"), Jump("invInspLightsaber")
        textbutton "Cancel":
            action Hide("menuLightsaberSelect")
            
label invInspLightsaber:
    "The hilt of a lightsaber."
    "These rare weapons are used by Jedi and can come in a number of different colors."
    "You try turning it on, but it doesn't seem to work."
    jump inventory
    
    
################################################################
############################ #2 WINE ############################
################################################################
define firstTimeWine = False

screen wine:
    add "UI/items/item2wine.png"
    
screen menuWineSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuWineSelect"), Jump("giftWine")
        textbutton "Inspect":
            clicked Hide("menuWineSelect"), Jump("invInspWine")
        textbutton "Cancel":
            action Hide("menuWineSelect")
            
label invInspWine:
    "An expensive wine that is said to have a special effect on Force users."
    jump inventory

label giftWine:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 18:
        $ ahsokaExpression = 15
        a "Hm? What's this?"
        y "It's wine."
        $ ahsokaExpression = 5
        a "Wine? We Jedi aren't suppose to drink."
        y "Just drink up, this stuff is expensive you know."
        $ ahsokaExpression = 14
        a "But why?"
        y "It's a gift."
        $ ahsokaExpression = 12
        a "Oh..."
        y "............."
        y "You're not going to drink it are you?"
        $ ahsokaExpression = 20
        a "I er..."
        y "It's fine. I'll keep it with me for a while longer then."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeWine == False:
        $ firstTimeWine = True
        $ wine -= 1
        $ mood += 40
        $ ahsokaExpression = 20
        a "Zelosian Wine..."
        a "They say it's has strange Force sensative qualities to it..."
        y "They just say that to drive up the price."
        "Ahsoka takes a sip."
        $ ahsokaExpression = 11
        a "Oh..."
        y "Good?"
        a "Yeah... {i}'really'{/i} good in fact."
        a "This is incredible!"
        y "That good...?"
        "Ahsoka puts the bottle to her lips."
        y "Woah~... easy there!"
        "Ahsoka drank the whole bottle!"
        $ ahsokaExpression = 8
        $ ahsokablush = 1
        a "{b}*Hic!*{/b} Thish shtuff's amazing~....!"
        a "I feel like I could forch push a mountain! {b}*Hic!*{/b}"
        "Ahsoka raises both hands out in front of her!"
        hide screen ahsoka_main
        with d2
        y "NO WAIT!"
        with hpunch
        "{b}BZZZZZZZZZZZZZZZZZZZZZZZZZ.......!!!{/b}"
        "The Force supressors kick in and soon the two of you roll on the ground in agony."
        "BZZZZZZZZZzzzzzzzzzzz{size=-6}zzzzzzzzzzzz.....................{/size}"
        y "Don't do that again."
        $ ahsokaExpression = 18
        show screen ahsoka_main
        with d3
        a "Shorry~...."
        "The shock has sobered Ahsoka up somewhat as she thanks you for the gift."
        "Her mood has increased substantially."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        $ ahsokaBlush = 0
        jump exitInventory
    if ahsokaSocial >= 19:
        $ addictWine += 1
        $ mood += 35
        $ wine -= 1
        $ ahsokaIgnore = 2
        $ ahsokaExpression = 11
        a "Zelosian wine? You shouldn't have!"
        "Ahsoka gladly accepts the gift."
        y "Now make sure you don't-...."
        $ ahsokaExpression = 3
        "Ahsoka has already put the bottle to her lips."
        y "Drink it in one go~...."
        if ahsokaSlut >= 33:
            $ sexCum = 0
            $ ahsokaExpression = 8
            $ ahsokaBlush = 1
            with d3
            a "Thish stuff is amazing, [playerName]..."
            a "It's making me feel a bit..."
            $ ahsokaExpression = 7
            a "......."
            $ ahsokaExpression = 8
            a "Naughty."
            a "Are you thinking what I'm thinking....?"
            y "......................"
            menu:
                "Have sex":
                    stop music fadeout 2.0
                    play sound "audio/sfx/cloth.mp3"
                    $ outfit = 0
                    with d3
                    pause 0.5
                    hide screen ahsoka_main
                    hide screen scene_darkening
                    with d3
                    $ sexCum = 0
                    $ sexExpression = 4
                    scene black with fade
                    play music "audio/music/danceMusic.mp3"
                    show screen sex_scene2
                    with d5
                    play sound "audio/sfx/slime1.mp3"
                    a "Ahhh~.... ♥{w} Hic!"
                    "Ahsoka slowly lowers herself down on your shaft as it slips inside her wet peach."
                    "It slides in deeper and deeper, coating the base in her slick love juices as the girl squeels and moans."
                    a "Yes, [playerName]. Deeper!"
                    "She pushes herself down all the way, filling herself up to the brim with cock."
                    "The drunk, whoobly girl takes a short breath as she adjusts herself to the length that has just penetrated her."
                    "Then she smirks down at you and begins sliding herself up and down your dick."
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump1.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.4
                    "The overly excited tipsy girl bounce up and down you like a pogostick."
                    a "Hic...!"
                    a "Drunk sex is the best sex!"
                    y "Damn right it is."
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump1.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.4
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.3
                    "Overcome by lust, the orange Torgruta bounces her way closer and closer to climax."
                    a "{b}*Bounce* *Bounce* *Bounce*{/b} I... I... I... I.... I love this so much!"
                    y "Damn, looks like someone was thirsty!"
                    a "Yeah I drank all of the wine!"
                    y "That's not what I meant, but whatever."
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump1.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump1.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump2.mp3"
                    with hpunch
                    pause 0.3
                    play sound "audio/sfx/thump3.mp3"
                    with hpunch
                    pause 0.3
                    y "ARE YA READY KIDS?!!!"
                    a "AY-AYE CAPTAIN!!!"
                    "You begin pumping the girl full of thick baby batter!"
                    play sound "audio/sfx/cum1.mp3"
                    $ sexCum = 1
                    with flashbulb and hpunch
                    pause 0.3
                    play sound "audio/sfx/cum3.mp3"
                    $ sexCum = 2
                    with flashbulb and hpunch
                    pause
                    hide screen sex_scene2
                    with d3
                    pause 0.3
                    $ ahsokaExpression = 5
                    show screen scene_darkening
                    show screen ahsoka_main
                    with d3
                    a "{b}*Pant* *Pant*{/b} Woooo~....{w} Hic...!"
                    y "I think you've had enough fun for today. Go sleep it off."
                    $ ahsokaExpression = 23
                    a "Ahsoka is tipsy!"
                    y "Yes... Yes she is."
                    $ ahsokaIgnore = 2
                    stop music fadeout 2.0
                    hide screen ahsoka_main
                    with d3
                    hide screen scene_darkening
                    with d3
                    $ sexCum = 0
                    play music "audio/music/soundBasic.mp3"
                    jump bridge
                "Decline":
                    y "I don't want to take advantage of  drunk woman. I have standards you know."
                    $ ahsokaExpression = 11
                    a "But...~{w} You {i}'enslaved'{/i} me!"
                    a "You drag me along like a pet and have sex with me! But taking advantage of drunk women crosses the line?"
                    "Ahsoka looks hopelessly confused."
                    y "What can I say? My standards are hopelessly skewed and don't make sense, but they're still standards."
                    y "Anyways, enjoy your gift!"
                    "You leave the drunk, confused girl alone as she stands flabbergasted in the room."
                    "She probably won't be available to work today."
                    pass
        if ahsokaSlut <= 30:
            $ ahsokaBlush = 1
            with d3
            $ ahsokaExpression = 3
            a "Mmm~... it tastes so good!"
            y "It really does seem to have an effect on Force users..."
            a "The room's spinning....~"
            y "................."
            "It doesn't look like Ahsoka is going to be very useful today. Best give her the day off."
            pass
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        $ ahsokaBlush = 0
        jump exitInventory
    
################################################################
############################ #3 PIZZA ############################
################################################################
define firstTimePizza = False

screen pizza:
    add "UI/items/item03PizzaHutt.png"
    
screen menuPizzaSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuPizzaSelect"), Jump("giftPizza")
        textbutton "Inspect":
            clicked Hide("menuPizzaSelect"), Jump("invInspPizza")
        textbutton "Cancel":
            action Hide("menuPizzaSelect")
            
label invInspPizza:
    "A {i}'freshly'{/i} baked pizza from Pizza Hutt. Bringing out the cartboard flavor all across the galaxy!"
    jump inventory

label giftPizza:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 10:
        $ ahsokaExpression = 8
        a "Oh, a pizza? It even has a picture of you on the box."
        y "....................."
        y "Just accept the pizza, all right?"
        $ ahsokaExpression = 5
        a "I'm trying to watch my figure."
        y "Your figure....?"
        $ ahsokaExpression = 8
        a "Yes.{w} Really I am just refusing your terrible gift and telling you to blast off without hurting your feelings."
        y ".................."
        y "So now you called me fat 'and' hurt my feelings."
        $ ahsokaExpression = 3
        a "It's a win / win for me."
        "Ahsoka refused the gift and called you fat."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimePizza == False:
        $ firstTimePizza = True
        $ pizza -= 1
        $ mood += 20
        $ ahsokaExpression = 9
        a "All right. I'll have a bite."
        $ ahsokaExpression = 20
        a "..............."
        a "I mean... it's not terrible."
        y "It's pizza. Not that hard to mess up. Good?"
        $ ahsokaExpression = 9
        a "Yeah! I like it! We never get pizza at the Jedi temple."
        y "You've been missing out!"
        "Ahsoka happily accepted the gift."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 11:
        $ mood += 10
        $ pizza -= 1
        $ ahsokaExpression = 9
        a "Oh we're having pizza tonight? Don't mind if I do!"
        "Ahsoka accepted your gift and her mood was raised."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
        
        
################################################################
############################ #4 PLUG ############################
################################################################
define firstTimePlug = False

screen plug:
    add "UI/items/item05ButtPlug.png"
    
screen menuPlugSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuPlugSelect"), Jump("giftPlug")
        textbutton "Inspect":
            clicked Hide("menuPlugSelect"), Jump("invInspPlug")
        textbutton "Cancel":
            action Hide("menuPlugSelect")
            
label invInspPlug:
    "It's a butt plug. It goes up the butt."
    jump inventory

label giftPlug:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 14
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSlut <= 35:
        a "What's this?"
        y "It's a toy, called a Butt Plug."
        if 0 <= ahsokaSlut <= 30:
            $ ahsokaExpression = 2
            a ".......!"
            a "Gross! Why would anyone even make this?!"
            a "Get it out of here. I don't want it!"
        if 31 <= ahsokaSlut <= 34:
            $ ahsokaExpression = 12
            a "Ehrm... hehe~... right. I knew that."
            $ ahsokaExpression = 13
            a "T-thanks...~"
            y "You don't want it, do you?"
            $ ahsokaExpression = 4
            a "Heh~... It's a little big. I'm not sure if I'm ready for those kinds of toys yet."
            y "All right. Let's get you trained up a bit first."
            a "Y-yeah~...."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump inventory

    if firstTimePlug == False:
        $ firstTimePlug = True
        $ plug -= 1
        $ mood += 20
        a "What's this?"
        y "It's a butt plug."
        $ ahsokaExpression = 16
        a "A what?"
        $ ahsokaExpression = 11
        a "You mean that goes...?! But why?! How?!"
        a "There's toys that do that? I... I... I'm not sure if it'll fit. I-...."
        $ ahsokaExpression = 20
        a "....................."
        $ ahsokaExpression = 12
        a "I mean...."
        $ ahsokaExpression = 7
        a "I'll try it of course...."
        $ ahsokaExpression = 8
        a "Thanks [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSlut >= 35:
        $ mood += 15
        $ plug -= 1
        $ ahsokaExpression = 9
        a "Hey what's this?"
        $ ahsokaExpression = 9
        a "Another butt plug?"
        $ ahsokaExpression = 8
        a "Thanks. I'll add it to my collection."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
        
        
################################################################
########################## #5 MAGAZINE #########################
################################################################
define firstTimeMagazine = False

screen magazine:
    add "UI/items/item05Magazine.png"
    
screen menuMagazineSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuMagazineSelect"), Jump("giftMagazine")
        textbutton "Inspect":
            clicked Hide("menuMagazineSelect"), Jump("invInspMagazine")
        textbutton "Cancel":
            action Hide("menuMagazineSelect")
            
label invInspMagazine:
    "{i}'Support the Troops'{/i}. Republic propaganda featuring some buff Clone Troopers."
    jump inventory

label giftMagazine:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 20:
        "W-what? I find this highly inappropriate!"
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        "Ahsoka refused the gift."
        jump exitInventory
    if firstTimeMagazine == False:
        $ magazine = True
        $ ahsokaExpression = 14
        $ firstTimeMagazine = True
        $ magazine -= 1
        $ mood += 15
        a "A magazine?"
        $ ahsokaBlush = 1
        with d3
        $ ahsokaExpression = 6
        "Ahsoka turns red."
        a "W-what's this?"
        y "Republic's gotta fund it's campaign somehow."
        $ ahsokaExpression = 18
        a "Ah. Heh heh~...."
        y "You don't want it?"
        $ ahsokaExpression = 11
        a "N-no! I'll keep it! To... keep up morale."
        y "Morale. Sure. Well enjoy, try to make sure the pictures don't stick together too much."
        "Ahsoka turns even redder than before!"
        $ ahsokaBlush = 0
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    else:
        $ mood += 15
        $ magazine -= 1
        $ ahsokaExpression = 9
        $ ahsokaBlush = 1
        with d3
        $ ahsokaExpression = 5
        a "{b}*Ahem*{/b} I see nothing wrong with supporting the troops..."
        a "I accept your gift."
        "Ahsoka quickly pockets the magazine."
        $ ahsokaBlush = 0
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
        
    
################################################################
########################## #6 ICECREAM #########################
################################################################

screen icecream:
    add "UI/items/item06Icecream.png"
    
screen menuIcecreamSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Eat":
            clicked Hide("menuIcecreamSelect"), Jump("giftIcecream")
        textbutton "Inspect":
            clicked Hide("menuIcecreamSelect"), Jump("invInspIcecream")
        textbutton "Cancel":
            action Hide("menuIcecreamSelect")
            
label invInspIcecream:
    "Brings back memories from a simpler, more bloodsoaked, time."
    jump inventory

label giftIcecream:
    $ icecream -= 1
    $ playerHitPoints = 3
    "You chomp down on the icecream!"
    play sound "audio/sfx/itemGot.mp3"
    "All your{color=#FFE653} hitpoints{/color} were restored!"
    jump inventory
        
    
################################################################
############################ #6 CHIPS ###########################
################################################################
define firstTimeChips = False

screen chips:
    add "UI/items/item07Chips.png"
    
screen menuChipsSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuChipsSelect"), Jump("giftChips")
        textbutton "Inspect":
            clicked Hide("menuChipsSelect"), Jump("invInspChips")
        textbutton "Cancel":
            action Hide("menuChipsSelect")
            
label invInspChips:
    "Stasis Chips! The Chips that stay stale for millennia to come!"
    jump inventory

label giftChips:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 5:
        $ ahsokaExpression = 6
        a "What are these...?"
        y "Chips. Try them."
        $ ahsokaExpression = 1
        a "Are you trying to poison me? What are you scheming, [playerName]?!"
        y "Just chips. Eat up."
        $ ahsokaExpression = 5
        a "Hmph~... I refuse."
        y "All right, suit yourself."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeChips == False:
        $ firstTimeChips = True
        $ chips -= 1
        $ mood += 5
        $ ahsokaExpression = 22
        a "So... what are these exactly?"
        y "I'm not sure. I looted them off of a vending machine."
        "Ahsoka carefully tries one."
        $ ahsokaExpression = 18
        a "Hmm...~"
        y "Good?"
        a "Salty. A bit stale. They're okay I guess."
        "You try some yourself. Stale and boring. A terrible gift, but Ahsoka seems happy enough."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 6:
        $ ahsokaExpression = 20
        $ mood += 5
        $ chips -= 1
        a "Yaaay~... more chips...."
        y "It's a gift. I got it just for you."
        $ ahsokaExpression = 7
        a "You... erm... shouldn't have."
        "Ahsoka accepts the gift."
        "It's the thought that counts. Her mood has improved a little."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
        
################################################################
######################## # 8 Cookies  #############################
################################################################
define firstTimeCookies = False

screen cookies:
    add "UI/items/item08Cookies.png"
    
screen menuCookiesSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuCookiesSelect"), Jump("giftCookies")
        textbutton "Inspect":
            clicked Hide("menuCookiesSelect"), Jump("invInspCookies")
        textbutton "Cancel":
            action Hide("menuCookiesSelect")
            
label giftCookies:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 10:
        $ ahsokaExpression = 19
        a "What do you want [playerName]?"
        y "I bring cookies as a peace offering."
        a "Cookies? I don't want your fithy cookies!"
        "Ahsoka declined the offered."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeCookies == False:
        $ ahsokaExpression = 15
        $ firstTimeCookies = True
        $ cookies -= 1
        $ mood += 5
        y "Here, try some cookies."
        a "Cookies?"
        $ ahsokaExpression = 11
        a "Oh! Girl Scout Cookies!"
        y "You like those?"
        $ ahsokaExpression = 9
        a "Who doesn't? Every year they host a fundraiser on Naboo for a good cause."
        a "The Jedi usually send someone to make an appearance to help raise interest. I've never actually tried a cookie before."
        y "Well here's your chance."
        $ ahsokaExpression = 3
        a "...{w}...{w}..."
        $ ahsokaExpression = 9
        a "Mmmm, they're good."
        a "Thank you [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 11:
        $ mood += 10
        $ cookies -= 1
        a "They pressured you into buying more cookies, didn't they?"
        y "......................"
        a "{b}*Giggles*{/b} Thank you for the gift, [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    jump inventory
            
label invInspCookies:
    "Girl Scout Cookies. Low fat... apperantly."
    jump inventory
    
    
    
################################################################
######################## # 9 Gum  #############################
################################################################
define firstTimeGum = False

screen gum:
    add "UI/items/item09Gum.png"
    
screen menuGumSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuGumSelect"), Jump("giftGum")
        textbutton "Inspect":
            clicked Hide("menuGumSelect"), Jump("invInspGum")
        textbutton "Cancel":
            action Hide("menuGumSelect")
            
label giftGum:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 10:
        $ ahsokaExpression = 19
        y "You like gum?"
        a "Gum? Not really. Why?"
        y "I got a spare Naboo Gum. Want it?"
        a "No... not really."
        "Ahsoka declined the gift."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeGum == False:
        $ ahsokaExpression = 21
        $ firstTimeGum = True
        $ gum -= 1
        $ mood += 5
        y "Want a package of gum?"
        a "I've never tried bubblegum before... Sure I'll have it."
        a "Hmmm~...."
        a "It's sticky."
        a "...................."
        a "It's okay. Thank you [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 11:
        $ mood += 10
        $ gum -= 1
        a "Thank you, [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    jump inventory
    
label invInspGum:
    "Commonly referred to as a Naboo Repair Kit. Why? {w}I don't know."
    jump inventory
    
    
################################################################
######################## # 10 Action Figure  #########################
################################################################
define firstTimeFigure = False
define actionFigureDonated = 0
define randomDroid = 0

image droidSong:
    "UI/droidHeart1.png"
    pause 0.45
    "UI/droidHeart2.png"
    pause 0.45
    "UI/droidHeart3.png"
    pause 0.45
    "UI/droidHeart4.png"
    pause 0.45
    "UI/droidHeart5.png"
    pause 0.45
    "UI/droidHeart6.png"
    pause 0.45
    "UI/empty.png"
    pause 0.45
    "UI/droidHeart6.png"
    pause 0.45
    "UI/empty.png"
    pause 0.45
    "UI/droidHeart6.png"
    pause 0.45
    "UI/empty.png"
    pause 0.45
    repeat

screen actionFigure:
    add "UI/items/item10Figure.png"
    
screen menuFigureSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuFigureSelect"), Jump("giftFigure")
        textbutton "Inspect":
            clicked Hide("menuFigureSelect"), Jump("invInspFigure")
        textbutton "Cancel":
            action Hide("menuFigureSelect")
            
label giftFigure:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 10:
        $ ahsokaExpression = 19
        y "I got you something."
        a "What is it this time, [playerName]?"
        a "Is this a toy? What makes you think I'd want this?"
        "Ahsoka declined the gift."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeFigure == False:
        $ actionFigureDonated += 1
        $ ahsokaExpression = 21
        $ firstTimeFigure = True
        $ actionFigure -= 1
        $ mood += 10
        a "What's this?"
        y "An action figure, apperantly. Want it?"
        "Ahsoka unpacks the box and turns the droid on."
        $ ahsokaExpression = 9
        a "It's cute. Thank you [playerName]."
        y "No problem. The guy I got it from said it was mint."
        $ ahsokaExpression = 15
        a "Oh neat... What does that mean?"
        y "I have no idea."
        "Ahsoka accepted the gift."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 11:
        $ ahsokaExpression = 11
        $ actionFigureDonated += 1
        if actionFigureDonated == 5:
            a "Oooh! I finally have a full set!"
            y "A full set?"
            a "Yeah, on the back of the box it says they come in all kinds of colors."
            $ ahsokaExpression = 9
            a "Collect them all and something is suppose to happen."
            y "...?"
            hide screen ahsoka_main
            hide screen scene_darkening
            with d3
            scene black with fade
            scene droidEvent
            with d5
            "..................."
            play music "audio/music/droids.mp3"
            show droidSong
            pause
            y "It warms my ice cold heart. ♥"
            stop music fadeout 5.0
            scene black with longFade
            pause 0.5
            scene bgBridge with fade
            play music "audio/music/soundBasic.mp3" fadein 1.5
            jump bridge
            
        $ ahsokaExpression = 9
        $ mood += 10
        $ actionFigure -= 1
        a "Hey! Another one! I'll add it to my collection. I don't think I have this color yet."
        a "Thank you, [playerName]."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    jump inventory
     
label invInspFigure:
    "The box reads:"
    "Astromech Action Squad!!! Collect them all! Batteries not included."
    jump inventory
    

################################################################
######################## #11 RED CRYSTAL #######################
################################################################
define redCrystalActive = False

screen redCrystal:
    add "UI/items/item11Crystal.png"
    
            
screen menuCrystalSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuCrystalSelect"), Jump("giftRedCrystal")
        textbutton "Inspect":
            clicked Hide("menuCrystalSelect"), Jump("invInspCrystal")
        textbutton "Cancel":
            action Hide("menuCrystalSelect")
            
label invInspCrystal:
    "It's a crystal meant to power lightsabers."
    "Could this be Ahsoka's missing crystal?"
    jump inventory

label giftRedCrystal:
    $ mission11 = 3
    jump exitInventory
    
    
################################################################
######################## #12 CHOCOLATE #######################
################################################################
define firstTimeChocolate = False

screen chocolate:
    add "UI/items/item12Chocolate.png"
    
screen menuChocolateSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuChocolateSelect"), Jump("giftChocolate")
        textbutton "Inspect":
            clicked Hide("menuChocolateSelect"), Jump("invInspChocolate")
        textbutton "Cancel":
            action Hide("menuChocolateSelect")
            
label invInspChocolate:
    "Spiced Chocolate Bar. A typical Republic candy."
    jump inventory

label giftChocolate:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 19
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 10:
        a "What do you want now?"
        a "A chocolate bar...?"
        a "Is this some half-hearted attempt to sway me to the Dark Side?"
        y "No. It's just chocolate."        
        a "I won't fall for your tricks to easily!"
        y "Fine, suit yourself."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if firstTimeChocolate == False:
        $ ahsokaExpression = 14
        $ firstTimeChocolate = True
        $ chocolate -= 1
        $ mood += 25
        a "Chocolate? Why are you giving me this?"
        y "A snack to help keep your spirits up?"
        $ ahsokaExpression = 20
        a ".............................."
        y "What? Are you allergic or something?"
        $ ahsokaExpression = 12
        a "No... at least I don't think I am. I've never had chocolate before."
        y "You never had chocolate...?"
        $ ahsokaExpression = 5
        a "The Jedi Teachings are very strict. We have no time for things that distract us from our duties. Fanciful candies included."
        y "It's just a chocolate bar. Eat it."
        $ ahsokaExpression = 19
        a "No, I don't want to."
        y "Yes you do. Eat it."
        $ ahsokaExpression = 2
        a "No!"
        y "Yes!"
        $ ahsokaExpression = 4
        a "..............."
        $ ahsokaExpression = 5
        a "Fine!"
        "Ahsoka snatches the chocolate bar from your hands and begrudgingly takes a bite."
        $ ahsokaExpression = 11
        a "!!!"
        a "This.... {w}This is really good."
        y "Told ya."
        "Ahsoka tasted chocolate for the first time! Her mood has gone up substantially."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    if ahsokaSocial >= 11:
        $ ahsokaExpression = 9
        $ mood += 10
        $ chocolate -= 1
        a "Chocolate? Oh, thank you. I don't get to eat these very often."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
        
        
################################################################
######################## #13 Health Stimm  #######################
################################################################
define firstTimeStim = False

screen stim:
    add "UI/items/item13Stim.png"
    
screen menuStimSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + -15)
        has vbox
        textbutton "Heal Self":
            clicked Hide("menuStimSelect"), Jump("healSelfStimm")
        textbutton "Heal Ahsoka":
            clicked Hide("menuStimSelect"), Jump("giftStim")
        textbutton "Inspect":
            clicked Hide("menuStimSelect"), Jump("invInspStim")
        textbutton "Cancel":
            action Hide("menuStimSelect")
            
label healSelfStimm:
    "The stim restored 1 point of health."
    $ playerHitPoints += 1
    $ healthStimm -= 1
    jump inventory
            
label invInspStim:
    "A Stim. Used to heal minor wounds."
    jump inventory

label giftStim:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    y "Hold on, you'll feel a little prick"
    $ ahsokaExpression = 20
    a "Yay~... flu shot."
    if ahsokaHealth <= 2:
        "Ahsoka recovers 1 HP."
        $ ahsokaHealth += 1
    $ healthStimm -= 1
    hide screen ahsoka_main
    hide screen scene_darkening
    with d3
    jump exitInventory
    
################################################################
######################### #14 Death Sticks  ########################
################################################################
define firstTimeDeathSticks = False
define drugsActive = False

screen deathStick:
    add "UI/items/item14sticks.png"
    
screen menuSticksSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + -15)
        has vbox
        textbutton "Gift":
            clicked Hide("menuSticksSelect"), Jump("giftSticks")
        textbutton "Inspect":
            clicked Hide("menuSticksSelect"), Jump("invInspSticks")
        textbutton "Cancel":
            action Hide("menuSticksSelect")
            
label invInspSticks:
    "Death Sticks. Dangerous and addictive drugs that fill the user with an overwhelming feeling of happiness. Highly addictive."
    jump inventory

label giftSticks:
    if ahsokaSocial <= 19:
        a "Death Sticks?! These are illigal! I don't want those!"
        jump inventory
    if 20 <= ahsokaSocial <= 30:
        a "Death Sticks? I don't know [playerName]...."
        a "I heard some really bad stories about these things. I'd rather not...."
        jump inventory
    if ahsokaSocial >= 31:
        if firstTimeDeathSticks == False:
            $ addictDrug += 1
            $ firstTimeDeathSticks = True
            $ mood += 45
            $ deathSticks -= 1
            hide screen ahsoka_main
            hide screen kit_main
            hide screen shin_main
            hide screen invAll
            scene bgBridge with fade
            $ ahsokaExpression = 18
            show screen scene_darkening
            with d3
            show screen ahsoka_main
            with d3
            a "Death Sticks? Are you sure? I heard they're highly addictive."
            y "Just a pick-me-up to raise morale."
            $ ahsokaExpression = 14
            a "If you say so..."
            "You and Ahsoka each take a Death Stick."
            $ ahsokaExpression = 15
            a "Hmm... That's strange. It tastes a bit like the burgers in the Lekka Lekku...."
            stop music fadeout 1.0
            hide screen ahsoka_main
            hide screen scene_darkening
            with d3
            scene bgBridgeDrugs with dissolve
            $ ahsokaExpression = 11
            pause 0.5
            show screen ahsoka_drugs
            with dissolve
            play music "audio/music/danceMusic.mp3" fadein 1.0
            a "Wooooah~......! Colors~...."
            y "Far out~...."
            a "I feel good~...."
            $ ahsokaExpression = 3
            a "When did I lose my clothes....?"
            $ ahsokaExpression = 8
            a "Hey [playerName]~.... ♥"
            hide screen ahsoka_drugs
            hide screen scene_darkening
            with d3
            hide screen ahsokaDrugs
            with d3
            scene black with dissolve
            pause 1.0
            a "Oooooh~.... ♥♥♥"
            if ahsokaSlut >= 31:
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.2
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                a "{b}*Giggles*{/b} Ahhhhh~.... ♥♥♥"
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.2
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                a "Yes~....! Right there~..... ♥♥♥"
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump1.mp3"
                with hpunch
                pause 0.3
                play sound "audio/sfx/thump2.mp3"
                with hpunch
                pause 0.2
                play sound "audio/sfx/thump3.mp3"
                with hpunch
                stop music fadeout 3.0
                pause 1.0
                scene bgBedroom
                with longFade
                pause 1.0
                show screen scene_darkening
                with d3
                pause 1.0
                $ outfit = 0
                $ underwearTop = 0
                $ underwearBottom = 0
                $ ahsokaBodyCum = 1
                $ ahsokaFaceCum = 1
            else:
                $ outfit = 0
                $ underwearTop = 0
                $ underwearBottom = 0
                a "{b}*Giggles*{/b} Ahhhhh~.... ♥♥♥"
                a "Yes~....! Right there~..... ♥♥♥"
                stop music fadeout 3.0
                pause 1.0
                scene bgBedroom
                with longFade
                pause 1.0
                show screen scene_darkening
                with d3
                pause 1.0
            $ ahsokaExpression = 3
            show screen ahsoka_main
            with d3
            a "..........................."
            $ ahsokaExpression = 18
            a "Why do I feel so....."
            $ ahsokaExpression = 6
            $ ahoskaBlush = 1
            a "!!!"
            $ ahsokaExpression = 11
            a "How long were we out for? What did we do?!"
            y "I don't know! Why are you naked?!"
            a "Let's... let's just pretend this never happened."
            hide screen ahsoka_main
            hide screen scene_darkening
            with d5
            $ ahsokaBodyCum = 0
            $ ahsokaFaceCum = 0
            play sound "audio/sfx/itemGot.mp3"
            "{color=#FFE653}Psychedelic Lekku{/color} have been unlocked in the Surgery menu."
            jump nightCycle
        else:
            if addictDrug >= 4:
                hide screen ahsoka_main
                hide screen kit_main
                hide screen shin_main
                hide screen invAll
                scene bgBridge with fade
                show screen scene_darkening
                with d3
                show screen ahsoka_main
                with d3
                a "YES!!!"
                y "..................."
                a "I-I mean... Yes, thank you~..."
                a "Can I have the Death Sticks now please?"
                y "You okay there? You seem to be wanting them a little bit too much."
                a "[playerName] please!"
                menu:
                    "Give Death Sticks":
                        $ addictDrug += 1
                        $ mood += 45
                        $ deathSticks -= 1
                        a "Thank you~...."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump exitInventory
                    "Refuse":
                        $ mood -= 20
                        a "W-what?! But you were going to give them!"
                        y "I changed my mind."
                        a "[playerName] please, just one!"
                        "You leave a clearly distressed Ahsoka alone."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump exitInventory
            else:
                $ drugsActive = True
                $ addictDrug += 1
                $ mood += 45
                $ deathSticks -= 1
                hide screen ahsoka_main
                hide screen kit_main
                hide screen shin_main
                hide screen invAll
                scene bgBridge with fade
                show screen scene_darkening
                with d3
                show screen ahsoka_main
                with d3
                a "More Death Sticks? Sure! I wonder why they're called that?"
                hide screen ahsoka_main
                hide screen scene_darkening
                with d3
                jump exitInventory
            
            
    
################################################################
######################### #15 Pazaak Cards  ########################
################################################################
define firstTimeCards = False

screen pazaakCards:
    add "UI/items/item15cards.png"
    
screen menuCardsSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + -15)
        has vbox
        textbutton "Gift":
            clicked Hide("menuCardsSelect"), Jump("giftCards")
        textbutton "Inspect":
            clicked Hide("menuCardsSelect"), Jump("invInspCards")
        textbutton "Cancel":
            action Hide("menuCardsSelect")
            
label giftCards:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 15:
        $ ahsokaExpression = 5
        a "Cards? I cannot distract myself with these."
        y "Not even for a game between friends?"
        $ ahsokaExpression = 2
        a "You're not my friend!"
        hide screen ahsoka_main
        with d3
        "Ahsoka refused the gift."
        jump exitInventory
    if firstTimeCards == False:
        $ firstTimeCards = True
        $ cards -= 1
        $ mood += 10
        $ ahsokaExpression = 14
        a "Pazaak cards?"
        y "You play?"
        $ ahsokaExpression = 9
        a "No..., but it always seemed like fun."
        a "Thank you, [playerName]. I'll try learning the rules. Maybe we can play together sometime."
        hide screen ahsoka_main
        with d3
        jump exitInventory
    if firstTimeCards == True:
        $ cards -= 1
        $ mood += 10
        a "Ohh! More cards! Now I can upgrade my deck even further!"
        a "Thank you, [playerName]."
        jump exitInventory
            
label invInspCards:
    "A popular card game called Pazaak."
    jump inventory
    
    
############################################################
######################## #16 HOLOVID ########################
############################################################
define firstTimeHolovid = False

screen holovid:
    add "UI/items/item16Vid.png"
    
screen menuHolovidSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + -15)
        has vbox
        textbutton "Gift":
            clicked Hide("menuHolovidSelect"), Jump("giftHolovid")
        textbutton "Inspect":
            clicked Hide("menuHolovidSelect"), Jump("invInspHolovid")
        textbutton "Cancel":
            action Hide("menuHolovidSelect")
            
label giftHolovid:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if ahsokaSocial <= 15:
        $ ahsokaExpression = 5
        a "A holovid? I'm sorry, but I cannot accept this."
        y "Why not?"
        $ ahsokaExpression = 2
        a "We've got more important things to worry about!"
        hide screen ahsoka_main
        with d3
        "Ahsoka refused the gift."
        jump exitInventory
    if firstTimeHolovid == False:
        $ firstTimeHolovid = True
        $ holotape -= 1
        $ mood += 20
        $ ahsokaExpression = 9
        a "Hey a holovid!"
        y "You like these?"
        a "Yeah, I always watch them while I'm travelling by myself to make me feel less alone."
        a "Thank you, [playerName]."
        hide screen ahsoka_main
        with d3
        jump exitInventory
    if firstTimeHolovid == True:
        $ holotape -= 1
        $ mood += 20
        a "Ooh! More holovids! I can't wait to see what these are about."
        a "Thank you, [playerName]."
        jump exitInventory
            
label invInspHolovid:
    "A holovid. A popular form of entertainment."
    jump inventory


############################################################
######################## #17 PLUSHIE ########################
############################################################

################################################################
########################## # 18 Bird  #############################
################################################################
define firstTimeBird = False

screen convorBird:
    add "UI/items/item18bird.png"
    
screen menuitem18birdSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem18birdSelect"), Jump("giftBird")
        textbutton "Inspect":
            clicked Hide("menuitem18birdSelect"), Jump("invInspBird")
        textbutton "Cancel":
            action Hide("menuitem18birdSelect")
            
label giftBird:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeBird == False:
        $ convorPet = True
        $ firstTimeBird = True
        $ bird -= 1
        $ mood += 60
        y "Ever had a pet before?"
        $ ahsokaExpression = 17
        a "A pet? {w}No, I've never really thought about it."
        y "Would you like one?"
        $ ahsokaExpression = 14
        a "What? A pet?"
        $ ahsokaExpression = 12
        a "I don't know, [playerName]... I probably wouldn't know how to take care of it and I'll have to make sure it's safe and healthy~...."
        $ ahsokaExpression = 20
        a "Although I have always loved those little Convor birds."
        y "................................"
        $ ahsokaExpression = 11
        a "No... You didn't!?!"
        "You hand over the bird thing to Ahsoka."
        a "It's a convor! Look at how cute it is!"
        y "(It's something all right.)"
        a "Oh [playerName], I was wrong. I 'do' really want a pet!"
        a "And I'm going to take good care of him! And....! And....!"
        $ ahsokaExpression = 13
        $ ahsokaTears = 1
        a "I'll be like it's mommy...! And....!"
        y "Are you okay?"
        $ ahsokaExpression = 23
        a "{b}*Sniff*{/b} Yeah, I'm fine... I just never expected to have my very own pet."
        a "Thank you, [playerName]. "
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        $ ahsokaTears = 0
        "Ahsoka's mood has skyrocketed!"
        jump exitInventory
            
label invInspBird:
    "The convar are a species of adorable little bird creatures. This one... looks a bit off."
    jump inventory
    
################################################################
###################### # 21 Potency Potion  #########################
################################################################

screen potencyPotion:
    add "UI/items/item21PotionRed.png"
    
screen menuitem21potencyPotionSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Inspect":
            clicked Hide("menuitem21potencyPotionSelect"), Jump("invInsppotencyPotion")
        textbutton "Cancel":
            action Hide("menuitem21potencyPotionSelect")
            
label invInsppotencyPotion:
    "The potion is labelled: {i}'Gizka Extract'{/i}."
    y "(I'm not drinking this, unless I want to walk around with a raging boner for the rest of the day.)"
    jump inventory
    
################################################################
###################### # 22 Happy Potion  #########################
################################################################

screen potencyPotion:
    add "UI/items/item22PotionRed.png"
    
screen menuitem22greenPotionSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem22greenPotionSelect"), Jump("giftgreenPotion")
        textbutton "Inspect":
            clicked Hide("menuitem22greenPotionSelect"), Jump("invInspgreenPotion")
        textbutton "Cancel":
            action Hide("menuitem22greenPotionSelect")
            
label giftgreenPotion:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    else:
        $ greenVial -= 1
        $ mood += 30
        a "{b}*Glug* *Glug* *Glug*{/b}"
        $ ahsokaExpression = 11
        with hpunch
        a "!!!!"
        a "I feel 'GREAT'! Thank you, [playerName]!"
        hide screen ahsoka_main
        with d2
        jump exitInventory
            
label invInspgreenPotion:
    "The potion is labelled: {i}'Artificial Happiness'{/i}."
    jump inventory
    
################################################################
###################### # 23 Health Potion  #########################
################################################################

screen potencyPotion:
    add "UI/items/item22PotionRed.png"
    
screen menuitem23bluePotionSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem23bluePotionSelect"), Jump("giftbluePotion")
        textbutton "Inspect":
            clicked Hide("menuitem23bluePotionSelect"), Jump("invInspbluePotion")
        textbutton "Cancel":
            action Hide("menuitem23bluePotionSelect")
            
label invInspbluePotion:
    "The potion is labelled: {i}'Kolto Extract'{/i}."
    jump inventory
    
label giftbluePotion:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    else:
        $ blueVial -= 1
        $ ahsokaHealth += 1
        a "{b}*Glug* *Glug* *Glug*{/b}"
        a "Bitter..."
        $ ahsokaExpression = 9
        a "My wounds don't hurt so much anymore now though. Thank you, [playerName]."
        hide screen ahsoka_main
        with d2
        jump exitInventory
    
################################################################
########################## # 24 Art  #############################
################################################################
define firstTimeArt = False

screen art:
    add "UI/items/item24bird.png"
    
screen menuitem24artSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem24artSelect"), Jump("giftArt")
        textbutton "Inspect":
            clicked Hide("menuitem24artSelect"), Jump("invInspArt")
        textbutton "Cancel":
            action Hide("menuitem24artSelect")
            
label giftArt:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 9
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeArt == False:
        $ firstTimeArt = True
        $ art -= 1
        $ mood += 35
        y "Hey Ahsoka, I came across this."
        $ ahsokaExpression = 22
        a "What's that...? It looks old. Really old."
        a "I think it's some kind of ancient Togruta artpiece. We should be careful with this."
        $ ahsokaExpression = 9
        a "I'll store it away somewhere safe. My people would be very grateful to receive such a piece. Thank you, [playerName]."
        jump exitInventory
    else:
        $ ahsokaExpression = 9
        $ mood += 30
        a "Thank you, [playerName]! I'll store it away somewhere safe."
        "Ahsoka's mood has improved."
        jump exitInventory
            
label invInspArt:
    "A wooden carving from a long lost Togruta tribe."
    jump inventory
    
    
################################################################
########################## # 25 Artifact  #############################
################################################################
define firstTimeArtifact = False

screen artifact:
    add "UI/items/item25bird.png"
    
screen menuitem25artifactSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem25artifactSelect"), Jump("giftArtifact")
        textbutton "Inspect":
            clicked Hide("menuitem25artifactSelect"), Jump("invInspArtifact")
        textbutton "Cancel":
            action Hide("menuitem25artifactSelect")
            
label giftArtifact:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 9
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        $ ahsokaExpression = 11
        a "A Jedi artifact?!"
        $ ahsokaExpression = 4
        a "I really want it, but...."
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeArtifact == False:
        $ firstTimeArtifact = True
        $ artifact -= 1
        $ mood += 35
        y "I brought you something shiny."
        $ ahsokaExpression = 9
        a "Hey that looks like a..."
        $ ahsokaExpression = 11
        a "!!!"
        a "That's a Jedi artifact!"
        a "These are incredible rare! You're giving me this?"
        a "Thank you, thank you, thank you!"
        $ ahsokaExpression = 9
        a "I'll store it away somewhere safe. I'll return it back to the Jedi once I'm free."
        if theLie >= 1:
            y "..................."
        hide screen ahsoka_main
        with d3
        jump exitInventory
    else:
        $ ahsokaExpression = 9
        $ mood += 30
        a "Thank you, [playerName]! I can't wait to bring this back to the Jedi!"
        "Ahsoka's mood has improved."
        hide screen ahsoka_main
        with d3
        jump exitInventory
            
label invInspArtifact:
    "An ancient Jedi artifact."
    jump inventory
    
################################################################
########################## # 26 Plushie  ###########################
################################################################
define firstTimeBantha = False

screen bantha:
    add "UI/items/item25bird.png"
    
screen menuitem26banthaSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuitem26banthaSelect"), Jump("giftBantha")
        textbutton "Inspect":
            clicked Hide("menuitem26banthaSelect"), Jump("invInspBantha")
        textbutton "Cancel":
            action Hide("menuitem26banthaSelect")
            
label giftBantha:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 9
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        $ ahsokaExpression = 11
        a "Is that a...?!"
        a "Giant Bantha plushie?!"
        $ ahsokaExpression = 4
        a "I really want it, but...."
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeBantha == False:
        $ plushieBantha = True
        $ firstTimeBantha = True
        $ plushieToy -= 1
        $ mood += 35
        $ ahsokaExpression = 11
        a "Is that a...?!"
        a "Giant plushie Bantha?!"
        y "It sure is."
        a "But that's...! That's such a cool gift!"
        y "I'm glad you like it."
        $ ahsokaExpression = 9
        a "Thank you, [playerName]! It's cute, I'll keep it in my room!"
        hide screen ahsoka_main
        with d3
        jump exitInventory
    else:
        $ plushieBantha = True
        $ plushieToy = 0
        $ ahsokaExpression = 9
        $ mood += 30
        a "Hm, that's strange..."
        y "What's strange?"
        hide screen ahsoka_main
        with d3
        show screen devdroid_main
        with d3
        y "Argh!!"
        y "Who are you?!"
        "Dev Bot" "I am dev bot! I'm a remnant of the game's beta!"
        y "........................."
        "Dev Bot" "........................"
        y "So what do you wa-...."
        "Dev Bot" "It looks like you have more than 1 plushie bantha! You're only suppose to get one of these!"
        y "Really?"
        "Dev Bot" "Exiscoming programmed this bit just for such an occassion that you might end up with 2 plushies by accident."
        "Dev Bot" "Let's hope nothing else broke, am I right?!"
        y "You're still scaring me...."
        "Dev Bot" "Okay! Byyyeeeee~.....!"
        hide screen devdroid_main
        with d3
        show screen ahsoka_main
        with d3
        $ ahsokaExpression = 6
        a "W-what just happened?"
        y "I don't know!"
        "Ahsoka's mood has improved."
        hide screen ahsoka_main
        with d3
        jump exitInventory
            
label invInspBantha:
    "An gigantic Bantha plushie."
    jump inventory

################################################################
########################## #20 STRAP ON ########################
################################################################

screen strapOn:
    add "UI/items/item20StrapOn.png"
    
screen menuStrapOnSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Gift":
            clicked Hide("menuStrapOnSelect"), Jump("giftStrapOn")
        textbutton "Inspect":
            clicked Hide("menuStrapOnSelect"), Jump("invInspStrapOn")
        textbutton "Cancel":
            action Hide("menuStrapOnSelect")
            
label invInspStrapOn:
    "Spiced StrapOn Bar. A typical Republic candy."
    jump inventory

label giftStrapOn:
    "XXX"
    
    

    
################################################################
################# # 28 Christmas Evil Candy Cane  ####################
################################################################
define firstTimeCandyCaneEvil = False

screen candyCaneEvil:
    add "UI/items/item28CandyCaneEvil.png"
    
screen menuCandyCaneEvilSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 10)
        has vbox
        textbutton "Gift":
            clicked Hide("menuCandyCaneEvilSelect"), Jump("giftCandyCaneEvil")
        textbutton "Inspect":
            clicked Hide("menuCandyCaneEvilSelect"), Jump("invInspCandyCaneEvil")
        textbutton "Cancel":
            action Hide("menuCandyCaneEvilSelect")
            
label giftCandyCaneEvil:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeCandyCaneEvil == False:
        $ ahsokaExpression = 21
        $ firstTimeCandyCaneEvil = True
        $ candyCaneEvil -= 1
        $ mood += 5
        y "Ehrm.... here you go."
        a "What's this...?"
        y "I'm not quite sure. It appears to be a double sided Candy Cane."
        $ ahsokaExpression = 6
        a "It looks evil..."
        y "Yup. Well it's yours now."
        $ ahsokaExpression = 16
        a "Er... thanks?"
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    else:
        $ ahsokaExpression = 6
        $ mood += 5
        $ candyCaneEvil -= 1
        a "These things creep me out!"
        y "Me too! Take it!"
        "Ahsoka accepts the candy cane."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    jump inventory
            
label invInspCandyCaneEvil:
    "A double sided candy cane. The worste of the holiday gifts!"
    jump inventory
    
################################################################
################## # 29 Christmas Candy Cane  ######################
################################################################
define firstTimeCandyCane = False

screen candyCane:
    add "UI/items/item29CandyCane.png"
    
screen menuCandyCaneSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 10)
        has vbox
        textbutton "Gift":
            clicked Hide("menuCandyCaneSelect"), Jump("giftCandyCane")
        textbutton "Inspect":
            clicked Hide("menuCandyCaneSelect"), Jump("invInspCandyCane")
        textbutton "Cancel":
            action Hide("menuCandyCaneSelect")
            
label giftCandyCane:
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    scene bgBridge with fade
    show screen scene_darkening
    with d3
    $ ahsokaExpression = 21
    show screen ahsoka_main
    with d3
    if addictDrug >= 4:
        a "Could... I maybe have some Death Sticks instead?"
        y "Listen, you know those things are bad for you, right? They literally have the word 'death' in them."
        a "............................."
        "Ahsoka refused your gift and seems disappointed to not have anymore Death Sticks"
        jump exitInventory
    if firstTimeCandyCane == False:
        $ ahsokaExpression = 21
        $ firstTimeCandyCane = True
        $ candyCane -= 1
        $ mood += 5
        y "Here you go. Merry Life-Day."
        a "What's this?"
        y "What do you mean 'whats this'? It's a candy cane."
        y ".........."
        y "You've never had a candy cane before?"
        "Ahsoka shakes her head."
        y "Do the Jedi even celebrate Life-Day?"
        $ ahsokaExpression = 20
        a "Of course we do! Only without the candy. {w} And without the presents.{w} And without the Life-Day dinner."
        a ".........."
        $ ahsokaExpression = 9
        a "But I mean, we put up the Life-Day trees! So yeah, we celebrate!"
        "Ahsoka thanks you for the gift."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    else:
        $ mood += 5
        $ candyCane -= 1
        a "Sure, I don't mind getting into the spirit of things."
        "Ahsoka accepts the candy cane."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        jump exitInventory
    jump inventory
            
label invInspCandyCane:
    "A common Life-Day candy. Don't forget to brush your teeth afterwards."
    jump inventory
    
################################################################
#################### # 30 Christmas Coal  #######################
################################################################
define donatedCoal = 0
define rayGunStatus = 0

screen xmasRaygun:
    if rayGunStatus == 1:
        add "UI/raycannon1.png"
    if rayGunStatus == 2:
        add "UI/raycannon2.png"
    if rayGunStatus == 3:
        add "UI/raycannon3.png"

screen coal:
    add "UI/items/item30Coal.png"
    
screen menuCoalSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 10)
        has vbox
        textbutton "Fuel the Raygun":
            clicked Hide("menuCoalSelect"), Jump("giftCoal")
        textbutton "Inspect":
            clicked Hide("menuCoalSelect"), Jump("invInspCoal")
        textbutton "Cancel":
            action Hide("menuCoalSelect")
            
label giftCoal:
    if christmas == False:
        "Santa Claus has left and there appears to be no more use for this. Toss it?"
        menu:
            "Yes":
                $ coal = 0
                jump inventory
            "No":
                jump inventory
    else:
        pass
    "How much coal would you like to deposit in the coal powered Raygun?"
    menu:
        "1 lump of coal" if coal >= 1:
            $ coal -= 1
            $ donatedCoal += 1
            "You drop off one lump of coal in the furnace."
        "10 lumps of coal" if coal >= 10:
            $ coal -= 10
            $ donatedCoal += 10
            "You drop off ten lumps of coal in the furnace."
        "20 lumps of coal" if coal >= 20:
            $ coal -= 20
            $ donatedCoal += 20
            "You drop off twenty lumps of coal in the furnace."
        "30 lumps of coal" if coal >= 30:
            $ coal -= 30
            $ donatedCoal += 30
            "You drop off thirty lumps of coal in the furnace."
        "40 lumps of coal" if coal >= 40:
            $ coal -= 40
            $ donatedCoal += 40
            "You drop off forty lumps of coal in the furnace."
            
    "Cannon: [donatedCoal]/80"
    if donatedCoal >= 80:
        $ christmasPresent += 10
        $ donatedCoal = 0
        $ gearChristmasActive = True 
        $ gearChristmasShinActive = True
        $ gearChristmasKitActive = True
        hide screen inventory
        hide screen ahsoka_main
        hide screen shin_main
        hide screen kit_main
        hide screen invAll
        scene bgBridge
        with d3
        show screen scene_darkening
        with d3
        pause 0.5
        show screen jason_main
        with d3
        mr "That should be enough coal, master."
        y "How's progress coming along on the cannon?"
        mr "It is just about finished. Feel free to fire it whenever you plea-...."
        stop music fadeout 1.0
        show screen scene_red
        with d3
        mr "Master. Incoming transmission."
        play music "audio/music/action1.mp3"
        hide screen jason_main
        with d3
        pause 0.5
        show screen santa
        with d3
        "Santa Claus" "Ho ho ho! How have you been, you little bitch boy?!"
        y "You again!? You'll get what's coming to you, you senile old bag!"
        y "Jason, the cannon!"
        hide screen santa
        with d3
        $ rayGunStatus = 1
        hide screen scene_darkening
        hide screen jason_main
        with d3
        hide screen scene_red
        scene black with fade
        show screen itemBackground
        with d5
        show screen xmasRaygun
        with d5
        pause
        y "Yes! Be ready to taste my vengence!!!"
        y "Aim~....."
        $ rayGunStatus = 2
        with d3
        pause
        y "Aaaaand~...."
        "Santa Claus" "Ho ho ho...?"
        menu:
            "FIRE!":
                pass
            "FIRE!":
                pass
            "FIRE!":
                pass
        y "FIRE!"
        $ rayGunStatus = 3
        with flashbulb
        with d2
        pause
        "Santa Claus" "HO HO HOOOOOOOOOOOOOOoooooooo{size=-6}oooooooooo{/size}{size=-10}ooooo~.....!{/size}"
        hide screen itemBackground
        hide screen xmasRaygun
        scene white with flash
        stop music fadeout 2.0
        pause 2.0
        scene bgBridge with d5
        pause 1.0
        show screen scene_darkening
        with d3
        pause 1.0
        show screen jason_main
        with d3
        pause 0.5
        y "Is it done?"
        mr "................"
        mr "One moment master, I am picking up another transmission."
        hide screen jason_main
        with d2
        "{b}*Static*{/b}"
        "???" "You... {b}*Static*{/b} ...be that easy... {b}*Static*{/b} ...you?"
        y "Is that...?"
        santa "You can't kill Santa Claus! As long as there are sluts in this universe who haven't been shamed. I shall never die!"
        y "Damn it!"
        show screen jason_main
        with d3
        mr "The signal was lost master."
        y "..............."
        mr "Don't worry sir. We didn't leave completely empty handed. Before he jumped to lightspeed, he had to dump his cargo."
        hide screen jason_main
        with d3
        hide screen scene_darkening
        with d3
        pause 0.5
        show screen itemBackground
        with d5
        show screen outfit15Ahsoka
        with d5
        play sound "audio/sfx/itemGot.mp3"
        "You got {color=#FFE653}Ahsoka's Life-Day Outfit{/color}!"
        hide screen outfit15Ahsoka
        with d3
        play sound "audio/sfx/itemGot.mp3"
        show screen outfit15Shin
        with d3
        if shinActive:
            "You got {color=#FFE653}Shin's Life-Day Outfit{/color}!"
        else:
            "You got {color=#FFE653}Somebody's Life-Day Outfit{/color}!"
        hide screen outfit15Shin
        with d3
        show screen outfit15Kit
        with d3
        play sound "audio/sfx/itemGot.mp3"
        if kitActive:
            "You got {color=#FFE653}Kit's Life-Day Outfit{/color}!"
        else:
            "You got {color=#FFE653}Somebody's Life-Day Outfit{/color}!"
        hide screen outfit15Kit
        with d3
        show screen christmasPresent
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You got {color=#FFE653}Life Day Present{/color} x10!"
        hide screen christmasPresent
        with d3
        hide screen itemBackground
        with d3
        show screen scene_darkening
        with d3
        show screen jason_main
        with d3
        mr "Ah yes, presents. The true meaning behind Life Day. For our many strives in life, we will always have consumerism to cheer us up."
        y "I guess so. {w}Merry Life Day, Mr. Jason."
        mr "Merry Life Day, sir."
        hide screen scene_darkening
        hide screen jason_main
        with d5
        $ renpy.pause(1.5, hard='True')
        scene white with d5
        play music "audio/music/xmas.mp3" fadein 2.0
        scene xmasGreetings 
        with d3
        pause
        stop music fadeout 2.0
        scene bgBridge 
        with fade
        play music "audio/music/soundBasic.mp3" fadein 2.0
        jump bridge
    else:
        jump inventory
            
label invInspCoal:
    "A lump of coal. Appropriate."
    jump inventory
    
################################################################
#################### # 31 Christmas Gift  #######################
################################################################

screen christmasPresent:
    add "UI/items/item31christmasPresent.png"
    
screen menuChristmasPresentSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 10)
        has vbox
        textbutton "Open":
            clicked Hide("menuChristmasPresentSelect"), Jump("giftChristmasPresent")
        textbutton "Inspect":
            clicked Hide("menuChristmasPresentSelect"), Jump("invInspChristmasPresent")
        textbutton "Cancel":
            action Hide("menuChristmasPresentSelect")
            
label giftChristmasPresent:
    $ christmasPresent -= 1
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    show screen itemBackground
    with d3
    show screen christmasPresent
    with d5
    pause
    hide screen christmasPresent
    with d5
    $ randomPresent = renpy.random.randint(4, 15)
    if randomPresent == 1:
        if gearChristmasActive == False:
            $ gearChristmasActive = True
            show screen outfit15Ahsoka
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka's Life-Day Outfit{/color}!"
            hide screen outfit15Ahsoka
            with d3
        else:
            show screen coal
            with d3
            $ coal += 1
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x1"
            hide screen coal
            with d3
    if randomPresent == 2:
        if gearChristmasShinActive == False:
            $ gearChristmasShinActive = True
            show screen outfit15Shin
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Shin'na's Life-Day Outfit{/color}!"
            hide screen outfit15Shin
            with d3
        else:
            show screen coal
            with d3
            $ coal += 2
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x3"
            hide screen coal
            with d3
    if randomPresent == 3:
        if gearChristmasKitActive == False:
            $ gearChristmasKitActive = True
            show screen outfit15Kit
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Kit's Life-Day Outfit{/color}!"
            hide screen outfit15Kit
            with d3
        else:
            show screen coal
            with d3
            $ coal += 2
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x2"
            hide screen coal
            with d3
    if randomPresent == 4:
        if ahsokaXmasHat == False:
            $ ahsokaXmasHat = True
            show screen acc15Xmas
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka's Life-Day Hat{/color}!"
            hide screen acc15Xmas
            with d3
        else:
            show screen coal
            with d3
            $ coal += 1
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x1"
            hide screen coal
            with d3
    if randomPresent == 5:
        if shinXmasHatActive == False:
            $ shinXmasHatActive = True
            show screen acc15ShinXmasHat
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Shin's Life-Day Hat{/color}!"
            hide screen acc15ShinXmasHat
            with d3
        else:
            show screen coal
            with d3
            $ coal += 2
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x2"
            hide screen coal
            with d3
    if randomPresent == 6:
        if kitXmasHat == False:
            $ kitXmasHat = True
            show screen acc15KitXmasHat
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Kit's Life-Day Hat{/color}!"
            hide screen acc15KitXmasHat
            with d3
        else:
            show screen coal
            with d3
            $ coal += 3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x3"
            hide screen coal
            with d3
    if randomPresent == 7:
        if tattoo5 == False:
            $ tattoo5 = True
            show screen xmasTattoos
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka Life-Day Tattoos{/color}!"
            hide screen xmasTattoos
            with d3
        else:
            show screen coal
            with d3
            $ coal += 5
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x5"
            hide screen coal
            with d3
    if  8 <= randomPresent <= 12:
        $ candyCane += 1
        show screen candyCane
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Candy Cane{/color} x1!"
        hide screen candyCane
        with d3
    if  randomPresent == 13:
        if modXmasLekkuActive == False:
            $ modXmasLekkuActive = True
            show screen modXmasLekku
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Christmas Lekku{/color} modification!"
            hide screen modXmasLekku
            with d3
    if  randomPresent == 14:
        $ candyCaneEvil += 1
        show screen candyCaneEvil
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Double Sided Candy Cane{/color} x1!"
        hide screen candyCaneEvil
        with d3
    if  randomPresent == 15:
        if firstTimeBird == False and bird == 0:
            $ bird += 1
            show screen convorBird
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received a {color=#FFE653}Convor{/color}!"
            y "......................"
            y "How long were you in that box for? Could you even breath in there?"
            "Convor" "{b}*Tjirp*{/b}"
            hide screen convorBird
            with d3
        else:
            show screen coal
            with d3
            $ coal += 2
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Lump of Coal{/color}! x5"
            hide screen coal
            with d3
    
    hide screen itemBackground
    with d3
    jump inventory
    
label invInspChristmasPresent:
    "A colorfully packaged gift for Life Day."
    jump inventory
    
    
################################################################
#################### # 32 Halloween Pumpkin  #######################
################################################################

screen treatBag:
    add "UI/items/item32TreatBag.png"
    
screen menuTreatBagSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 10)
        has vbox
        textbutton "Open":
            clicked Hide("menuTreatBagSelect"), Jump("giftTreatBag")
        textbutton "Inspect":
            clicked Hide("menuTreatBagSelect"), Jump("invInspTreatBag")
        textbutton "Cancel":
            action Hide("menuTreatBagSelect")
            
label giftTreatBag:
    $ TreatBag -= 1
    hide screen ahsoka_main
    hide screen kit_main
    hide screen shin_main
    hide screen invAll
    show screen itemBackground
    with d3
    show screen treatBag
    with d5
    pause
    hide screen treatBag
    with d5
    $ randomPumpkin = renpy.random.randint(1, 12)
    if randomPumpkin == 1:
        $ chocolate += 1
        show screen chocolate
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
        hide screen chocolate
        with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 2:
        $ chips += 1
        show screen chips
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Stasis Chips{/color} x1!"
        hide screen chips
        with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 3:
        $ gum += 1
        show screen gum
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Naboo Gum{/color} x1!"
        hide screen gum
        with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 4:
        $ cookies += 1
        show screen cookies
        with d3
        play sound "audio/sfx/itemGot.mp3"
        "You received {color=#FFE653}Naboo Girlscout Cookies{/color} x1!"
        hide screen cookies
        with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 5:
        if gearHalloweenActive == False:
            $ gearHalloweenActive = True
            show screen outfit16Ahsoka
            with d3
            pause
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka's Halloween Outfit{/color}!"
            hide screen outfit16Ahsoka
            with d3
        else:
            $ chocolate += 1
            show screen chocolate
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
            hide screen chocolate
            with d3
            hide screen itemBackground
            with d2
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 6:
        if shinActive == True:
            if gearHalloweenShinActive == False:
                $ gearHalloweenShinActive = True
                show screen outfit16Shin
                with d3
                pause
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Shin's Halloween Outfit{/color}!"
                hide screen outfit16Shin
                with d3
            else:
                $ chocolate += 1
                show screen chocolate
                with d3
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
                hide screen chocolate
                with d3
                hide screen itemBackground
                with d2
        if shinActive == False:
            $ chocolate += 1
            show screen chocolate
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
            hide screen chocolate
            with d3
            hide screen itemBackground
            with d2
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 7:
        if kitActive == True:
            if gearHalloweenKitActive == False:
                $ gearHalloweenKitActive = True
                show screen outfit16Kit
                with d3
                pause
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Kit's Halloween Outfit{/color}!"
                hide screen outfit16Kit
                with d3
            else:
                $ chocolate += 1
                show screen chocolate
                with d3
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
                hide screen chocolate
                with d3
                hide screen itemBackground
                with d2
            hide screen itemBackground
        if kitActive == False:
            $ chocolate += 1
            show screen chocolate
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
            hide screen chocolate
            with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 8:
        if kitActive == True:
            if kitHatActive == False:
                $ kitHatActive = True
                show screen acc01KitHat
                with d3
                pause
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Kit's Halloween Hat{/color}!"
                hide screen acc01KitHat
                with d3
            else:
                $ cookies += 1
                show screen cookies
                with d3
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Naboo Girlscout Cookies{/color} x1!"
                hide screen cookies
                with d3
                hide screen itemBackground
                with d2
            hide screen itemBackground
        else:
            $ cookies += 1
            show screen cookies
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Naboo Girlscout Cookies{/color} x1!"
            hide screen cookies
            with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 9:
        if shinActive == True:
            if shinHalloweenMaskActive == False:
                $ shinHalloweenMaskActive = True
                show screen acc16ShinHalloweenMask
                with d3
                pause
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Shin's Halloween Mask{/color}!"
                hide screen acc16ShinHalloweenMask
                with d3
            else:
                $ cookies += 1
                show screen cookies
                with d3
                play sound "audio/sfx/itemGot.mp3"
                "You received {color=#FFE653}Naboo Girlscout Cookies{/color} x1!"
                hide screen cookies
                with d3
                hide screen itemBackground
                with d2
            hide screen itemBackground
        else:
            $ cookies += 1
            show screen cookies
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Naboo Girlscout Cookies{/color} x1!"
            hide screen cookies
            with d3
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 10:
        if modHalloweenLekkuActive == False:
            $ modHalloweenLekkuActive = True
            show screen modHalloweenLekku
            with d3
            pause
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka's Halloween Lekku{/color}!"
            hide screen modHalloweenLekku
            with d3
        else:
            $ chocolate += 1
            show screen chocolate
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Republic Spiced Chocolate{/color} x1!"
            hide screen chocolate
            with d3
            hide screen itemBackground
            with d2
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 11:
        if tattoo4 == False:
            $ tattoo4 = True
            show screen halloweenTattoos
            with d3
            pause
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Tattoo Pattern #4{/color}!"
            hide screen halloweenTattoos
            with d3
        else:
            $ gum += 1
            show screen gum
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Naboo Gum{/color} x1!"
            hide screen gum
            with d3
            hide screen itemBackground
            with d2
        hide screen itemBackground
        with d2
        
    if randomPumpkin == 12:
        if gearHalloween2AhsokaActive == False:
            $ gearHalloween2AhsokaActive = True
            show screen halloween2Ahsoka
            with d3
            pause
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Ahsoka's Orange Costume{/color}!"
            hide screen halloween2Ahsoka
            with d3
        else:
            $ gum += 1
            show screen gum
            with d3
            play sound "audio/sfx/itemGot.mp3"
            "You received {color=#FFE653}Naboo Gum{/color} x1!"
            hide screen gum
            with d3
            hide screen itemBackground
            with d2
        hide screen itemBackground
        with d2
        
    jump inventory
            
label invInspTreatBag:
    "A hollowed out pumpkin full of candy! It's your lucky day!"
    jump inventory
    
    
############################# ACCESSORIES ##################################
    
###############################################################
################### #1 - Ahsoka -  Slave headress ###################
###############################################################   

###############################################################
######################## #15 - Ahsoka - Xmas ######################
###############################################################   

screen acc15Xmas:
    add "UI/items/acc15AhsokaXmas.png"
    
screen menuAhsokaXmasHatSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaXmasHatSelect"), Jump("invInspAhsokaXmasHat")
        textbutton "Cancel":
            action Hide("menuAhsokaXmasHatSelect")
            
label invInspAhsokaXmasHat:
    if accessories == 15:
        $ accessories  = 0
        $ accessoriesSet = 0
        jump inventory
    else:
        $ accessories = 15
        $ accessoriesSet = 15
        jump inventory
            

    
###############################################################
######################### #1 - Shin -  Mask ########################
###############################################################   
define maskActive = False

screen acc01Mask:
    add "UI/items/acc01Mask.png"
    
screen menuMaskSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuMaskSelect"), Jump("invInspMask")
        textbutton "Cancel":
            action Hide("menuMaskSelect")
            
label invInspMask:
    if shinMask == 1:
        $ shinMask = 0
        jump inventory
    if shinMask == 0:
        $ shinMask = 1
        jump inventory
    
###############################################################
##################### #2 - ALL - BLINDFOLD #######################
###############################################################

screen acc02Blindfold:
    add "UI/items/acc02Blindfold.png"

            
screen menuBlindfoldSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuBlindfoldSelect"), Jump("invInspBlindfold")
        textbutton "Cancel":
            action Hide("menuBlindfoldSelect")
            
label invInspBlindfold:
    if inventoryCurrentCharacter == 0:
        if accessories == 3:
            $ accessories = 0
            $ accessoriesSet = 0
            jump inventory
        if accessories != 3:
            $ accessories = 3
            $ accessoriesSet = 3
            jump inventory
    if inventoryCurrentCharacter == 1:
        if accessoriesShin == 3:
            $ accessoriesShin = 0
            jump inventory
        if accessoriesShin != 3:
            $ accessoriesShin = 3
            jump inventory
    if inventoryCurrentCharacter == 2:
        if accessoriesKit == 3:
            $ accessoriesKit = 0
            jump inventory
        if accessoriesKit != 3:
            $ accessoriesKit = 3
            jump inventory
    
    
###############################################################
####################### #3 - shin - Headband Shin ##################
###############################################################   
define headbandActive = False

screen acc01Headband:
    add "UI/items/acc03Headband.png"
    
screen menuHeadbandSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuHeadbandSelect"), Jump("invInspHeadband")
        textbutton "Cancel":
            action Hide("menuHeadbandSelect")
            
label invInspHeadband:
    if accessoriesShin == 1:
        $ accessoriesShin = 0
        jump inventory
    if accessoriesShin != 1:
        $ accessoriesShin = 1
        jump inventory
        
        
###############################################################
##################### #4 - ALL - Tail  #######################
###############################################################

screen acc04Tail:
    add "UI/items/acc04tail.png"
    
screen acc05Tail:
    add "UI/items/acc05tail.png"
            
screen menuTailSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuTailSelect"), Jump("invInspTail")
        textbutton "Cancel":
            action Hide("menuTailSelect")
            
label invInspTail:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 4:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 4:
            $ accessories2 = 4
            jump inventory
    if inventoryCurrentCharacter == 1:
        if accessories2Shin == 4:
            $ accessories2Shin = 0
            jump inventory
        if accessories2Shin != 4:
            $ accessories2Shin = 4
            jump inventory
    if inventoryCurrentCharacter == 2:
        if accessories2Kit == 4:
            $ accessories2Kit = 0
            jump inventory
        if accessories2Kit != 4:
            $ accessories2Kit = 4
            jump inventory
            
            
###############################################################
######################### #5 - Collars  ###########################
###############################################################
            
screen menuCollar1Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuCollar1Select"), Jump("invInspCollar1")
        textbutton "Cancel":
            action Hide("menuCollar1Select")
            
label invInspCollar1:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 7:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 7:
            $ accessories2 = 7
            jump inventory
            
            
screen menuCollar2Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuCollar2Select"), Jump("invInspCollar2")
        textbutton "Cancel":
            action Hide("menuCollar2Select")
            
label invInspCollar2:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 8:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 8:
            $ accessories2 = 8
            jump inventory
            
            
screen menuCollar3Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuCollar3Select"), Jump("invInspCollar3")
        textbutton "Cancel":
            action Hide("menuCollar3Select")
            
label invInspCollar3:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 9:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 9:
            $ accessories2 = 9
            jump inventory
            
            
screen menuCollar4Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuCollar4Select"), Jump("invInspCollar4")
        textbutton "Cancel":
            action Hide("menuCollar4Select")
            
label invInspCollar4:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 10:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 10:
            $ accessories2 = 10
            jump inventory
            
            
screen menuAhsokaStockingsBWSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaStockingsBWSelect"), Jump("invInspBWStockings")
        textbutton "Cancel":
            action Hide("menuAhsokaStockingsBWSelect")
            
label invInspBWStockings:
    if inventoryCurrentCharacter == 0:
        if accessories3 == 1:
            $ accessories3 = 0
            jump inventory
        if accessories3 != 1:
            $ accessories3 = 1
            jump inventory
    else:
        "eror"
            
            
################################################################
############################# #5 GAG ###########################
################################################################
define firstTimeGag = False

screen gag:
    add "UI/items/acc05Gag.png"
    
screen menuGagSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuGagSelect"), Jump("invInspGag")
        textbutton "Cancel":
            action Hide("menuGagSelect")
            
label invInspGag:
    if inventoryCurrentCharacter == 0:
        if accessories2 == 5:
            $ accessories2 = 0
            jump inventory
        if accessories2 != 5:
            $ accessories2 = 5
            jump inventory
    if inventoryCurrentCharacter == 1:
        if accessories2Shin == 5:
            $ accessories2Shin = 0
            jump inventory
        if accessories2Shin != 5:
            $ accessories2Shin = 5
            jump inventory
    if inventoryCurrentCharacter == 2:
        if accessories2Kit == 5:
            $ accessories2Kit = 0
            jump inventory
        if accessories2Kit != 5:
            $ accessories2Kit = 5
            jump inventory
            
################################################################
########################## #6 BUTTPLUG #########################
################################################################
define firstTimeButtplug = False
define buttPlugAhsoka = 0
define buttPlugShin = 0
define buttPlugKit = 0

screen buttplug:
    add "UI/items/item04ButtPlug.png"
    
screen menuButtplugSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuButtplugSelect"), Jump("invInspButtplug")
        textbutton "Cancel":
            action Hide("menuButtplugSelect")
            
label invInspButtplug:
    if inventoryCurrentCharacter == 0:
        $ buttPlugAhsoka = 1
        $ outfit = 0
        with d3
        $ ahsokaExpression = 4
        a "Ah~... {w}Ah~..."
        $ ahsokaExpression = 11
        a "OH!!!!"
        y "How's that feel?"
        a "It's so BIG!"
        y "Have fun walking around with that today."
        $ ahsokaExpression = 13
        a "Ngh~...."
        $ outfit = outfitSet
        with d3
        jump inventory
    if inventoryCurrentCharacter == 1:
        $ buttPlugShin = 1
        $ shinExpression = 13
        $ shinOutfit = 0
        with d3
        s "Oh dear...!"
        s "Slowly...~"
        with hpunch
        $ shinExpression = 31
        s "OHHH!!!"
        y "You have fun with that."
        $ shinExpression = 15
        s "Th-...{w} thank you, master...."
        $ shinOutfit = shinOutfitSet
        with d3
        jump inventory
    if inventoryCurrentCharacter == 2:
        $ buttPlugKit = 1
        $ kitOutfit = 0
        with d3
        k "Woooah, doggy! This is a big one!"
        k "Let me just~...."
        with hpunch
        k "Yeeehaw! That'll keep me filled up!"
        y "There ya go, you nutjob."
        $ kitOutfit = kitOutfitSet
        with d3
        jump inventory
###############################################################
######################## #15 - shin - Xmas Hat ####################
###############################################################   

screen acc15ShinXmasHat:
    add "UI/items/acc15ShinXmax.png"
    
screen menuShinXmasHatSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinXmasHatSelect"), Jump("invInspShinXmasHat")
        textbutton "Cancel":
            action Hide("menuShinXmasHatSelect")
            
label invInspShinXmasHat:
    if accessoriesShin == 15:
        $ accessoriesShin = 0
        jump inventory
    else:
        $ accessoriesShin = 15
        jump inventory
        
###############################################################
###################### #16 - shin - halloween mask ##################
###############################################################   

screen acc16ShinHalloweenMask:
    add "UI/items/acc16ShinHalloweenMask.png"
    
screen menuShinHalloweenMaskSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinHalloweenMaskSelect"), Jump("invInspShinHalloweenMask")
        textbutton "Cancel":
            action Hide("menuShinHalloweenMaskSelect")
            
label invInspShinHalloweenMask:
    if accessoriesShin == 2:
        $ accessoriesShin = 0
        jump inventory
    if accessoriesShin != 2:
        $ accessoriesShin = 2
        jump inventory
        
###############################################################
######################## #15 - kit - Xmas Hat ####################
###############################################################   

screen acc15KitXmasHat:
    add "UI/items/acc15KitXmaxHat.png"
    
screen menuKitXmasHatSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitXmasHatSelect"), Jump("invInspKitXmasHat")
        textbutton "Cancel":
            action Hide("menuKitXmasHatSelect")
            
label invInspKitXmasHat:
    if accessoriesKit == 15:
        $ accessoriesKit = 0
        jump inventory
    else:
        $ accessoriesKit = 15
        jump inventory
        
###############################################################
##################### #2 - Kit -  Halloween Hat #####################
###############################################################   

screen acc01KitHat:
    add "UI/items/acc01KitHat.png"
    
screen menuKitHatSelect():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitHatSelect"), Jump("invInspKitHat")
        textbutton "Cancel":
            action Hide("menuKitHatSelect")
            
label invInspKitHat:
    if accessoriesKit == 1:
        $ accessoriesKit = 0
        jump inventory
    else:
        $ accessoriesKit = 1
        jump inventory
    
############################# OUTFITS ##################################

###############################################################
##################### #0 Ahsoka Underwear #######################
###############################################################
            
screen menuAhsokaOutfit0Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit0Select"), Jump("invUseAhsokaOutfit0")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit0Select"), Jump("invInspAhsokaOutfit0")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit0Select")
            
label invInspAhsokaOutfit0:
    y "Ahsoka's underwear...."
    y "Why am I checking out Ahsoka's underwear? That's kinda creepy."
    jump inventory
    
label invUseAhsokaOutfit0:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ underwearTop = 1
            $ underwearBottom = 1
            jump inventory
        "No top" if ahsokaSlut >= 20:
            $ underwearTop = 0
            $ underwearBottom = 1
            jump inventory
        "No bottom" if ahsokaSlut >= 23:
            $ underwearTop = 1
            $ underwearBottom = 0
            jump inventory
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory
            
###############################################################
#################### #9 Ahsoka Underwear2 ######################
###############################################################
            
screen menuAhsokaOutfit9Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit9Select"), Jump("invUseAhsokaOutfit9")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit9Select"), Jump("invInspAhsokaOutfit9")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit9Select")
            
label invInspAhsokaOutfit9:
    "Sexy underwear for Ahsoka."
    jump inventory
    
label invUseAhsokaOutfit9:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ underwearTop = 3
            $ underwearBottom = 3
            jump inventory
        "Sexy" if ahsokaSlut >= 20:
            $ underwearTop = 4
            $ underwearBottom = 4
            jump inventory
        "No top" if ahsokaSlut >= 20:
            $ underwearTop = 0
            $ underwearBottom = 3
            jump inventory
        "No bottom" if ahsokaSlut >= 23:
            $ underwearTop = 3
            $ underwearBottom = 0
            jump inventory
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory

###############################################################
######################## #1 Ahsoka Basic #########################
###############################################################
            
screen menuAhsokaOutfit1Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit1Select"), Jump("invUseAhsokaOutfit1")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit1Select"), Jump("invInspAhsokaOutfit1")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit1Select")
            
label invInspAhsokaOutfit1:
    "This seems to be Ahsoka's go-to outfit."
    jump inventory
    
label invUseAhsokaOutfit1:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 1
            $ outfitSet = 1
            jump inventory
        "No Leggings" if ahsokaSlut >= 17:
            $ armorEquipped = False
            $ outfit = 2
            $ outfitSet = 2
            jump inventory
        "Short Skirt" if ahsokaSlut >= 17:
            $ armorEquipped = False
            $ outfit = 3
            $ outfitSet = 3
            jump inventory
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory

###############################################################
######################## #2 Ahsoka Fastfood ######################
###############################################################
            
screen menuAhsokaOutfit2Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit2Select"), Jump("invUseAhsokaOutfit2")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit2Select"), Jump("invInspAhsokaOutfit2")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit2Select")
            
label invInspAhsokaOutfit2:
    "The standard uniform worn by all Lekka Lekku employees."
    jump inventory
    
label invUseAhsokaOutfit2:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 5
            $ outfitSet = 5
            jump inventory
        "No Leggings" if ahsokaSlut >= 17:
            $ armorEquipped = False
            $ outfit = 6
            $ outfitSet = 6
            jump inventory
        "Revealing" if ahsokaSlut >= 17:
            $ armorEquipped = False
            $ outfit = 7
            $ outfitSet = 7
            jump inventory     
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory

###############################################################
######################## #3 Ahsoka slave #########################
###############################################################
            
screen menuAhsokaOutfit3Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 30)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit3Select"), Jump("invUseAhsokaOutfit3")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit3Select"), Jump("invInspAhsokaOutfit3")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit3Select")
            
label invInspAhsokaOutfit3:
    "A sexy dressed used by slave girls to please their masters."
    jump inventory
    
label invUseAhsokaOutfit3:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 8
            $ outfitSet = 8
            jump inventory
        "No underwearTop" if ahsokaSlut >= 17:
            $ armorEquipped = False
            $ underwearTop = 0
            $ outfit = 10
            $ outfitSet = 10
            jump inventory     
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfit = 0
            jump inventory
            
###############################################################
######################## #4 Ahsoka Pitgirl outift ####################
###############################################################
            
screen menuAhsokaOutfit4Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit4Select"), Jump("invUseAhsokaOutfit4")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit4Select"), Jump("invInspAhsokaOutfit4")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit4Select")
            
label invInspAhsokaOutfit4:
    "A tight pitgirl outfit used by the girls over on Tatooine."
    jump inventory
    
label invUseAhsokaOutfit4:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 11
            $ outfitSet = 11
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory
            
            
            
###############################################################
######################## #5 Ahsoka Escort outift ####################
###############################################################
            
screen menuAhsokaOutfit5Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit5Select"), Jump("invUseAhsokaOutfit5")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit5Select"), Jump("invInspAhsokaOutfit5")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit5Select")
            
label invInspAhsokaOutfit5:
    "A sexy silken dress."
    jump inventory
    
label invUseAhsokaOutfit5:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfit = 12
            $ outfitSet = 12
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory
   
 
###############################################################
######################## #6 Ahsoka armor outift ####################
###############################################################
            
screen menuAhsokaOutfit6Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit6Select"), Jump("invUseAhsokaOutfit6")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit6Select"), Jump("invInspAhsokaOutfit6")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit6Select")
            
label invInspAhsokaOutfit6:
    "A mercenary power armor. Just when you thought a Jedi couldn't get anymore dangerous."
    jump inventory
    
label invUseAhsokaOutfit6:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ outfit = 18
            $ outfitSet = 18
            $ accessories1 = 0
            $ accessories2 = 0
            $ armorEquipped = True
            $ hair = 99
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
   
            
###############################################################
######################## #7 Ahsoka season3 outift ##################
###############################################################
            
screen menuAhsokaOutfit7Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit7Select"), Jump("invUseAhsokaOutfit7")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit7Select"), Jump("invInspAhsokaOutfit7")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit7Select")
            
label invInspAhsokaOutfit7:
    "Ahsoka's new outfit."
    jump inventory
    
label invUseAhsokaOutfit7:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 19
            $ outfitSet = 19
            jump inventory 
        "Sith":
            $ armorEquipped = False
            $ outfit = 20
            $ outfitSet = 20
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
###############################################################
########################## #8 Ahsoka bikini  ######################
###############################################################
            
screen menuAhsokaOutfit8Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit8Select"), Jump("invUseAhsokaOutfit8")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit8Select"), Jump("invInspAhsokaOutfit8")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit8Select")
            
label invInspAhsokaOutfit8:
    "A bikini given as a reward by Lady Nemthak."
    jump inventory
    
label invUseAhsokaOutfit8:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 21
            $ outfitSet = 21
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
###############################################################
########################## #9 Clone Trooper  ######################
###############################################################
define gearTrooperAhsoka = False

screen TrooperAhsoka:
    add "UI/items/outfit09Ahsoka.png"

screen menuAhsokaOutfit09Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit09Select"), Jump("invUseAhsokaOutfit09")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit09Select"), Jump("invInspAhsokaOutfit09")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit09Select")
            
label invInspAhsokaOutfit09:
    "The armor of a Clone Trooper with an exposed midriff for... extra mobility?"
    "A small label reads: {i}'Decorative use only'{/i}"
    jump inventory
    
label invUseAhsokaOutfit09:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 22
            $ outfitSet = 22
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
###############################################################
########################### #10 Slave girl 2  ######################
###############################################################
            
screen menuAhsokaOutfit10Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit10Select"), Jump("invUseAhsokaOutfit10")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit10Select"), Jump("invInspAhsokaOutfit10")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit10Select")
            
label invInspAhsokaOutfit10:
    "Ahsoka's harem room outfit."
    jump inventory
    
label invUseAhsokaOutfit10:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 23
            $ outfitSet = 23
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
###############################################################
########################## #11 Nightwearl  #######################
###############################################################
            
screen menuAhsokaOutfit11Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit11Select"), Jump("invUseAhsokaOutfit11")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit11Select"), Jump("invInspAhsokaOutfit11")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit11Select")
            
label invInspAhsokaOutfit11:
    "Go to bed, you must. Yoda T-shirt perfect, it is!"
    jump inventory
    
label invUseAhsokaOutfit11:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 13
            $ outfitSet = 13
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
            
###############################################################
############################ #12 Orange  ########################
###############################################################
            
screen menuAhsokaOutfit12Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 40)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit12Select"), Jump("invUseAhsokaOutfit12")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit12Select"), Jump("invInspAhsokaOutfit12")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit12Select")
            
label invInspAhsokaOutfit12:
    "Hah hah! Get it? It's a giant orange costume!"
    "Cause she's orange!"
    "............."
    "Yeah I've got nothing."
    jump inventory
    
label invUseAhsokaOutfit12:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ outfit = 24
            $ outfitSet = 24
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            $ lekku = 1
            jump inventory
            
###############################################################
######################## #15 Ahsoka Christmas ####################
###############################################################

screen outfit15Ahsoka:
    add "UI/items/outfit15Ahsoka.png"
            
screen menuAhsokaOutfit15Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit15Select"), Jump("invUseAhsokaOutfit15")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit15Select"), Jump("invInspAhsokaOutfit15")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit15Select")
            
label invInspAhsokaOutfit15:
    "A cute Life Day outfit."
    jump inventory
    
label invUseAhsokaOutfit15:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfit = 15
            $ outfitSet = 15
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory
            
###############################################################
######################## #16 Ahsoka Halloween ####################
###############################################################

screen outfit16Ahsoka:
    add "UI/items/outfit16Ahsoka.png"
            
screen menuAhsokaOutfit16Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuAhsokaOutfit16Select"), Jump("invUseAhsokaOutfit16")
        textbutton "Inspect":
            clicked Hide("menuAhsokaOutfit16Select"), Jump("invInspAhsokaOutfit16")
        textbutton "Cancel":
            action Hide("menuAhsokaOutfit16Select")
            
label invInspAhsokaOutfit16:
    "It's tradition to wear something sexy during Halloween."
    jump inventory
    
label invUseAhsokaOutfit16:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ armorEquipped = False
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfit = 16
            $ outfitSet = 16
            jump inventory 
        "Take Off" if ahsokaSlut >= 23:
            $ armorEquipped = False
            $ outfit = 0
            $ underwearTop = 0
            $ underwearBottom = 0
            $ outfitSet = 0
            jump inventory
            
############################################################
############################# MISC. #########################
############################################################
screen bundleClothes:
    add "UI/items/bundleClothes.png"
screen metalJaw:
    add "UI/items/item03metalJaw.png"
screen item10Dildo:
    add "UI/items/item10Dildo.png"
    
            
###############################################################
######################## #1 Shin Basic #########################
###############################################################
define shinBasicActive = True
  
screen menuShinOutfit1Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit1Select"), Jump("invUseShinOutfit1")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit1Select"), Jump("invInspShinOutfit1")
        textbutton "Cancel":
            action Hide("menuShinOutfit1Select")
            
label invInspShinOutfit1:
    "Shin's basic outfit."
    jump inventory
    
label invUseShinOutfit1:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 1
            $ shinOutfitSet = 1
            jump inventory
        "No Shirt" if shinSocial >= 6:
            $ shinOutfit = 2
            $ shinOutfitSet = 2
            jump inventory
        "Naked" if shinSocial >= 8:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
            
###############################################################
######################### #2 Shin Prostitute ######################
###############################################################
define shinProsOutfit = False
  
screen menuShinOutfit2Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit2Select"), Jump("invUseShinOutfit2")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit2Select"), Jump("invInspShinOutfit2")
        textbutton "Cancel":
            action Hide("menuShinOutfit2Select")
            
label invInspShinOutfit2:
    "For when you {i}'really'{/i} need to get naughty!"
    jump inventory
    
label invUseShinOutfit2:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 3
            $ shinOutfitSet = 3
            jump inventory
        "Naked" if shinSocial >= 8:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
###############################################################
######################### #3 Shin Bikini ######################
###############################################################
define shinBikiniOutfit = False
  
screen menuShinOutfit3Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit3Select"), Jump("invUseShinOutfit3")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit3Select"), Jump("invInspShinOutfit3")
        textbutton "Cancel":
            action Hide("menuShinOutfit3Select")
            
label invInspShinOutfit3:
    "A stylish bikini designed by Zygerria's most prestigious designers."
    jump inventory
    
label invUseShinOutfit3:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 4
            $ shinOutfitSet = 4
            jump inventory
        "Naked" if shinSocial >= 8:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
###############################################################
#############$############ #4 Shin Trooper  $######################
###############################################################
define gearTrooperShin = False

screen TrooperShin:
    add "UI/items/outfit4Shin.png"
  
screen menuShinOutfit4Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit4Select"), Jump("invUseShinOutfit4")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit4Select"), Jump("invInspShinOutfit4")
        textbutton "Cancel":
            action Hide("menuShinOutfit4Select")
            
label invInspShinOutfit4:
    "Female clone trooper armor...{w} Wait, since when are there female clone troopers?."
    jump inventory
    
label invUseShinOutfit4:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 5
            $ shinOutfitSet = 5
            jump inventory
        "Naked" if shinSocial >= 8:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
###############################################################
#############$############ #4 Shin Slave  $######################
###############################################################
  
screen menuShinOutfit5Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit5Select"), Jump("invUseShinOutfit5")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit5Select"), Jump("invInspShinOutfit5")
        textbutton "Cancel":
            action Hide("menuShinOutfit5Select")
            
label invInspShinOutfit5:
    "Community suggested outfit."
    jump inventory
    
label invUseShinOutfit5:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 6
            $ shinOutfitSet = 6
            jump inventory
        "Naked" if shinSocial >= 8:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
###############################################################
######################## #15 Shin Christmas ######################
###############################################################

screen outfit15Shin:
    add "UI/items/outfit15Shin.png"

screen menuShinOutfit15Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit15Select"), Jump("invUseShinOutfit15")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit15Select"), Jump("invInspShinOutfit15")
        textbutton "Cancel":
            action Hide("menuShinOutfit15Select")
            
label invInspShinOutfit15:
    "Life Day comes with some pretty sexy benefits."
    jump inventory
    
label invUseShinOutfit15:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 15
            $ shinOutfitSet = 15
            jump inventory 
        "Take Off" if ahsokaSlut >= 31:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
###############################################################
######################## #16 Shin Halloween ####################
###############################################################

screen outfit16Shin:
    add "UI/items/outfit16Shin.png"

screen menuShinOutfit16Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuShinOutfit16Select"), Jump("invUseShinOutfit16")
        textbutton "Inspect":
            clicked Hide("menuShinOutfit16Select"), Jump("invInspShinOutfit16")
        textbutton "Cancel":
            action Hide("menuShinOutfit16Select")
            
label invInspShinOutfit16:
    "Lingerie. The perfect tool to get into the Halloween mood."
    jump inventory
    
label invUseShinOutfit16:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ shinOutfit = 16
            $ shinOutfitSet = 16
            jump inventory 
        "Take Off" if ahsokaSlut >= 31:
            $ shinOutfit = 0
            $ shinOutfitSet = 0
            jump inventory
            
            
###############################################################
########################## #1 Kit Basic ##########################
###############################################################
define kitBasicActive = True
  
screen menuKitOutfit1Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit1Select"), Jump("invUseKitOutfit1")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit1Select"), Jump("invInspKitOutfit1")
        textbutton "Cancel":
            action Hide("menuKitOutfit1Select")
            
label invInspKitOutfit1:
    "Kit's ragtag Exile outfit."
    jump inventory
    
label invUseKitOutfit1:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 1
            $ kitOutfitSet = 1
            jump inventory
        "Pulled up top":
            $ kitOutfit = 3
            $ kitOutfitSet = 3
            jump inventory
        "Naked":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
            
###############################################################
######################### #2 Kit Prostitute #########################
###############################################################
define kitProsOutfit = False
  
screen menuKitOutfit2Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit2Select"), Jump("invUseKitOutfit2")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit2Select"), Jump("invInspKitOutfit2")
        textbutton "Cancel":
            action Hide("menuKitOutfit2Select")
            
label invInspKitOutfit2:
    "Judging by how revealing and sexual it is, you're surprised she doesn't wear this everyday."
    jump inventory
    
label invUseKitOutfit2:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 4
            $ kitOutfitSet = 4
            jump inventory
        "Naked":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
###############################################################
########################## #3 Kit bikini ###########################
###############################################################
  
screen menuKitOutfit3Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit3Select"), Jump("invUseKitOutfit3")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit3Select"), Jump("invInspKitOutfit3")
        textbutton "Cancel":
            action Hide("menuKitOutfit3Select")
            
label invInspKitOutfit3:
    "A designer bikini, given by Lady Nemthak."
    jump inventory
    
label invUseKitOutfit3:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 6
            $ kitOutfitSet = 6
            jump inventory
        "Naked":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
###############################################################
########################## #4 Kit Trooper #########################
###############################################################
define gearTrooperKit = False

screen TrooperKit:
    add "UI/items/outfit4Kit.png"

screen menuKitOutfit4Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit4Select"), Jump("invUseKitOutfit4")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit4Select"), Jump("invInspKitOutfit4")
        textbutton "Cancel":
            action Hide("menuKitOutfit4Select")
            
label invInspKitOutfit4:
    "{i}'Hey, aren't you a little short for a Clonetrooper?'{/i}"
    jump inventory
    
label invUseKitOutfit4:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 7
            $ kitOutfitSet = 7
            jump inventory
        "Naked":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
###############################################################
########################## #4 Kit Slave #########################
###############################################################
  
screen menuKitOutfit5Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y + 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit5Select"), Jump("invUseKitOutfit5")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit5Select"), Jump("invInspKitOutfit5")
        textbutton "Cancel":
            action Hide("menuKitOutfit5Select")
            
label invInspKitOutfit5:
    "Community suggested outfit."
    jump inventory
    
label invUseKitOutfit5:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 8
            $ kitOutfitSet = 8
            jump inventory
        "Naked":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
###############################################################
######################## #15 Kit Christmas ####################
###############################################################

screen outfit15Kit:
    add "UI/items/outfit15Kit.png"

screen menuKitOutfit15Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit15Select"), Jump("invUseKitOutfit15")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit15Select"), Jump("invInspKitOutfit15")
        textbutton "Cancel":
            action Hide("menuKitOutfit15Select")
            
label invInspKitOutfit15:
    "Not edible. Trust me, Kit has already tried."
    jump inventory
    
label invUseKitOutfit15:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 15
            $ kitOutfitSet = 15
            jump inventory 
        "Take Off":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
###############################################################
######################## #16 Kit Halloween ####################
###############################################################

screen outfit16Kit:
    add "UI/items/outfit16Kit.png"

screen menuKitOutfit16Select():
    modal True
    default x = renpy.get_mouse_pos()[0]
    default y = renpy.get_mouse_pos()[1]
    
    frame:
        pos (x + 10, y - 20)
        has vbox
        textbutton "Equip":
            clicked Hide("menuKitOutfit16Select"), Jump("invUseKitOutfit16")
        textbutton "Inspect":
            clicked Hide("menuKitOutfit16Select"), Jump("invInspKitOutfit16")
        textbutton "Cancel":
            action Hide("menuKitOutfit16Select")
            
label invInspKitOutfit16:
    "Not edible. Trust me, Kit has already tried."
    jump inventory
    
label invUseKitOutfit16:
    "Which version would you like to equip?"
    menu:
        "Normal":
            $ kitOutfit = 16
            $ kitOutfitSet = 16
            jump inventory 
        "Take Off":
            $ kitOutfit = 0
            $ kitOutfitSet = 0
            jump inventory
            
            
###############################################################
############################ Modifications ########################
###############################################################


screen halloween2Ahsoka:
        add "UI/items/outfit12Ahsoka.png"

screen modHalloweenLekku:
        add "UI/items/modHalloweenLekku.png"
        
screen halloweenTattoos:
        add "UI/items/modHalloweenTattoos.png"
        
screen modXmasLekku:
        add "UI/items/modChristmasLekku.png"
        
screen xmasTattoos:
        add "UI/items/modXmasTattoos.png"
