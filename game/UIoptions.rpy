################################################################################################
####################################### WAR METER START ########################################
################################################################################################    
screen warTable:
    add "bgs/warTable.jpg"
    
    if fightersAvailable == 1:
        vbox xpos 680 ypos 13:
            add "UI/fighterDot.png"
    if fightersAvailable == 2:
        vbox xpos 680 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 700 ypos 13:
            add "UI/fighterDot.png"
    if fightersAvailable == 3:
        vbox xpos 680 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 700 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 720 ypos 13:
            add "UI/fighterDot.png"
    if fightersAvailable == 4:
        vbox xpos 680 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 700 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 720 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 740 ypos 13:
            add "UI/fighterDot.png"
    if fightersAvailable == 5:
        vbox xpos 680 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 700 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 720 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 740 ypos 13:
            add "UI/fighterDot.png"
        vbox xpos 760 ypos 13:
            add "UI/fighterDot.png"
    
    if target1 == 1:
        vbox xpos 55 ypos 40:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target1")
    if target2 == 1:
        vbox xpos 125 ypos 140:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target2")
    if target3 == 1:
        vbox xpos 235 ypos 200:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target3")
    if target4 == 1:
        vbox xpos 78 ypos 290:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target4")
    if target5 == 1:
        vbox xpos 635 ypos 390:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target5")
    if target6 == 1:
        vbox xpos 435 ypos 90:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target6")
    if target7 == 1:
        vbox xpos 635 ypos 240:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target7")
    if target8 == 1:
        vbox xpos 285 ypos 330:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target8")
    if target9 == 1:
        vbox xpos 135 ypos 35:
            imagebutton:
                idle "UI/target1.png"
                hover "UI/target1-hover.png"
                action Jump("target9")
    if target10 == 1:
        vbox xpos 420 ypos 390:
            imagebutton:
                idle "UI/target2.png"
                hover "UI/target2-hover.png"
                action Jump("target10")
    if target11 == 1:
        vbox xpos 79 ypos 210:
            imagebutton:
                idle "UI/target2.png"
                hover "UI/target2-hover.png"
                action Jump("target11")
                
    if target12 == 1:
        vbox xpos 280 ypos 260:
            imagebutton:
                idle "UI/hatTarget.png"
                hover "UI/hatTarget-hover.png"
                action Jump("target12")
                
    if target13 == 1:
        vbox xpos 260 ypos 67:
            imagebutton:
                idle "UI/hatTarget.png"
                hover "UI/hatTarget-hover.png"
                action Jump("target13")
                
    if target14 == 1:
        vbox xpos 479 ypos 300:
            imagebutton:
                idle "UI/hatTarget.png"
                hover "UI/hatTarget-hover.png"
                action Jump("target14")
                
    if target15 == 1:
        vbox xpos 19 ypos 190:
            imagebutton:
                idle "UI/target2.png"
                hover "UI/target2-hover.png"
                action Jump("target15")
                
    if 1 <= target16 <= 4:
        vbox xpos 390 ypos 20:
            imagebutton:
                idle "UI/target3.png"
                hover "UI/target3-hover.png"
                action Jump("target16")
                
    if 1 <= target17 <= 4:
        vbox xpos 360 ypos 370:
            imagebutton:
                idle "UI/target3.png"
                hover "UI/target3-hover.png"
                action Jump("target17")

define aidCost = 500
define scanAmount = 0   #amount of times scanned. If scanned more than 10, start adding CIS bounties

define target1 = 0
define target2 = 0
define target3 = 0
define target4 = 0
define target5 = 0
define target6 = 0
define target7 = 0
define target8 = 0
define target9 = 0
define target10 = 0
define target11 = 0

#christmas targets
define target12 = 0
define target13 = 0
define target14 = 0

#Dark side quest
define target15 = 0

#CIS raids
define target16 = 0
define target17 = 0
define target16Status = 0
define target17Status = 0
define CISraidCounter = False

image scan = "UI/scanLine.png"
transform scanMove:
    xalign 0.0 yalign 0.0
    linear 2.0 yalign 1.0
    repeat

# Fighter craft begin
image fighter1 = "bgs/fighter1.png"
image fighter2 = "bgs/fighter1.png"
image fighter3 = "bgs/fighter1.png"
image fighter4 = "bgs/fighter1.png"
image fighter5 = "bgs/fighter1.png"
image fighter6 = "bgs/fighter1.png"
image fighter7 = "bgs/fighter1.png"
image fighter8 = "bgs/fighter1.png"
image fighter9 = "bgs/fighter1.png"
image fighter10 = "bgs/fighter1.png"
image fighter11 = "bgs/fighter1.png"
image fighter12 = "bgs/fighter1.png"

image fighter13 = "bgs/fighter2.png"
image fighter14 = "bgs/fighter2.png"
image fighter15 = "bgs/fighter2.png"
image fighter16 = "bgs/fighter2.png"

image lazor1 = "bgs/lazor1.png"
image lazor2 = "bgs/lazor2.png"
image lazor3 = "bgs/lazor3.png"

transform fighterMove1:
    xalign 0.4 yalign 0.65
    linear 0.3 xalign 0.405 yalign 0.655
    pause 0.4
    linear 0.6 xalign 0.4 yalign 0.65
    pause 0.25
    linear 0.1 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove2:
    zoom 0.5
    xalign 0.05 yalign 0.4
    linear 0.3 xalign 0.047 yalign 0.396
    pause 0.4
    linear 0.6 xalign 0.05 yalign 0.4
    pause 0.2
    linear 0.1 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove3:
    zoom 0.5
    xalign 0.75 yalign 0.4
    linear 0.3 xalign 0.752 yalign 0.397
    pause 0.3
    linear 0.6 xalign 0.75 yalign 0.4
    linear 0.1 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove4:
    zoom 0.6
    xalign -0.75 yalign 1.2
    pause 0.9
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove5:
    zoom 0.5
    xalign -0.75 yalign 1.2
    pause 0.55
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove6:
    zoom 0.6
    xalign -0.75 yalign 1.8
    pause 0.7
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove7:
    zoom 1.0
    xalign -2.75 yalign 1.2
    pause 0.4
    linear 0.91 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove8:
    zoom 0.5
    xalign -0.75 yalign 1.7
    pause 0.5
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove9:
    zoom 0.6
    xalign -1.75 yalign 2.7
    pause 0.6
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove10:
    zoom 0.7
    xalign -0.75 yalign 1.3
    pause 0.9
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove11:
    zoom 1.2
    xalign -1.75 yalign 1.4
    pause 0.2
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
transform fighterMove12:
    zoom 0.8
    xalign -0.75 yalign 2.5
    pause 0.9
    linear 0.61 xalign 0.99 yalign 0.01 zoom 0.05
    pause 0.2
    
#Toasters
transform fighterMove13:
    zoom 0.05
    pause 1.5
    xalign 0.99 yalign 0.01
    linear 0.6 xalign -0.9 yalign 1.0 zoom 1.5
    
transform fighterMove14:
    zoom 0.05
    pause 1.8
    xalign 0.99 yalign 0.14
    linear 0.6 xalign -0.9 yalign 1.6 zoom 1.5
    
transform fighterMove15:
    zoom 0.05
    pause 1.35
    xalign 0.99 yalign 0.22
    linear 0.5 xalign -0.9 yalign 1.4 zoom 1.5
    
transform fighterMove16:
    zoom 0.05
    pause 1.6
    xalign 0.99 yalign 0.2
    linear 0.6 xalign -0.9 yalign 1.2 zoom 1.5
    
#Lazor fire
transform lazor1:
    zoom 0.05
    pause 1.5
    xalign 0.99 yalign 0.2
    linear 1.3 xalign -2.3 yalign 1.5 zoom 1.7
    
transform lazor2:
    zoom 0.05
    pause 1.25
    xalign 0.99 yalign 0.1
    linear 2.3 xalign -2.3 yalign 1.6 zoom 1.7
    
transform lazor3:
    zoom 0.05
    pause 1.25
    xalign 0.99 yalign 0.1
    linear 3.3 xalign -3.3 yalign 0.7 zoom 1.7

# Fighter craft end

label warEfforts:    
    if tutorialActive == True:
        yTut "This station is monitoring the war between the Republic and the Separatists...?"
        yTut "What's this button do?"
        menu:
            "Launch Strike Force":
                show screen scene_red
                with d3
                "Console" "Preparing to launch strike force."
                "Console" "Insufficient Hypermatter. Please re-fuel fighters."
                yTut "Perhaps I shouldn't be drawing too much attention to myself."
                hide screen scene_red
                with d3
                jump bridge
            "Don't touch any buttons":
                yTut "Perhaps I shouldn't be drawing too much attention to myself."
                jump bridge
    else:
        if messageWarMechanic == True:
            "The console reads: 'Galactic Conflict'."
            "It seems to be tracking the war between the Republic and the Separatists. The Republic is on the losing side."
            "There's a scan button, but it appears to be out of order."
            jump bridge
        if messageWarMechanic == False:
            show screen warTable
            pause
            scene bgBridge
            hide screen warTable
            with d3
            jump bridge
            
            label target17:
                "The Separatists are gathering to strike The Republic!"
                if CISraidCounter == True:
                    y "I already sent out ships today. I'll have to wait until they return later tonight."
                    jump warEfforts
                "Scans reveal that they're currently at [target17Status]/100 strength."
                "Send ships to disrupt their plans?"
                menu:
                    "Send 1 ship":
                        $ CISraidCounter = True
                        "You send one ship to disrupt the CIS plans."
                        $ target17 = 2
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        play sound "audio/sfx/ships.mp3"
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Send 2 ships" if fightersAvailable >= 2:
                        $ CISraidCounter = True
                        $ target17 = 3
                        $ missionInProgress = True
                        $ fightersAvailable -= 2
                        play sound "audio/sfx/ships.mp3"
                        "You send two ship to disrupt the CIS plans."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Send 3 ships" if fightersAvailable >= 3:
                        $ CISraidCounter = True
                        $ target17 = 4
                        $ missionInProgress = True
                        $ fightersAvailable -= 3
                        play sound "audio/sfx/ships.mp3"
                        "You send three ship to disrupt the CIS plans."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        show fighter3 at fighterMove3
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Do not send ships":
                        jump warEfforts
                
            label target16:
                "The Separatists are gathering to strike The Republic!"
                if CISraidCounter == True:
                    y "I already sent out ships today. I'll have to wait until they return later tonight."
                    jump warEfforts
                "Scans reveal that they're currently at [target16Status]/100 strength."
                "Send ships to disrupt their plans?"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ CISraidCounter == True
                        "You send one ship to disrupt the CIS plans."
                        $ target16 = 2
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        play sound "audio/sfx/ships.mp3"
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Send 2 ships" if fightersAvailable >= 2:
                        $ CISraidCounter = True
                        $ target16 = 3
                        $ missionInProgress = True
                        $ fightersAvailable -= 2
                        play sound "audio/sfx/ships.mp3"
                        "You send two ship to disrupt the CIS plans."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Send 3 ships" if fightersAvailable >= 3:
                        $ CISraidCounter = True
                        $ target16 = 4
                        $ missionInProgress = True
                        $ fightersAvailable -= 3
                        play sound "audio/sfx/ships.mp3"
                        "You send three ship to disrupt the CIS plans."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        show fighter3 at fighterMove3
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Do not send ships":
                        jump warEfforts
            label target15:
                "{color=#f6b60a}Target: 'Raid Republic Relief Transports'{/color}"
                "Send out ships to raid innocent relief convoys?"
                menu:
                    "Yes, send 1 ship":
                        $ target15 = 2
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighter to raid The Republic transport ships."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Yes, send 2 ships" if fightersAvailable >= 2:
                        $ target15 = 3
                        $ missionInProgress = True
                        $ fightersAvailable -= 2
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighters to raid The Republic transport ships."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Yes, send 3 ships" if fightersAvailable >= 3:
                        $ target15 = 4
                        $ missionInProgress = True
                        $ fightersAvailable -= 3
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighters to raid The Republic transport ships."
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        show fighter3 at fighterMove3
                        $ renpy.pause(2.5, hard='True')
                        scene warTable with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts
                
            label target14:
                "{color=#e01a1a}H{/color}{color=#22b613}u{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color} {color=#22b613}{color=#e01a1a}S{/color}{color=#22b613}a{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color}{color=#e01a1a}a{/color} {color=#22b613}C{/color}{color=#e01a1a}l{/color}{color=#22b613}a{/color}{color=#e01a1a}u{/color}{color=#22b613}s{/color} - Send out ships to hunt for Santa?"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        $ missionSuccessChance = renpy.random.randint(1, 100)
                        if missionSuccessChance >= 10:
                            $ target14 = 2
                        else:
                            $ target14 = 3
                        play sound "audio/sfx/ships.mp3"
                        hide screen warTable
                        scene stars
                        with fade
                        show fighter1 at fighterMove1
                        pause 2.5
                        show screen warTable 
                        with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts
                
            label target13:
                "{color=#e01a1a}H{/color}{color=#22b613}u{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color} {color=#22b613}{color=#e01a1a}S{/color}{color=#22b613}a{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color}{color=#e01a1a}a{/color} {color=#22b613}C{/color}{color=#e01a1a}l{/color}{color=#22b613}a{/color}{color=#e01a1a}u{/color}{color=#22b613}s{/color} - Send out ships to hunt for Santa?"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        $ missionSuccessChance = renpy.random.randint(1, 100)
                        if missionSuccessChance >= 10:
                            $ target13 = 2
                        else:
                            $ target13 = 3
                        play sound "audio/sfx/ships.mp3"
                        hide screen warTable
                        scene stars
                        with fade
                        show fighter1 at fighterMove1
                        pause 2.5
                        show screen warTable 
                        with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts
            label target12:
                "{color=#e01a1a}H{/color}{color=#22b613}u{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color} {color=#22b613}{color=#e01a1a}S{/color}{color=#22b613}a{/color}{color=#e01a1a}n{/color}{color=#22b613}t{/color}{color=#e01a1a}a{/color} {color=#22b613}C{/color}{color=#e01a1a}l{/color}{color=#22b613}a{/color}{color=#e01a1a}u{/color}{color=#22b613}s{/color} - Send out ships to hunt for Santa?"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        $ missionSuccessChance = renpy.random.randint(1, 100)
                        if missionSuccessChance >= 10:
                            $ target12 = 2
                        else:
                            $ target12 = 3
                        play sound "audio/sfx/ships.mp3"
                        hide screen warTable
                        scene stars
                        with fade
                        show fighter1 at fighterMove1
                        $ renpy.pause(2.5, hard='True')
                        show screen warTable 
                        with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts
                
            label target11:
                "{color=#e90000}Bounty: 'Assassinate a Senator'{/color}"
                "Attacking the Senator's ship will set back the War Meter and have negative effects on Ahsoka's mood."
                $ randomBounty = renpy.random.randint(300, 400)
                "They are offering you [randomBounty] Hypermatter. Accept?"
                menu:
                    "Accept and send out the fleet":
                        $ target11 = False
                        $ hypermatter += randomBounty
                        $ mood -= 20
                        if fleet == 1:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to assassinate the Senator."
                            $ republicWinning -= 5
                            $ target1 = False
                            $ target6 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            $ renpy.pause(2.5, hard='True')
                            scene bgBridge with fade
                            jump warEfforts
                        if fleet == 2:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to assassinate the Senator."
                            $ republicWinning -= 10
                            $ target1 = False
                            $ target6 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            show fighter2 at fighterMove2
                            $ renpy.pause(2.5, hard='True')
                            scene bgBridge with fade
                            jump warEfforts
                        if fleet == 3:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to assassinate the Senator."
                            $ republicWinning -= 15
                            $ target1 = False
                            $ target6 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            show fighter2 at fighterMove2
                            show fighter3 at fighterMove3
                            pause 2.5
                            scene bgBridge with fade
                            jump warEfforts
                    "Back":
                        jump warEfforts
                        
            label target10:
                "{color=#e90000}Bounty: 'Attack Republic Fleet'{/color}"
                "Attacking the Republic fleet will set back the War Meter and have negative effects on Ahsoka's mood."
                $ randomBounty = renpy.random.randint(200, 250)
                "They are offering you [randomBounty] Hypermatter. Accept?"
                menu:
                    "Accept and send out the fleet":
                        $ target10 = False
                        $ hypermatter += randomBounty
                        $ mood -= 15
                        if fleet == 1:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to harass the Republic."
                            $ republicWinning -= 5
                            $ target1 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            pause 2.5
                            scene bgBridge with fade
                            jump warEfforts
                        if fleet == 2:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to harass the Republic."
                            $ republicWinning -= 10
                            $ target1 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            show fighter2 at fighterMove2
                            pause 2.5
                            scene bgBridge with fade
                            jump warEfforts
                        if fleet == 3:
                            play sound "audio/sfx/ships.mp3"
                            "You unleash your fighter ships to harass the Republic."
                            $ republicWinning -= 15
                            $ target1 = False
                            scene stars with fade
                            show fighter1 at fighterMove1
                            show fighter2 at fighterMove2
                            show fighter3 at fighterMove3
                            pause 2.5
                            scene bgBridge with fade
                            jump warEfforts
                    "Back":
                        jump warEfforts
                        
            label target9:
                "{color=#f6b60a}Target: 'Explore unknown regions'{/color}"
                "Send out a ship to explore unknown space?"
                menu:
                    "Yes":
                        $ fightersAvailable -= 1
                        $ target9 = 2
                        "You send a ship out to explore the unknown reaches of space."
                        scene stars with fade
                        show fighter1 at fighterMove1
                        pause 2.5
                        scene bgBridge with fade
                        jump warEfforts
                    "No":
                        jump warEfforts
                
                        
            label target8:
                "{color=#f6b60a}Target: 'Search for smuggle opportunities'{/color}"
                "You send out scouts to look for available smuggling missions."
                "........................................................."
                $ target8 = False
                $ randomSmuggle = renpy.random.randint(1, 8)
                if randomSmuggle == 1:
                    $ smuggleZygerria = True
                    "A smuggling mission from Zygerria is available."
                if randomSmuggle == 2:
                    $ smuggleGeonosis = True
                    "A smuggling mission from Geonosis is available."
                if randomSmuggle == 3:
                    $ smuggleChristophsis = True
                    "A smuggling mission from Christophsisis available."
                if randomSmuggle == 4:
                    $ smuggleNaboo = True
                    "A smuggling mission from Naboo is available."
                if randomSmuggle == 5:
                    $ smuggleTaris = True
                    "A smuggling mission from Taris is available."
                if randomSmuggle == 6:
                    $ smuggleCoruscant = True
                    "A smuggling mission from Coruscant is available."
                if randomSmuggle == 7:
                    $ smuggleTatooine = True
                    "A smuggling mission from Tatooine is available."
                if randomSmuggle == 8:
                    $ smuggleMandalore = True
                    "A smuggling mission from Mandalore is available."
                jump warEfforts
                
            label target7:
                $ target7 = 0
                "Whenever you're trying to communicate with this target, the Force Suppressors begins acting up."
                "You decide to leave it alone for now."
                jump warEfforts
                
            label target6:
                "{color=#f6b60a}Target: 'Word of Mouth'{/color}"
                $ target6 = False
                "You give the order to begin spreading rumors to attract smugglers to the space station."
                $ randomRecruitment = renpy.random.randint(3, 11)
                $ smugglesAvailable += randomRecruitment
                play sound "audio/sfx/whispers.mp3"
                scene bgBridge with fade
                "You manage to attract [randomRecruitment] new smugglers to set up residence on the station."
                jump warEfforts
                
            label target5:
                $ target5 = 0
                $ republicWinning += 1
                "{color=#f6b60a}Target: 'Propaganda'{/color}"
                play sound "audio/sfx/whispers.mp3"
                "You begin spreading Republic propaganda. The war effort moves slightly in your favor."
                if hypermatter >= 250:
                    "Boost the signal of the transmission? (50 Hypermatter)"
                    menu:
                        "Yes":
                            play sound "audio/sfx/whispers.mp3"
                            $ hypermatter -= 50
                            $ republicWinning += 1
                            "Your transmission reaches people from all over the galaxy."
                        "No":
                            pass
                jump warEfforts
                
            label target4:
                "{color=#f6b60a}Target: 'Order Pizza'{/color}"
                $ target4 = 0
                menu:
                    "Order pizza (20 Hypermatter)" if hypermatter >= 20:
                        $ foundryItem2 = "Pizza Hutt - 45 Hypermatter"
                        $ hypermatter -= 20
                        $ pizza += 1
                        y "Mmmm~.... Pizza... ♥"
                        play sound "audio/sfx/itemGot.mp3"
                        "You ordered {color=#FFE653}Pizza{/color} x1!"
                        scene warTable with fade
                        jump warEfforts
                    "Dismiss target":
                        jump warEfforts
                
            label target3:
                "{color=#f6b60a}Target: 'Scavenge Space Graveyard' - The remnants of a space battle have been detected. Send out ships to salvage it?{/color}"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        $ randomScavenge = renpy.random.randint(10, 30)
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighter ship to salvage the graveyard."
                        $ target3 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Send 2 ships" if fightersAvailable >= 2:
                        $ missionInProgress = True
                        $ fightersAvailable -= 2
                        $ randomScavenge = renpy.random.randint(30, 60)
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighters to salvage the graveyard."
                        $ target3 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Send 3 ships" if fightersAvailable >= 3:
                        $ missionInProgress = True
                        $ fightersAvailable -= 3
                        $ randomScavenge = renpy.random.randint(60, 90)
                        play sound "audio/sfx/ships.mp3"
                        "You send out your fighters to salvage the graveyard."
                        $ target3 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        show fighter3 at fighterMove3
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts
                
                $ hypermatter += randomBounty
                $ target3 = False
                "You send out ships to recover any useful junk from a nearby space battle."
                scene bgBridge with fade
                "They managed to recover [randomBounty] Hypermatter."
                jump warEfforts
                
            label target2:
                "{color=#f6b60a}Target: 'Charity Drive' - Collectors are accepting donations to support the Republic War Efforts. Donate?{/color}"
                menu:
                    "50 Hypermatter" if hypermatter >=  50:
                        $ target2 = 2
                        play sound "audio/sfx/donate1.mp3"
                        $ hypermatter -= 50
                        $ republicWinning += 1
                        "You made an anonymous donation of 50 Hypermatter."
                        jump warEfforts
                    "100 Hypermatter" if hypermatter >= 100:
                        $ target2 = 2
                        play sound "audio/sfx/donate1.mp3"
                        $ hypermatter -= 100
                        $ republicWinning += 2
                        "You made an anonymous donation of 100 Hypermatter."
                        jump warEfforts
                    "200 Hypermatter" if hypermatter >= 200:
                        $ target2 = 2
                        play sound "audio/sfx/donate1.mp3"
                        $ hypermatter -= 200
                        $ republicWinning += 4
                        "You made an anonymous donation of 200 Hypermatter."
                        jump warEfforts
                    "300 Hypermatter" if hypermatter >= 300:
                        $ target2 = 2
                        play sound "audio/sfx/donate1.mp3"
                        $ hypermatter -= 300
                        $ republicWinning += 6
                        "You made an anonymous donation of 300 Hypermatter."
                        jump warEfforts
                    "500 Hypermatter" if hypermatter >= 500:
                        $ target2 = 2
                        play sound "audio/sfx/donate1.mp3"
                        $ republicWinning += 8
                        $ hypermatter -= 500
                        "You made an anonymous donation of 500 Hypermatter."
                        jump warEfforts
                    "Back":
                        jump warEfforts
                        
            label target1:
                "{color=#f6b60a}Target: 'Harass CIS Fleet' - Send out ships to harass the CIS fleet?{/color}"
                menu:
                    "Send 1 ship" if fightersAvailable >= 1:
                        $ missionInProgress = True
                        $ fightersAvailable -= 1
                        play sound "audio/sfx/ships.mp3"
                        "You unleash your fighter ship to harass the Separatists."
                        $ republicWinning += 2
                        $ target1 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Send 2 ship" if fightersAvailable >= 2:
                        $ missionInProgress = True
                        $ fightersAvailable -= 2
                        "You unleash your fighter ships to harass the Separatists."
                        $ republicWinning += 3
                        $ target1 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Send 3 ship" if fightersAvailable >= 3:
                        $ missionInProgress = True
                        $ fightersAvailable -= 3
                        "You unleash your fighter ships to harass the Separatists."
                        $ republicWinning += 4
                        $ target1 = 2
                        hide screen warTable
                        scene stars 
                        with fade
                        show fighter1 at fighterMove1
                        show fighter2 at fighterMove2
                        show fighter3 at fighterMove3
                        pause 2.5
                        scene warTable with fade
                        jump warEfforts
                    "Back":
                        jump warEfforts


################################################################################################
####################################### WAR METER END #########################################
################################################################################################



################################################################################################
####################################### EVENING START ##########################################
################################################################################################    

label time:
    if tutorialActive == True:
        yTut "I don't have time to wait around. I'd best do some more exploring."
        jump bridge
    else:
        jump jobReport
                
################################################################################################
####################################### OPTIONS END ###########################################
################################################################################################       
        
################################################################################################
####################################### STATUS START ##########################################
################################################################################################  

#################################### Status Screen #################################################
screen status_screen:
    zorder 0
    imagemap:
        ground "UI/status.jpg"
        hover "UI/status-hover.jpg"
        
        hotspot (730, 10, 60, 55) clicked Jump("bridge")
        hotspot (590, 530, 145, 50) clicked Jump("suppressors")
        #hotspot (130, 515, 28, 30) clicked Jump("samActive")                   # Toggles between artstyles
        
    hbox:
        spacing 10 xpos 360 ypos 30
        text "{color=#FFE653}{size=+4}Status{/size}{/color}"
        
    hbox:
        spacing 10 xpos 110 ypos 100
        text "{size=-1}{color=#FFE653}Ahsoka:{/color} \nMood: [mood] \nSlut level: [ahsokaSlut] \nSocial level: [ahsokaSocial] \n\n{color=#FFE653}Player skills:{/color}\nRepair skill: [repairSkill] \nGunslinger: [gunslinger] \n \n{color=#FFE653}Other:{/color} \nDay: [day] \nHypermatter: [hypermatter] \nInfluence: [influence]{/size}"


label status:
    call screen status_screen
    jump bridge
    
label suppressors:
    if tutorialActive == True:
        y "Best not start pressing buttons randomly."
        jump status
    if ahsokaSocial == 42:
        "You call Mr. Jason."
        show screen scene_darkening
        with d3
        show screen jason_main
        with d3
        y "It's time to turn off the Force Suppressors."
        mr "Do you believe that is wise, master?"
        menu:
            "Yes. Turn off the suppressors":
                $ suppressorsDisabled = True
                mr "Very well master."
                "Jason presses a few buttons on his arm console and the lights flicker for a moment."
                mr "It is done master."
                hide screen jason_main
                with d3
                pause 1.0
                y "........................."
                if ahsokaIgnore <= 1:
                    play sound "audio/sfx/exploreFootstepsRun.mp3"
                    pause 1.0
                    $ ahsokaExpression = 11
                    show screen ahsoka_main
                    with d3
                    if mission13 <= 14:
                        a "[playerName]! What did you do?!"
                        y "Huh?"
                        a "I suddenly felt a massive spike of Dark Side energy!"
                    if mission13 >= 15:
                        a "[playerName]..? Did you talk to Mister Jason?"
                    y "I turned off the Force Suppressors."
                    a "You did..? Wow~... I..."
                    a "{b}*Smirks*{/b}"
                    y "Uh-oh..."
                    a "{color=#ec79a2}You {i}will{/i} give me the day off.{/color}"
                    y "{color=#ec79a2}I... will give you the day off....{/color}"
                    a "Thank you, [playerName]! That's very sweet of you! Bye now!"
                    hide screen ahsoka_main
                    hide screen scene_darkening
                    with d3
                    y ".........................."
                    pause 2.0
                    y "Wait...! She tricked me!"
                    scene black with longFade
                    "The rest of the day passes....."
                    jump jobReport
                
                jump status
            "On second thought....":
                mr "Very well master.  I'll let you think it over for now."
                hide screen scene_darkening
                hide screen jason_main
                with d3
                jump status
    else:
        "You call Mr. Jason."
        show screen scene_darkening
        with d3
        show screen jason_main
        with d3
        mr "How may I help you, master?"
        y "Can you turn off the force suppressors?"
        mr "The Force Suppressors? Are you certain that is a good idea?"
        if shinActive == True:
            y "Good point, I do have two Jedi's running around the station..."
        else:
            y "Good point, I do have a Jedi running around the station..."
        y "Forget I said anything."
        hide screen scene_darkening
        hide screen jason_main
        with d3
        jump status
    
################################################################################################
####################################### STATUS END ##########################################
################################################################################################ 


################################################################################################
###################################### INVENTORY BEGINS #######################################
################################################################################################ 

screen itemBackground:
    add "UI/items/itemBackground.png"




#########################################################
################### INVENTORY START ####################
#########################################################

define inventoryCharacterName = "Ahsoka"

screen inventory1:
    imagemap:
        ground "UI/inventoryItems1.png"
        hover "UI/inventoryItems1-hover.png"
        
        hotspot (505, 0, 110, 75) action Jump("inventoryPreviousCharacter")
        hotspot (615, 0, 110, 75) action Jump("inventoryNextCharacter")
        
        hotspot (15, 30, 130, 50) action Jump("inventorySelectItems")
        hotspot (135, 30, 200, 50) action Jump("inventorySelectAcc")
        hotspot (330, 30, 153, 50) action Jump("inventorySelectOutfits")
        
        hotspot (145, 525, 60, 55) action Jump("inventoryPreviousPage")
        hotspot (300, 525, 60, 55) action Jump("inventoryNextPage")
        
        hotspot (728, 0, 70, 75) action Jump("exitInventory")

        
    hbox: ### Character select in inventory ###
        spacing 10 xpos 570 ypos 28
        text "{size=-1}{color=#FFD34F}[inventoryCharacterName]{/color}{/size}" 
        
    hbox: ### Character select in inventory ###
        spacing 10 xpos 240 ypos 532
        text "{size=+10}{color=#FFD34F}[inventoryStageSelect]{/color}{/size}" 

        
define inventoryCurrentCharacter = 0   # 0 is Ahsoka, 1 is Shin, 2 is Kit
define inventoryStageSelect = 0

# Switch to the previous character
label inventoryPreviousCharacter:
    $ inventoryCurrentCharacter -= 1
    if inventoryCurrentCharacter == -1:
        $ inventoryCurrentCharacter = 2
    jump hideAll

# Switch to the next character
label inventoryNextCharacter:
    
    $ inventoryCurrentCharacter += 1
    if inventoryCurrentCharacter == 3:
        $ inventoryCurrentCharacter = 0
    jump hideAll
    
    
# Switch to Items
label inventorySelectItems:
    $ inventoryStageSelect = 0
    jump hideAll

# Switch to Accessories
label inventorySelectAcc:
    $ inventoryStageSelect = 3
    jump hideAll

# Switch to Outfits
label inventorySelectOutfits:
    $ inventoryStageSelect = 5
    jump hideAll
    
    
label inventoryNextPage:
    $ inventoryStageSelect += 1
    if inventoryStageSelect > 6:
        $ inventoryStageSelect = 0
    jump hideAll
    
label inventoryPreviousPage:
    $ inventoryStageSelect -= 1
    if inventoryStageSelect < 0:
        $ inventoryStageSelect = 6
    jump hideAll
    

########################################################################
################################ HIDE ALL  ############################## to prevent overlapping graphics
########################################################################
label hideAll:
    # hide all items
    hide screen invAll
    jump inventory

    
# INVENTORY LABEL
label inventory:
    if armorEquipped == True:
        $ hair = 99
    else:
        $ hair = hairSet;
    if shinActive == True:
        if shinSkin == 2:
            $ shinSkin = 2
        else:
            $ shinSkin = 0
    if kitActive == True:
        $ kitSkin = 0
    $ ahsokaExpression = 9
    $ shinExpression = 26
    if kitActive == False:
        $ kitExpression = 0
        $ kitOutfit = 0
    if kitActive == True:
        $ kitOutfit = kitOutfitSet
    if shinActive == False:
        $ shinExpression = 0
    if shinActive == True:
        $ shinOutfit = shinOutfitSet
    if tutorialActive == True:
        "I'm not really carrying anything at the moment."
        jump bridge
    if tutorialActive == False:
        scene white
        $ ahsokaExpression = 9
        if inventoryCurrentCharacter == 0:
            $ inventoryCharacterName = "Ahsoka"
            hide screen shin_main
            hide screen kit_main
            show screen ahsoka_main
        if inventoryCurrentCharacter == 1:
            if shinActive == False:
                $ inventoryCharacterName = "    ???"
            if shinActive == True:
                $ inventoryCharacterName = "Shin'na"
            hide screen kit_main
            hide screen ahsoka_main
            if shinActive == False:
                $ shinSkin = 1
            if shinSkin == 2:
                $ shinSkin = 2
            else:
                pass
            show screen shin_main
        if inventoryCurrentCharacter == 2:
            if kitActive == False:
                $ inventoryCharacterName = "    ???"
            if kitActive == True:
                $ inventoryCharacterName = "    Kit"
            hide screen shin_main
            hide screen ahsoka_main
            if kitActive == False:
                $ kitSkin = 1
            else:
                $ kitSkin = 0
            show screen kit_main
            
        ### Call inventory screen after everything has been calculated ###
        if shinActive == False:
            $ accessoriesShin = 0
        scene bgInventory
        show screen invAll
        call screen inventory1

label exitInventory:
    if body >= 3:
        $ body = 0
    $ outfit = outfitSet
    $ kitOutfit = kitOutfitSet
    if shinActive == False:
        $ shinSkin = 0
    if kitActive == False:
        $ kitSkin = 0
    $ kitExpression = 1
    $ shinExpression = 1
    if shinActive == False:
        $ accessoriesShin = 1
    if accessories2 == 5:
        $ accessories2 = 6
    if accessories2Shin == 5:
        $ accessories2Shin = 6
    if accessories2Kit == 5:
        $ accessories2Kit = 6
    hide screen shin_main
    hide screen kit_main
    hide screen ahsoka_main
    hide screen scene_darkening
    hide screen invAll
    if mission11 == 3:
        jump mission11
    else:
        jump bridge


################################################################################################
####################################### INVENTORY ENDS ########################################
################################################################################################ 


################################################################################################
########################################### MISSIONS ###########################################
################################################################################################
# Mission titles are changes as they update to the next stage.
define mission1Title = ""
define mission2Title = ""
define mission3Title = ""
define mission4Title = ""
define mission5Title = ""
define mission6Title = ""
define mission7Title = ""
define mission8Title = ""
define mission9Title = ""
define mission10Title = ""
define mission11Title = ""
define mission12Title = ""
define mission13Title = ""

screen mission1:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission1PlaceholderText]" 
        
screen mission2:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission2PlaceholderText]" 
        
screen mission3:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission3PlaceholderText]" 
        
screen mission4:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission4PlaceholderText]" 
        
screen mission5:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission5PlaceholderText]" 
        
screen mission6:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission6PlaceholderText]" 
        
screen mission7:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission7PlaceholderText]" 
        
screen mission8:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission8PlaceholderText]" 
        
screen mission9:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission9PlaceholderText]" 
        
screen mission10:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission10PlaceholderText]" 
        
screen mission11:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission11PlaceholderText]" 
        
screen mission12:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission12PlaceholderText]" 
        
screen mission13:
    hbox:
        spacing 10 xpos 60 ypos 160 xsize 670
        text "[mission13PlaceholderText]" 


define mission1PlaceholderText = ""
define mission2PlaceholderText = ""
define mission3PlaceholderText = ""
define mission4PlaceholderText = ""
define mission5PlaceholderText = ""
define mission6PlaceholderText = ""
define mission7PlaceholderText = ""
define mission8PlaceholderText = ""
define mission9PlaceholderText = ""
define mission10PlaceholderText = ""
define mission11PlaceholderText = ""
define mission12PlaceholderText = ""
define mission13PlaceholderText = ""

label missions:
    scene bgMissions
    menu:
        "[mission1Title]" if mission1 >= 1:
            show screen mission1
            pause
            hide screen mission1
            jump missions
        "[mission2Title]" if mission2 >= 1:
            show screen mission2
            pause
            hide screen mission2
            jump missions
        "[mission3Title]" if mission3 >= 1:
            show screen mission3
            pause
            hide screen mission3
            jump missions
        "[mission4Title]" if mission4 >= 1:
            show screen mission4
            pause
            hide screen mission4
            jump missions
        "[mission5Title]" if mission5 >= 1:
            show screen mission5
            pause
            hide screen mission5
            jump missions
        "[mission6Title]" if mission6 >= 1:
            show screen mission6
            pause
            hide screen mission6
            jump missions
        "[mission7Title]" if mission7 >= 1:
            show screen mission7
            pause
            hide screen mission7
            jump missions
        "[mission8Title]" if mission8 >= 1:
            show screen mission8
            pause
            hide screen mission8
            jump missions
        "[mission9Title]" if mission9 >= 1:
            show screen mission9
            pause
            hide screen mission9
            jump missions
        "[mission10Title]" if mission10 >= 1:
            show screen mission10
            pause
            hide screen mission10
            jump missions
        "[mission11Title]" if mission11 >= 1:
            show screen mission11
            pause
            hide screen mission11
            jump missions
        "[mission12Title]" if mission12 >= 1:
            show screen mission12
            pause
            hide screen mission12
            jump missions  
        "[mission13Title]" if mission13 >= 1:
            show screen mission13
            pause
            hide screen mission13
            jump missions  
            
        #"{color=#1cff0b}DEV: RESET MISSION: NABOOLICIOUS{/color}" if 11 <= mission10 <= 12:
            define fixNaboolicious = 0
            show screen scene_darkening
            show screen devdroid_main
            with d3
            "Dev Bot" "Hi it's me, you're lovable companion Dev Bot."
            "Dev Bot" "Are you having some trouble with the Naboolicious quest? We can try fixing that."
            menu:
                "Please help me fix it" if fixNaboolicious == 0:
                    $ fixNaboolicious = 1
                    "Dev Bot" "It probably has to do with with Ahsoka Slut level being too high and accidentally progressing the quest."
                    "Dev Bot" "Let's have a look. The mission is suppose to be on stage 11."
                    if mission10 == 11:
                        "Dev Bot" "I can see that it still is. You should be able to gather clues from the brothel now, but if for some reason you can't. I can skip the quest forward a tiny bit?"
                        menu:
                            "Yes please":
                                $ prosCounter = 3
                                $ meetingMarieke = False
                                "Dev Bot" "You got it bucko! I'm jumping you over to Naboo, visit the brothel and see if Marieke shows up to talk to you."
                                stop music fadeout 1.5
                                hide screen scene_darkening
                                hide screen devdroid_main
                                scene black
                                with longFade
                                scene bgNaboo
                                with fade
                                jump exploreNabooMenu
                            "Never mind":
                                jump missions
                    if mission10 == 12:
                        $ mission10 = 11
                        $ ahsokaSlut = 34
                        "Dev Bot" "But it looks like it's already on stage 12."
                        "Dev Bot" "I'm resetting it to stage 11 now and adjusting Ahsoka's slut level."
                        "Dev Bot" "Try visiting the brothel a few times now. The girls should start talking about what Mandora likes."
                        "Dev Bot" "If they don't...."
                        "Dev Bot" "Then come back here and we'll try something else."
                        hide screen scene_darkening
                        hide screen devdroid_main
                        with d3
                        scene black with longFade
                        scene bgBridge with fade
                        jump mission10
                "It didn't work. Could you try something else?" if fixNaboolicious == 1:
                    $ mission10 = 12
                    $ ahsokaSlut = 35
                    "Dev Bot" "Let's try something else."
                    "Dev Bot" "I'm progressing the quest forward a bit to a stage where it should be more stable."
                    "Dev Bot" "It should jump you to a scene where you figured out what Mandora likes and you're getting the girls ready to go work at the brothel."
                    "Dev Bot" "If after all this, that doesn't happen and you instead get something completely unrelated.... Then I'm afraid it's completely broken."
                    "Dev Bot" "I'm really sorry, but the only thing to do after that would be to start a new game."
                    "Dev Bot" "Fingers crossed!"
                    hide screen scene_darkening
                    hide screen devdroid_main
                    with d3
                    scene black with longFade
                    scene bgBridge with fade
                    jump mission10
                "SHINY BUTTON! You shouldn't see me, but you can click me anyways!" if fixNaboolicious > 2:
                    $ mission10 = 12
                    $ ahsokaSlut = 35
                    "Dev Bot" "We could also try to...{w} to..."
                    "Dev Bot" "Wait a second... How did you even get to this bit?"
                    "Dev Bot" "According to my data... you should have been given option 1 or 2, but somehow you found yourself at option 3!"
                    "Dev Bot" "We'll just go ahead and try the same thing as option 2 and see if it works."
                    "Dev Bot" "I fear that if it does not work.. Then I'm afraid it's completely broken."
                    "Dev Bot" "I'm really sorry, but the only thing to do after that would be to start a new game."
                    "Dev Bot" "Fingers crossed!"
                    hide screen scene_darkening
                    hide screen devdroid_main
                    with d3
                    scene black with longFade
                    scene bgBridge with fade
                    jump mission10
                "Never mind":
                    jump missions
            
        "": #{color=#1cff0b}DEV: RESET MISSION: EVIL EYES{/color}
            show screen scene_darkening
            show screen devdroid_main
            with d3
            "Dev Bot" "Uh oh! Getting stuck on Mission: Crazy Eyes?"
            "Dev Bot" "Try resetting the quest now and playing through it again."
            menu:
                "{color=#1cff0b} DEV: Reset quest now{/color}":
                    $ body = 0
                    $ underAttackClearCells = False
                    $ underAttackClearMed = False
                    $ underAttackClearForge = False
                    $ underAttackClearExplore = False
                    $ crazyEyesStatusUpdate = 0
                    $ countdownAttack = 4
                    $ hair = 0
                    $ mission13 = 6
                    $ ahsokaSlut = 37 
                    $ crazyEyesStatusUpdate == 0
                    "Dev Bot" "All right, the quest has been reset to a previous stage!"
                    "Dev Bot" "To refresh your memory. This is what happened:"
                    "Dev Bot" "The girls are helping Kit make a new training manual."
                    "Dev Bot" "You are in the process of training them when news spreads of The Republic's transport being shot down. Ahsoka reacts upset."
                    "Dev Bot" "You can progress her training to continue the story, though you might have to re-do a few scenes."
                    hide screen scene_darkening
                    hide screen devdroid_main
                    with d3
                    scene black with longFade
                    jump bridge
                "Don't reset":
                    hide screen scene_darkening
                    hide screen devdroid_main
                    jump missions
        "Back":
            jump bridge
            
##########################################################################
############################### CREDITS ##################################
##########################################################################
image star1 = "bgs/star1.png"
image star2 = "bgs/star1.png"
image star3 = "bgs/star1.png"
image star4 = "bgs/star1.png"

image credits = "credits/CREDITS.png"
image 0credits = "credits/0-exiscoming.png"
image 1credits = "credits/1-models.png"
image 2credits = "credits/2-outfits.png"
image 3credits = "credits/3-backgrounds.png"
image 4credits = "credits/4-animations.png"
image 4Halfcredits = "credits/4.5-high-detailed-art.png"
image 5credits = "credits/5-sex-scenes.png"
image 6credits = "credits/6-items.png"
image 7credits = "credits/7-voice-acting.png"
image 8credits = "credits/8-additional-art.png"
image 9credits = "credits/9-title-screen.png"
image 10credits = "credits/10-sound-effects.png"
image 11credits = "credits/11-music-one.png"
image 12credits = "credits/12-music-two.png"
image 13credits = "credits/13-music-three.png"
image 14credits = "credits/14-music-four.png"
image 15credits = "credits/15-patreon-one.png"
image 16credits = "credits/16-patreon-two.png"
image 17credits = "credits/17-special-thanks.png"

transform fighterMoveCredits:
    xalign 0.05 yalign 0.85 zoom 0.9
    linear 0.3 xalign 0.0505 yalign 0.855
    pause 0.4
    linear 0.6 xalign 0.05 yalign 0.85
    pause 0.4
    repeat
    
##################### STARS ####################
    
transform creditStar1:
    xalign 0.9 yalign 0.01 zoom 0.07
    linear 0.3 xalign -0.1 yalign 0.755 zoom 0.2
    pause 6.0
    repeat
    
transform creditStar2:
    xalign 0.9 yalign 0.4 zoom 0.01
    pause 1.0
    linear 0.3 xalign -0.1 yalign 1.5 zoom 0.36
    pause 5.0
    repeat
    
transform creditStar3:
    xalign 0.7 yalign 0.01 zoom 0.01
    pause 2.5
    linear 0.3 xalign -0.4 yalign 0.57 zoom 0.38
    pause 2.0
    repeat
    
transform creditStar4:
    xalign 0.95 yalign 0.55 zoom 0.01
    pause 3.5
    linear 0.3 xalign 0.45 yalign 1.2 zoom 0.27
    pause 2.5
    repeat
    
##################### CREDITS ####################

transform credits:
    xalign 1.0 yalign 0.3 zoom 0.01
    linear 0.1 xalign 0.88 yalign 0.4 zoom 1.0
    pause 5.5
    linear 0.2 xalign -0.75 yalign 1.2 zoom 0.27
    
label credits:
    show star1 at creditStar1
    show star2 at creditStar2
    show star3 at creditStar3
    show star4 at creditStar4
    show fighter1 at fighterMoveCredits
    pause 3.0
    show credits at credits
    pause 6.5
    show 0credits at credits
    pause 6.5
    show 1credits at credits
    pause 6.5
    show 2credits at credits
    pause 6.5
    show 3credits at credits
    pause 6.5
    show 4credits at credits
    pause 6.5
    show 4Halfcredits at credits
    pause 6.5
    show 5credits at credits
    pause 6.5
    show 6credits at credits
    pause 6.5
    show 7credits at credits
    pause 6.5
    show 8credits at credits
    pause 6.5
    show 9credits at credits
    pause 6.5
    show 10credits at credits
    pause 6.5
    show 11credits at credits
    pause 6.5
    show 12credits at credits
    pause 6.5
    show 13credits at credits
    pause 6.5
    show 14credits at credits
    pause 6.5
    show 15credits at credits
    pause 6.5
    show 16credits at credits
    pause 6.5
    show 17credits at credits
    pause 10.0
    stop music fadeout 4.0
    scene black with longFade
    show text "{size=+20}11218{/size}" with dissolve
    pause 1.5
    return
