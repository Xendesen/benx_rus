﻿define staticWatch = 0
# Story and code by Exiscoming - https://www.patreon.com/exiscoming
# Character art by Flick, SamCooper
# Background art by Nyuunzi and Ganassa
# Props and items by Velenor
# Ship by OffWorldTrooper
# Title / special art Juneles & Octomush Art
# music - Newgrounds, see music credit list for details

# Alternative Art Style
define samActive = True

# effects.
define flashbulb = Fade(0.2, 0.0, 0.4, color='#fff')
define flash = Fade(0.2, 0.5, 0.8, color='#fff')
define bloodbulb = Fade(0.2, 0.0, 0.8, color='#DF0101')
define medFade = Fade(0.1, 0.1, 2.0, color='#000000')
define longFade = Fade(0.8, 1.0, 1.5, color='#000000')
define d1 = Dissolve(0.1)
define d2 = Dissolve(0.2)
define d3 = Dissolve(0.3)
define d4 = Dissolve(0.4)
define d5 = Dissolve(0.5)
define dHorror = Dissolve(4.1)

# declare images.

#Spy events
image spyEvent1 = "bgs/spyEvents/spyEvent1.jpg"

#Animation
image ahsokaMovie movie = Movie(play="animations/boobjob.ogv")
image shinMovie movie = Movie(play="animations/anal.ogv")
image kitMovie movie = Movie(play="animations/cowgirl.ogv")

#Backgrounds Star Forge
image tutorial1 = "bgs/tutorial1.jpg"
image bgBridge = "bgs/bgBridge.jpg"
image bgBridgeBlur = "bgs/bgBridgeBlur.jpg"
image bgBridgeDrugs = "bgs/bgBridgeDrugs.jpg"
image bgExplore = "bgs/bgExplore.jpg"
image bgExplore2 = "bgs/bgExplore2.jpg"
image bgExplore3 = "bgs/bgExplore3.jpg"
image bgExplore3Dis = "bgs/bgExplore3Dis.jpg"
image bgExplore4Dis = "bgs/bgExplore4Dis.jpg"
image bgExplore4 = "bgs/bgExplore4.jpg"
image bgBedroom = "bgs/bgBedroom.jpg"
image bgStatus = "bgs/bgStatus.jpg"
image bgConstruct = "bgs/bgConstruct.jpg"
image bgTraining = "bgs/training.png"
image bgCell01 = "bgs/bgCell01.jpg"
image bgCell02 = "bgs/bgCell02.jpg"
image bgCell03 = "bgs/bgCell03.jpg"
image bgCellSith = "bgs/bgCellSith.jpg"
image stars = "bgs/stars.jpg"
image bgArena = "bgs/bgArena.jpg"
image bgStripclub = "bgs/bgStripclub.jpg"
image bgStation = "bgs/bgStation.jpg"
image bgBrothel = "bgs/bgBrothel.jpg"
image bgBrothel2 = "bgs/bgBrothel2.jpg"
image koltoDrugs1 = "XXX/kolto/koltoSceneDrugs1.jpg"
image koltoDrugs2 = "XXX/kolto/koltoSceneDrugs2.jpg"
image koltoDrugs3 = "XXX/kolto/koltoSceneDrugs3.jpg"
image breakdownShin = "bgs/breakdownShin.jpg"
image warTable = "bgs/warTable.jpg"

#ENDGAME
image bgSector6Dark = "bgs/bgSector6Dark.jpg"
image bgSector6 = "bgs/bgSector6.jpg"
image bgSector6Destroyed = "bgs/bgSector6Destroyed.jpg"
image droidEvent = "UI/droids.png"
image jasonDeath = "bgs/jasonDeath.jpg"
image flashback0 = "bgs/flashback0.jpg"
image flashback1 = "bgs/flashback1.jpg"
image flashback2 = "bgs/flashback2.jpg"
image flashback3 = "bgs/flashback3.jpg"
image flashback4 = "bgs/flashback4.jpg"
image flashback5 = "bgs/flashback5.jpg"
image bgSector6DestroyedBlur = "bgs/bgSector6DestroyedBlur.jpg"

#Backgrounds Planets
image bgTatooine = "bgs/bgTatooine.jpg"
image bgCoruscant = "bgs/bgCoruscant.jpg"
image bgZygerria = "bgs/bgZygerria.jpg"
image bgNaboo = "bgs/bgNaboo.jpg"
image bgChristophsis = "bgs/bgChristophsis.jpg"
image bgGeonosis = "bgs/bgGeonosis.jpg"
image bgMandalore = "bgs/bgMandalore.jpg"
image bgTaris = "bgs/bgTaris.jpg"

#Training images
image trainingDance1 = "XXX/ahsokaDancing1.png"

#misc. images
image bgTitle = "bgs/bgTitle.png"
image wardrobeModel = "UI/wardrobe/wardrobeModel.png"
image bgInventory = "bgs/bgInventory.png"
image bgMissions = "ui/missions.jpg"

#################################### Scene Xmas  #################################################

image xmasGreetings = "bgs/xmasGreetings.jpg"

#neutral bgs
image white = "bgs/white.png"
image black = "bgs/black.png"


#################################### Scene Darkening #################################################

screen scene_darkening: 
    add "bgs/sceneIntroduction.png"
    
screen scene_dankening: 
    add "bgs/sceneDank.png"
    
screen scene_red:
    add "bgs/alarmOverlay.png"
    
screen scene_green:
    add "bgs/scanOverlay.png"
    

#################################### Santa Claus #################################################
screen santa:
    add "models/skin/santa.png" at right

#################################### Ahsoka Body / clothing / emotions #################################################
define ahsokaSaberEmotion = 1

screen ahsokaMainSaber:
    add "models/skin/sam/ahsokaMainSaber.png" at right
        
    if ahsokaSaberEmotion == 1:
        add "models/emotions/ahsokaEmotesSam/ahsokaSaberEmotion1.png" at right
    if ahsokaSaberEmotion == 2:
        add "models/emotions/ahsokaEmotesSam/ahsokaSaberEmotion2.png" at right
    if ahsokaSaberEmotion == 3:
        add "models/emotions/ahsokaEmotesSam/ahsokaSaberEmotion3.png" at right


define body = 0
define breastSize = 0
define tattoosFace = 1
define tattoosBody = 1
define underwear = 1
define breasts = 0
define outfit = 1
define underwearBottom = 1
define underwearTop = 1
define accessories = 0
define accessories2 = 0
define accessories3 = 0
define outfitSet = 1
define accessoriesSet = 0
define ahsokaExpression = 1
define ahsokaBlush = 0
define ahsokaTears = 0
define ahsokaFaceCum = 0
define ahsokaBodyCum = 0
define hair = 0
define hairSet = 0
define armorEquipped = False

######################################## PUZZLE HOVERMAP ########################################

screen puzzle:
    zorder 0
    imagemap:
        ground "bgs/imgMaps/puzzle.png"
        hover "bgs/imgMaps/puzzle-hover.png"
        
        hotspot (75, 150, 295, 115) clicked Jump("puzzleSelect1")
        hotspot (75, 330, 295, 115) clicked Jump("puzzleSelect2")
        hotspot (435, 330, 295, 115) clicked Jump("puzzleSelect3")
        hotspot (435, 150, 295, 115) clicked Jump("puzzleSelect4")
        
        if puzzleSelect1 == True:
            add "bgs/imgMaps/puzzle1Selected.png" xpos 105 ypos 231
        if puzzleSelect2 == True:
            add "bgs/imgMaps/puzzle2Selected.png" xpos 104 ypos 336
        if puzzleSelect3 == True:
            add "bgs/imgMaps/puzzle3Selected.png" xpos 444 ypos 336
        if puzzleSelect4 == True:
            add "bgs/imgMaps/puzzle4Selected.png" xpos 105 ypos 336
            


screen ahsoka_main: #Ahsoka ancor
    #Body
    if body == 0:
        if samActive == True:
            add "models/skin/sam/ahsokaNude.png" at right
        else:
            add "models/skin/ahsokaNude.png" at right
    if body == 1:
        if samActive == True:
            add "models/skin/sam/ahsokaNudeHuman.png" at right
        else:
            add "models/skin/ahsokaNudeHuman.png" at right
    if body == 2:
        if samActive == True:
            add "models/skin/sam/ahsokaEvil.png" at right
        else:
            add "models/skin/ahsokaNudeHuman.png" at right
            
    #Breasts
    if breastSize == 1 and outfit == 0 and underwearTop == 0:
        if body == 0:
            add "models/skin/sam/ahsokaBreastsMd.png" at right
        if body == 1:
            add "models/skin/sam/ahsokaBreastsMdHuman.png" at right
        if body == 2:
            add "models/skin/sam/ahsokaBreastsMdSith.png" at right
    if breastSize == 2 and outfit == 0 and underwearTop == 0:
        if body == 0:
            add "models/skin/sam/ahsokaBreastsLg.png" at right
        if body == 1:
            add "models/skin/sam/ahsokaBreastsLgHuman.png" at right
        if body == 2:
            add "models/skin/sam/ahsokaBreastsLgSith.png" at right
            
        
    #Tattoos body
    if tattoosBody == 0:
        if samActive == True:
            add "models/mods/ahsokaNoMods.png" at right
        else:
            add "models/mods/ahsokaNoMods.png" at right
    if tattoosBody == 1:
        if samActive == True:
            add "models/mods/sam/tattooSet1Body.png" at right # Basic tattoos
        else:
            add "models/mods/tattooSet1.png" at right # Basic tattoos
    if tattoosBody == 2:
        if samActive == True:
            add "models/mods/sam/tattooSet2Body.png" at right # Tattoo 2
        else:
            add "models/mods/tattooSet2.png" at right # Tattoo 2
    if tattoosBody == 3:
        if samActive == True:
            add "models/mods/sam/tattooSet3Body.png" at right # Tattoo 3
        else:
            add "models/mods/tattooSet3.png" at right # Tattoo 3
    if tattoosBody == 4:
        if samActive == True:
            add "models/mods/sam/tattooSet4Body.png" at right # Tattoo 4
        else:
            add "models/mods/tattooSet4.png" at right # Tattoo 4
    if tattoosBody == 5:
        if samActive == True:
            add "models/mods/sam/tattooSet5Body.png" at right # Tattoo 5
        else:
            add "models/mods/tattooSet5.png" at right # Tattoo 5
            
    #Tattoos face
    if tattoosFace == 0:
        if samActive == True:
            add "models/mods/ahsokaNoMods.png" at right
    if tattoosFace == 1:
        if samActive == True:
            add "models/mods/sam/tattooSet1Face.png" at right # Basic tattoos
    if tattoosFace == 2:
        if samActive == True:
            add "models/mods/sam/tattooSet2Face.png" at right # Tattoo 2
    if tattoosFace == 3:
        if samActive == True:
            add "models/mods/sam/tattooSet3Face.png" at right # Tattoo 3
    if tattoosFace == 4:
        if samActive == True:
            add "models/mods/sam/tattooSet4Face.png" at right # Tattoo 4
    if tattoosFace == 5:
        if samActive == True:
            add "models/mods/sam/tattooSet5Face.png" at right # Tattoo 5
    
    #Underwear Top
    if samActive == True:
        if underwearTop == 1:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearTop.png" at right #Basic underwearTop  
        if underwearTop == 3:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear2Top.png" at right #
        if underwearTop == 4:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear3Top.png" at right
    else:
        add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at right #Basic underwearTop
           
    #Underwear Bottom
    if underwearBottom == 1:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearBottom1.png" at right #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at right #Basic underwearBottom
    if underwearBottom == 2:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearBottom2.png" at right #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at right #Basic underwearBottom
    if underwearBottom == 3:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear2Bottom.png" at right 
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at right 
    if underwearBottom == 4:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear3Bottom.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at right
            
    #accessories 3    
    if accessories3 == 1:
        if samActive == True:
            add "models/accessories/sam/ahsokaStockings.png" at right
    #Outfit
    if outfit == 0:
        add "models/outfits/outfitAhsoka/ahsokaNoOutfit.png" at right #No underwear
    if outfit == 1:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasic.png" at right #Basic outfit
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right #Basic outfit
    if outfit == 2:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicPantsless.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicPantsless.png" at right #Basic outfit no leggings
    if outfit == 3:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicShortSkirt.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicShortSkirt.png" at right #Basic short skirt
    if outfit == 4:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicTopLift.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicTopLift.png" at right #Basic top pulled up.
    if outfit == 5:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/lekkuBasic.png" at right
        else:
            add "models/outfits/outfitAhsoka/lekkuBasic.png" at right #Lekku Basic
    if outfit == 6:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/lekkuBasic2.png" at right
        else:
            add "models/outfits/outfitAhsoka/lekkuBasic2.png" at right #Lekku no leggings
    if outfit == 7:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/lekkuBasic3.png" at right
        else:
            add "models/outfits/outfitAhsoka/lekkuBasic3.png" at right #Lekku short shirt, no leggings
    if outfit == 8:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/slave1.png" at right
        else:
            add "models/outfits/outfitAhsoka/slave1.png" at right #Nemtek outfit
    if outfit == 9:
        add "models/outfits/outfitAhsoka/slave2.png" at right #Nemtek dress without bottom
    if outfit == 10:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/slave2.png" at right 
        else:
            add "models/outfits/outfitAhsoka/slave2.png" at right 
    if outfit == 11:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/pitgirl.png" at right
        else:
            add "models/outfits/outfitAhsoka/pitgirl.png" at right #Pitgirl uniform
    if outfit == 12:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/exoticDress.png" at right
        else:
            add "models/outfits/outfitAhsoka/exoticDress.png" at right #Exotic Dress
    if outfit == 13:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/sleepwear.png" at right
        else:
            add "models/outfits/outfitAhsoka/sleepwear.png" at right # T-shirt with yoda's
    if outfit == 15:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/xmasAhsokaOutfit.png" at right
        else:
            add "models/outfits/outfitAhsoka/xmasAhsokaOutfit.png" at right # Christmas outfit
    if outfit == 16:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/halloweenAhsokaBikini.png" at right
        else:
            add "models/outfits/outfitAhsoka/halloweenAhsokaBikini.png" at right # Halloween bikini
    if outfit == 17:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/lekkuBasicEvent1.png" at right #See-through top
        else:
            add "models/outfits/outfitAhsoka/lekkuBasicEvent1.png" at right #See-through top
    if outfit == 18:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/armor.png" at right #Armor
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right #Armor
    if outfit == 19:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaS3.png" at right #
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right #
    if outfit == 20:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaS3Evil.png" at right 
    if outfit == 21:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBikini.png" at right 
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right
    if outfit == 22:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaTrooper.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right
    if outfit == 23:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaOrgy.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right
    if outfit == 24:
        if samActive == True:
            if body == 0:
                add "models/outfits/outfitAhsoka/sam/ahsokaOrange.png" at right
            if body == 1:
                add "models/outfits/outfitAhsoka/sam/ahsokaOrange.png" at right
            if body == 2:
                add "models/outfits/outfitAhsoka/sam/ahsokaOrange.png" at right
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at right
        
    if ahsokaExpression == 0:
        add "models/emotions/none.png" at right #None
    if ahsokaExpression == 1:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/angry1.png" at right #angry1
            else:
                add "models/emotions/ahsokaEmotesHuman/angry1.png" at right #angry1       
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/angry1.png" at right #angry1
        else:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/angry1.png" at right #angry1
            else:
                add "models/emotions/angry1.png" at right #angry1
    if ahsokaExpression == 2:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/angry2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/angry2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/angry2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/angry2.png" at right #angry2
            else:
                add "models/emotions/angry2.png" at right #angry2
    if ahsokaExpression == 3:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed1.png" at right #closed1
            else:
                add "models/emotions/closed1.png" at right #closed1
    if ahsokaExpression == 4:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed2.png" at right #closed2
            else:
                add "models/emotions/closed2.png" at right #closed2
    if ahsokaExpression == 5:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed3.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed3.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed3.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed3.png" at right #closed3
            else:
                add "models/emotions/closed3.png" at right #closed3
    if ahsokaExpression == 6:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/hesitant1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/hesitant1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/hesitant1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/hesitant1.png" at right #hesitant1
            else:
                add "models/emotions/hesitant1.png" at right #hesitant1
    if ahsokaExpression == 7:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/naughty1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/naughty1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/naughty1.png" at right #
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/naughty1.png" at right #naughty1
            else:
                add "models/emotions/naughty1.png" at right #naughty1
    if ahsokaExpression == 8:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/naughty2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/naughty2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/naughty2.png" at right #
        if body == 0:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/naughty2.png" at right #naughty2
            else:
                add "models/emotions/naughty2.png" at right #naughty2
    if ahsokaExpression == 9:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/normal1.png" at right #normal1
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/normal1.png" at right #
        if body == 0:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/normal1.png" at right #
            else:
                add "models/emotions/normal1.png" at right #
    if ahsokaExpression == 10:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/reluctant1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/reluctant1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/reluctant1.png" at right #
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/reluctant1.png" at right #reluctant1
            else:
                add "models/emotions/reluctant1.png" at right #relucant1
    if ahsokaExpression == 11:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/surprised1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/surprised1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/surprised1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/surprised1.png" at right #surprised1
            else:
                add "models/emotions/surprised1.png" at right #surprised1
    if ahsokaExpression == 12:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/unsure1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/unsure1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/unsure1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/unsure1.png" at right
            else:
                add "models/emotions/unsure1.png" at right
    if ahsokaExpression == 13:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed4.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed4.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed4.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed4.png" at right #closed4
            else:
                add "models/emotions/closed4.png" at right #closed4
    if ahsokaExpression == 14:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/confused1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/confused1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/confused1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/confused1.png" at right #confused1
            else:
                add "models/emotions/confused1.png" at right #Confused1
    if ahsokaExpression == 15:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/confused2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/confused2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/confused2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/confused2.png" at right #confused2
            else:
                add "models/emotions/confused2.png" at right #Confused2
    if ahsokaExpression == 16:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/confused3.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/confused3.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/confused3.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/confused3.png" at right #confused3
            else:
                add "models/emotions/confused3.png" at right #Confused3
    if ahsokaExpression == 17:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed5.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed5.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed5.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed5.png" at right #closed5
            else:
                add "models/emotions/closed5.png" at right #closed5
    if ahsokaExpression == 18:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed6.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed6.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed6.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed6.png" at right #closed6
            else:
                add "models/emotions/closed6.png" at right #closed6
    if ahsokaExpression == 19:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/angry3.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/angry3.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/angry3.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/angry3.png" at right #angry3
            else:
                add "models/emotions/angry3.png" at right #angry3
    if ahsokaExpression == 20:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/reluctant2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/reluctant2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/reluctant2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/reluctant2.png" at right #reluctant2
            else:
                add "models/emotions/reluctant2.png" at right #reluctant2
    if ahsokaExpression == 21:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/normal2.png" at right
            else:
                add "models/emotions/normal2.png" at right   
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/normal2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/normal2.png" at right
            else:
                add "models/emotions/normal2.png" at right
    if ahsokaExpression == 22:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/confused4.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/confused4.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/confused4.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/confused4.png" at right #confused4
            else:
                add "models/emotions/confused4.png" at right #confused4
    if ahsokaExpression == 23:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/closed7.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/closed7.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/closed7.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/closed7.png" at right #closed7
            else:
                add "models/emotions/closed7.png" at right #closed7
    if ahsokaExpression == 24:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/human/pleasure1.png" at right #pleasure1
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/pleasure1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure1.png" at right
            else:
                add "models/emotions/pleasure1.png" at right
    if ahsokaExpression == 25:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure2.png" at right #pleasure2
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/pleasure2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure2.png" at right
            else:
                add "models/emotions/pleasure2.png" at right
    if ahsokaExpression == 26:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure3.png" at right #pleasure3
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/pleasure3.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure3.png" at right
            else:
                add "models/emotions/pleasure3.png" at right
    if ahsokaExpression == 27:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure4.png" at right #pleasure4
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/pleasure4.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/pleasure4.png" at right
            else:
                add "models/emotions/pleasure4.png" at right
    if ahsokaExpression == 28:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/sad1.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/sad1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/sad1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/sad1.png" at right #sad1
            else:
                add "models/emotions/sad1.png" at right #sad1
    if ahsokaExpression == 29:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/sad2.png" at right
            else:
                add "models/emotions/ahsokaEmotesHuman/sad2.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/sad2.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/sad2.png" at right #sad2
            else:
                add "models/emotions/sad2.png" at right #sad2
    if ahsokaExpression == 30:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/evil1.png" at right #Evil 1
            else:
                add "models/emotions/ahsokaEmotesHuman/evil1.png" at right     
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/evil/evil1.png" at right #angry1
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/evil1.png" at right #Evil 1
            else:
                add "models/emotions/evil1.png" at right #Evil 1
                
    if ahsokaBlush == 0:
        add "models/emotions/none.png" at right #No blush
    if ahsokaBlush == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/blush1.png" at right
        else:
            add "models/emotions/blush1.png" at right

    if ahsokaTears == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaTears.png" at right #Tears
        else:
            add "models/emotions/ahsokaTears.png" at right #Tears
        
    #Face Cum
    if ahsokaFaceCum == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/faceCum1.png" at right
        else:
            add "models/emotions/faceCum1.png" at right
    if ahsokaFaceCum == 2:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/faceCum2.png" at right
        else:
            add "models/emotions/faceCum2.png" at right
    if ahsokaFaceCum == 3:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/faceCum3.png" at right
        else:
            add "models/emotions/faceCum3.png" at right
        
    #Body Cum
    if ahsokaBodyCum == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaBodyCum1.png" at right
        else:
            add "models/emotions/ahsokaBodyCum1.png" at right
    if ahsokaBodyCum == 2:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaBodyCum2.png" at right
        else:
            add "models/emotions/ahsokaBodyCum2.png" at right
    if ahsokaBodyCum == 3:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaBodyCum3.png" at right
        else:
            add "models/emotions/ahsokaBodyCum3.png" at right
            
    #Accessories2
    if accessories2 == 4:
        add "models/accessories/ahsokaTail.png" at right
    if accessories2 == 5:
        if samActive == True:
            add "models/accessories/sam/ahsokaGag.png" at right
        else:
            add "models/accessories/ahsokaGag.png" at right
    if accessories2 == 7:
        if samActive == True:
            add "models/accessories/sam/collar1.png" at right
    if accessories2 == 8:
        if samActive == True:
            add "models/accessories/sam/collar2.png" at right
    if accessories2 == 9:
        if samActive == True:
            add "models/accessories/sam/collar3.png" at right
    if accessories2 == 10:
        if samActive == True:
            add "models/accessories/sam/collar4.png" at right
            
    #Hair
    if hair == 0:
        if samActive == True:
            add "models/mods/sam/hair1.png" at right # Lekku
        else:
            add "models/mods/hair1.png" at right # Lekku
    if hair == 1:
        if samActive == True:
            add "models/mods/sam/hairHalloween.png" at right # Halloween lekku
        else:
            add "models/mods/hairHalloween.png" at right # Halloween lekku
    if hair == 2:
        if samActive == True:
            add "models/mods/sam/hairChristmas.png" at right # Halloween lekku
        else:
            add "models/mods/hairChristmas.png" at right # Christmas lekku
    if hair == 3:
        if samActive == True:
            add "models/mods/sam/hairNeon.png" at right # Halloween lekku
        else:
            add "models/mods/hairNeon.png" at right 
    if hair == 4:
        if samActive == True:
            add "models/mods/sam/hair2.png" at right
        else:
            add "models/mods/hair1.png" at right 
    if hair == 5:
        if samActive == True:
            add "models/mods/sam/hair3.png" at right
        else:
            add "models/mods/hair1.png" at right 
    if hair == 6:
        if samActive == True:
            add "models/mods/sam/hair4.png" at right
        else:
            add "models/mods/hair1.png" at right 
            
        
    #Accessories
    if accessories == 1:
        if samActive == True:
            add "models/mods/sam/slaveHeaddress.png" at right # Slave golden headdress
        else:
            add "models/accessories/slaveHeaddress.png" at right # Slave golden headdress
    if accessories == 3:
        if samActive == True:
            add "models/accessories/sam/ahsokaBlindfold.png" at right
        else:
            add "models/accessories/ahsokaBlindfold.png" at right
    if accessories == 15:
        if samActive == True:
            add "models/accessories/sam/ahsokaXmasHat.png" at right
        else:
            add "models/accessories/ahsokaXmasHat.png" at right
            
    if accessories2 == 6:
        if samActive == True:
            add "models/accessories/sam/ahsokaGagNeck.png" at right
        else:
            add "models/accessories/ahsokaGag.png" at right
        
            
    if armorEquipped == True and outfit == 18:
        add "models/outfits/outfitAhsoka/sam/armorTop.png" at right
    

        
        ##################### Mirrored #######################
        
screen ahsoka_main2: #Ahsoka ancor
    if body == 0:
        if samActive == True:
            add "models/skin/sam/ahsokaNudeM.png" at left
        else:
            add "models/skin/ahsokaNudeM.png" at left
    if body == 1:
        if samActive == True:
            add "models/skin/sam/ahsokaNudeHumanM.png" at left
        else:
            add "models/skin/ahsokaNudeHumanM.png" at left
    if body == 2:
        if samActive == True:
            add "models/skin/sam/ahsokaEvilM.png" at left
        else:
            add "models/skin/ahsokaNudeM.png" at left
            
    #Tattoos body
    if tattoosBody == 0:
        if samActive == True:
            add "models/mods/ahsokaNoMods.png" at left
    if tattoosBody == 1:
        if samActive == True:
            add "models/mods/sam/tattooSet1BodyM.png" at left # Basic tattoos
    if tattoosBody == 2:
        if samActive == True:
            add "models/mods/sam/tattooSet2BodyM.png" at left # Tattoo 2
    if tattoosBody == 3:
        if samActive == True:
            add "models/mods/sam/tattooSet3BodyM.png" at left # Tattoo 3
    if tattoosBody == 4:
        if samActive == True:
            add "models/mods/sam/tattooSet4BodyM.png" at left # Tattoo 4
    if tattoosBody == 5:
        if samActive == True:
            add "models/mods/sam/tattooSet5BodyM.png" at left # Tattoo 5
    
    #Tattoos face
    if tattoosFace == 0:
        if samActive == True:
            add "models/mods/ahsokaNoMods.png" at left
    if tattoosFace == 1:
        if samActive == True:
            add "models/mods/sam/tattooSet1FaceM.png" at left # Basic tattoos
    if tattoosFace == 2:
        if samActive == True:
            add "models/mods/sam/tattooSet2FaceM.png" at left # Tattoo 2
    if tattoosFace == 3:
        if samActive == True:
            add "models/mods/sam/tattooSet3FaceM.png" at left # Tattoo 3
    if tattoosFace == 4:
        if samActive == True:
            add "models/mods/sam/tattooSet4FaceM.png" at left # Tattoo 4
    if tattoosFace == 5:
        if samActive == True:
            add "models/mods/sam/tattooSet5FaceM.png" at left # Tattoo 5
            
    #Underwear
            
    #Underwear Bottom
    if underwearBottom == 1:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearBottom1M.png" at left #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwearM.png" at left #Basic underwearBottom
    if underwearBottom == 2:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearBottom2M.png" at left #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwearM.png" at left #Basic underwearBottom
    if underwearBottom == 3:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear2BottomM.png" at left #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwearM.png" at left #Basic underwearBottom
    if underwearBottom == 4:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear3BottomM.png" at left #Basic underwearBottom
        else:
            add "models/outfits/outfitAhsoka/ahsokaUnderwearM.png" at left #Basic underwearBottom
            
    #Underwear Top
    if samActive == True:
        if underwearTop == 1:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwearTopM.png" at left #Basic underwearTop  
        if underwearTop == 3:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear2TopM.png" at left #
        if underwearTop == 4:
            add "models/outfits/outfitAhsoka/sam/ahsokaUnderwear3TopM.png" at left
    else:
        add "models/outfits/outfitAhsoka/ahsokaUnderwear.png" at left
        
    #Accessories 3
    if accessories3 == 1:
        if samActive == True:
            add "models/accessories/sam/ahsokaStockingsM.png" at left    
    #Outfit
    if outfit == 1:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicM.png" at left #Basic outfit
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicM.png" at left #Basic outfit
    if outfit == 2:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicPantslessM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicPantslessM.png" at left #Basic outfit no leggings
    if outfit == 3:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicShortSkirtM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicShortSkirtM.png" at left #Basic short skirt
    if outfit == 4:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBasicTopLiftM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicTopLiftM.png" at left #Basic top pulled up.
    if 5<= outfit <= 7:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/lekkuBasicM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/lekkuBasicM.png" at left #Nemtek outfit
    if outfit == 8:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/slave1M.png" at left 
        else:
            add "models/outfits/outfitAhsoka/slave1M.png" at left #Nemtek outfit
    if outfit == 10:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/slave1M.png" at left 
        else:
            add "models/outfits/outfitAhsoka/slave1M.png" at left #Nemtek outfit
    if outfit == 11:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/pitgirlM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/pitgirlM.png" at left #Pitgirl uniform
    if outfit == 12:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/exoticDressM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/exoticDressM.png" at left #Exotic Dress
    if outfit == 15:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/xmasAhsokaOutfitM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/xmasAhsokaOutfitM.png" at left #Xmas Dress
    if outfit == 16:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/halloweenAhsokaBikiniM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/halloweenAhsokaBikiniM.png" at left # Halloween bikini
    if outfit == 18:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/armorM.png" at left #Armor
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasicM.png" at left #Armor
    if outfit == 19:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaS3M.png" at left #
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at left #
    if outfit == 20:
        if samActive == True:
            if body == 2:
                add "models/outfits/outfitAhsoka/sam/ahsokaS3EvilM.png" at left 
            else:
                add "models/outfits/outfitAhsoka/sam/ahsokaS3M.png" at left 
    if outfit == 21:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaBikiniM.png" at left 
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at left #
    if outfit == 22:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaTrooperM.png" at left
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at left
    if outfit == 23:
        if samActive == True:
            add "models/outfits/outfitAhsoka/sam/ahsokaOrgyM.png" at left
        else:
            add "models/outfits/outfitAhsoka/ahsokaBasic.png" at left
        
    if ahsokaExpression == 0:
        add "models/emotions/none.png" at left #None
    if ahsokaExpression == 1:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/angry1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/angry1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/angry1.png" at left
        else:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/angry1.png" at left #angry1
            else:
                add "models/emotions/ahsokaEmotesMirrored/angry1.png" at left #angry1
    if ahsokaExpression == 2:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/angry2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/angry2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/angry2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/angry2.png" at left #angry2
            else:
                add "models/emotions/ahsokaEmotesMirrored/angry2.png" at left #angry2
    if ahsokaExpression == 3:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed1.png" at left #closed1
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed1.png" at left #closed1
    if ahsokaExpression == 4:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed2.png" at left #closed2
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed2.png" at left #closed2
    if ahsokaExpression == 5:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed3.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed3.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed3.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed3.png" at left #closed3
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed3.png" at left #closed3
    if ahsokaExpression == 6:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/hesitant1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/hesitant1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/hesitant1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/hesitant1.png" at left #hesitant1
            else:
                add "models/emotions/ahsokaEmotesMirrored/hesitant1.png" at left #hesitant1
    if ahsokaExpression == 7:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/naughty1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/naughty1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/naughty1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/naughty1.png" at left #naughty1
            else:
                add "models/emotions/ahsokaEmotesMirrored/naughty1.png" at left #naughty1
    if ahsokaExpression == 8:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/naughty2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/naughty2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/naughty2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/naughty2.png" at left #naughty2
            else:
                add "models/emotions/ahsokaEmotesMirrored/naughty2.png" at left #naughty2
    if ahsokaExpression == 9:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/normal1.png" at left #normal1
            else:
                add "models/emotions/ahsokaEmotesMirrored/normal1.png" at left #normal1
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/normal1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/normal1.png" at left #
            else:
                add "models/emotions/ahsokaEmotesMirrored/normal1.png" at left #
    if ahsokaExpression == 10:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/reluctant1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/reluctant1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/reluctant1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/reluctant1.png" at left #reluctant1
            else:
                add "models/emotions/ahsokaEmotesMirrored/reluctant1.png" at left #relucant1
    if ahsokaExpression == 11:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/surprised1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/surprised1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/surprised1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/surprised1.png" at left #surprised1
            else:
                add "models/emotions/ahsokaEmotesMirrored/surprised1.png" at left #surprised1
    if ahsokaExpression == 12:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/unsure1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/unsure1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/unsure1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/unsure1.png" at left #unsure1
            else:
                add "models/emotions/ahsokaEmotesMirrored/unsure1.png" at left #unsure1
    if ahsokaExpression == 13:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed4.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed4.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed4.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed4.png" at left #closed4
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed4.png" at left #closed4
    if ahsokaExpression == 14:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/confused1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/confused1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/confused1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/confused1.png" at left 
            else:
                add "models/emotions/ahsokaEmotesMirrored/confused1.png" at left #Confused1
    if ahsokaExpression == 15:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/confused2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/confused2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/confused2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/confused2.png" at left #confused2
            else:
                add "models/emotions/ahsokaEmotesMirrored/confused2.png" at left #Confused2
    if ahsokaExpression == 16:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/confused3.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/confused3.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/confused3.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/confused3.png" at left #confused3
            else:
                add "models/emotions/ahsokaEmotesMirrored/confused3.png" at left #Confused3
    if ahsokaExpression == 17:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed5.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed5.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed5.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed5.png" at left #closed5
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed5.png" at left #closed5
    if ahsokaExpression == 18:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed6.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed6.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed6.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed6.png" at left #closed6
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed6.png" at left #closed6
    if ahsokaExpression == 19:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/angry3.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/angry3.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/angry3.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/angry3.png" at left #angry3
            else:
                add "models/emotions/ahsokaEmotesMirrored/angry3.png" at left #angry3
    if ahsokaExpression == 20:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/reluctant2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/reluctant2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/reluctant2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/reluctant2.png" at left #reluctant2
            else:
                add "models/emotions/ahsokaEmotesMirrored/reluctant2.png" at left #reluctant2
    if ahsokaExpression == 21:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/normal2.png" at left #normal2
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/normal2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/normal2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/normal2.png" at left
    if ahsokaExpression == 22:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/confused4.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/confused4.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/confused4.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/confused4.png" at left #confused4
            else:
                add "models/emotions/ahsokaEmotesMirrored/confused4.png" at left #confused4
    if ahsokaExpression == 23:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/closed7.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/closed7.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/closed7.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/closed7.png" at left #closed7
            else:
                add "models/emotions/ahsokaEmotesMirrored/closed7.png" at left #closed7
    if ahsokaExpression == 24:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/pleasure1.png" at left #pleasure1
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/pleasure1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/pleasure1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/pleasure1.png" at left
    if ahsokaExpression == 25:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/pleasure2.png" at left #pleasure2
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/pleasure2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/pleasure2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/pleasure2.png" at left
    if ahsokaExpression == 26:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/pleasure3.png" at left #pleasure3
            elif body == 2:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/pleasure3.png" at left
        if body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/pleasure3.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/pleasure3.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/pleasure3.png" at left
    if ahsokaExpression == 27:
        if body == 1:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/pleasure4.png" at left #pleasure4
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/pleasure4.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/pleasure4.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/pleasure4.png" at left
    if ahsokaExpression == 28:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/sad1.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/sad1.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/sad1.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/sad1.png" at left #sad1
            else:
                add "models/emotions/ahsokaEmotesMirrored/sad1.png" at left #sad1
    if ahsokaExpression == 29:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/sad2.png" at left
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/sad2.png" at left
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/sad2.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/sad2.png" at left #sad2
            else:
                add "models/emotions/ahsokaEmotesMirrored/sad2.png" at left #sad2
    if ahsokaExpression == 30:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/human/evil1.png" at left #evil1
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/angry3.png" at left #evil1
        elif body == 2:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil/angry3.png" at left
        else:
            if samActive == True:
                add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/evil1.png" at left #evil1
            else:
                add "models/emotions/ahsokaEmotesMirrored/angry3.png" at left 
                
    #Hair
    if hair == 0:
        if samActive == True:
            add "models/mods/sam/hair1M.png" at left # Lekku
    if hair == 1:
        if samActive == True:
            add "models/mods/sam/hairHalloweenM.png" at left # Halloween lekku
    if hair == 2:
        if samActive == True:
            add "models/mods/sam/hairChristmasM.png" at left # Halloween lekku
    if hair == 3:
        if samActive == True:
            add "models/mods/sam/hairNeonM.png" at left
    if hair == 4:
        if samActive == True:
            add "models/mods/sam/hair2M.png" at left
    if hair == 5:
        if samActive == True:
            add "models/mods/sam/hair3M.png" at left
    if hair == 6:
        if samActive == True:
            add "models/mods/sam/hair4M.png" at left
            
    if ahsokaTears == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/ahsokaTears.png" at left #Tears
        else:
            add "models/emotions/ahsokaTears.png" at right #Tears
        
    if ahsokaBlush == 0:
        add "models/emotions/none.png" at left #No blush
    if ahsokaBlush == 1:
        if samActive == True:
            add "models/emotions/ahsokaEmotesSam/ahsokaEmotesMirrored/blush1.png" at left
        else:
            add "models/emotions/blush1m.png" at left
        
    if hair == 0:
        if samActive == True:
            add "models/mods/sam/hair1M.png" at left # Lekku
        else:
            add "models/mods/hair1M.png" at left # Lekku
            
    if armorEquipped == True:
        add "models/outfits/outfitAhsoka/sam/armorTopM.png" at left
        
        
screen ahsoka_main3: #Ahsoka evil ancor OLD CODE
    if body == 2:
        if samActive == True:
            add "models/skin/sam/ahsokaEvil.png" at right
        else:
            add "models/skin/ahsokaNude.png" at right
            
        if breastSize == 1:
            if body == 0:
                add "models/skin/sam/ahsokaBreastsMd.png" at right
        if breastSize == 2:
            if body == 0:
                add "models/skin/sam/ahsokaBreastsLg.png" at right
            
        if outfit == 19:
            add "models/outfits/outfitAhsoka/sam/ahsokaS3.png" at right
        if outfit == 20:
            add "models/outfits/outfitAhsoka/sam/ahsokaS3Evil.png" at right
        if hair == 2:
            add "models/mods/sam/hair2.png" at right
            
    if ahsokaExpression == 30:
        if body == 1:
            if samActive == True: 
                add "models/emotions/ahsokaEmotesSam/human/evil1.png" at right #Evil 1
            else:
                add "models/emotions/ahsokaEmotesMirrored/human/angry3.png" at right #evil1
                
    if ahsokaExpression == 31:
        if samActive == True: 
            add "models/emotions/ahsokaEmotesSam/evil2.png" at right #evil1
        else:
            add "models/emotions/ahsokaEmotesMirrored/human/angry3.png" at right #evil1

        
        
        ############### Drug trip ######################
        
screen ahsoka_drugs: #Ahsoka ancor
    add "models/skin/drugTrip/ahsokaNudeNeon.png" at right
    
    if ahsokaExpression == 3:
        add "models/skin/drugTrip/closedNeon.png"  at right
    if ahsokaExpression == 8:
        add "models/skin/drugTrip/naughtyNeon.png"  at right
    if ahsokaExpression == 11:
        add "models/skin/drugTrip/surprisedNeon.png"  at right
        
        
        
#################################### Groping scene #################################################       
define gropeUnderwear = 1
define gropeOutfit = 1
define gropeExpression = 1
define gropeCock = 0
define gropeCum = 0

screen grope_scene1:
    add "XXX/grope/gropeSceneBG.jpg"
    
    if gropeUnderwear == 1:
        add "XXX/grope/gropeUnderwear.png"
        
    if gropeCock == 1:
        add "XXX/grope/gropeCock.png"

    if gropeOutfit == 1:
        add "XXX/grope/gropeClothes.png"
        
    if gropeCum == 1:
        add "XXX/grope/gropeCum.png"
    if gropeCum == 2:
        add "XXX/grope/gropeCum2.png"
        
    if gropeExpression == 1:
        add "XXX/grope/gropeAngry.png"
    if gropeExpression == 2:
        add "XXX/grope/gropeClosed.png"
    if gropeExpression == 3:
        add "XXX/grope/gropeSurprised.png" 
        
    if ahsokaBlush == 1:
        add "XXX/grope/gropeBlush.png" 
        
#################################### Dancing scene #################################################       
define poleDuringDance = 0
define danceUnderwear = 1
define danceExpression = 1
define danceCum = 0

screen dance_scene1:
    if poleDuringDance == 1:
        add "XXX/dance/ahsokaDancingPole.png"
        
    add "XXX/dance/ahsokaDancing1.png"
    
    if 1 <= outfit <= 100:
        add "XXX/dance/ahsokaDancingOutfit1.png"
        
    if danceExpression == 1:
        add "XXX/dance/ahsokaDancingExpression1.png"
        
    if danceCum == 1:
        add "XXX/dance/danceCum1.png"
    if danceCum == 2:
        add "XXX/dance/danceCum2.png"
        
        
#################################### Dancing scene 2 #################################################
define dance2Ahs = 0
define dance2Shin = 0
define dance2Kit = 0
define dance2AhsOutfitTop = 1
define dance2ShinOutfitTop = 1
define dance2KitOutfitTop = 1
define dance2AhsOutfitBottom = 1
define dance2ShinOutfitBottom = 1
define dance2KitOutfitBottom = 1

screen dance_scene2:
    add "XXX/dance2/danceScene2.jpg"
    
    if dance2Ahs == 1:
        add "XXX/dance2/danceAhsoka.png"
    if dance2Shin == 1:
        add "XXX/dance2/danceShin.png"
    if dance2Kit == 1:
        add "XXX/dance2/danceKit.png"
    
    if dance2AhsOutfitTop == 1 and dance2Ahs == 1:
        add "XXX/dance2/danceAhsokaOutfit1Top.png"
    if dance2ShinOutfitTop == 1 and dance2Shin == 1:
        add "XXX/dance2/danceShinOutfit1Top.png"
    if dance2KitOutfitTop == 1 and dance2Kit == 1:
        add "XXX/dance2/danceKitOutfit1Top.png"
        
    if dance2AhsOutfitBottom == 1 and dance2Ahs == 1:
        add "XXX/dance2/danceAhsokaOutfit1Bottom.png"
    if dance2ShinOutfitBottom == 1 and dance2Shin == 1:
        add "XXX/dance2/danceShinOutfit1Bottom.png"
    if dance2KitOutfitBottom == 1 and dance2Kit == 1:
        add "XXX/dance2/danceKitOutfit1Bottom.png"
        
#################################### hand scene #################################################       
define jerkExpression = 1
define jerkHand = 1
define jerkCum = 0

screen jerk_scene1:
    add "XXX/jerk/jerkSceneBG.jpg"
        
    if jerkHand == 1:
        add "XXX/jerk/jerkHand1.png"
    if jerkHand == 2:
        add "XXX/jerk/jerkHand2.png"
        
    if jerkExpression == 1:
        add "XXX/jerk/jerkExpression1.png"
    if jerkExpression == 2:
        add "XXX/jerk/jerkExpression2.png"
    if jerkExpression == 3:
        add "XXX/jerk/jerkExpression3.png" 
    if jerkExpression == 4:
        add "XXX/jerk/jerkExpression4.png"
    if jerkExpression == 5:
        add "XXX/jerk/jerkExpression5.png" 
    if jerkExpression == 6:
        add "XXX/jerk/jerkExpression6.png" 
    if jerkExpression == 7:
        add "XXX/jerk/jerkExpression7.png" 
        
    if jerkCum == 1:
        add "XXX/jerk/jerkCum1.png"
    if jerkCum == 2:
        add "XXX/jerk/jerkCum2.png"
    if jerkCum == 3:
        add "XXX/jerk/jerkCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/jerk/jerkBlush.png" 
        
        
#################################### Butt scene #################################################       
define buttExpression = 1
define buttCum = 0
define buttExpression = 1

screen butt_scene1:
    add "XXX/butt/buttSceneBG.jpg"
        
    if buttExpression == 1:
        add "XXX/butt/buttExpression1.png"              #Normal
    if buttExpression == 2:
        add "XXX/butt/buttExpression2.png"              #Angry
    if buttExpression == 3:
        add "XXX/butt/buttExpression3.png"              #Surprised
    if buttExpression == 4:
        add "XXX/butt/buttExpression4.png"              #Closed
    if buttExpression == 5:
        add "XXX/butt/buttExpression5.png"              #Closed angry
    if buttExpression == 6:
        add "XXX/butt/buttExpression6.png"              #Closed happy
        
    if buttCum == 1:
        add "XXX/butt/buttCum1.png" 
    if buttCum == 2:
        add "XXX/butt/buttCum2.png" 
    if buttCum == 3:
        add "XXX/butt/buttCum3.png" 
        
        
#################################### blow scene1 #################################################       
define blowExpression = 1
define blowHand = 1
define blowCum = 0

screen blow_scene1:
    add "XXX/blow/blowjob1/blowScene1.jpg"
        
    if blowHand == 1:
        add "XXX/blow/blowjob1/blowHand1.png"
    if blowHand == 2:
        add "XXX/blow/blowjob1/blowHand2.png"
        
    if blowExpression == 1:
        add "XXX/blow/blowjob1/blowExpression1.png"
    if blowExpression == 2:
        add "XXX/blow/blowjob1/blowExpression2.png"
    if blowExpression == 3:
        add "XXX/blow/blowjob1/blowExpression3.png" 
    if blowExpression == 4:
        add "XXX/blow/blowjob1/blowExpression4.png"
    if blowExpression == 5:
        add "XXX/blow/blowjob1/blowExpression5.png" 
    if blowExpression == 6:
        add "XXX/blow/blowjob1/blowExpression6.png" 
        
    if blowCum == 1:
        add "XXX/blow/blowjob1/blowCum1.png"
    if blowCum == 2:
        add "XXX/blow/blowjob1/blowCum2.png"
    if blowCum == 3:
        add "XXX/blow/blowjob1/blowCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/blow/blowjob1/blowBlush.png" 
        
#################################### blow scene2 #################################################       
# see defines in previous blow scene

screen blow_scene2:
    add "XXX/blow/blowjob2/blowScene2.jpg"
        
    if blowHand == 1:
        add "XXX/blow/blowjob2/blowHand1.png"
    if blowHand == 2:
        add "XXX/blow/blowjob2/blowHand2.png"
        
    if blowExpression == 1:
        add "XXX/blow/blowjob2/blowExpression1.png"
    if blowExpression == 2:
        add "XXX/blow/blowjob2/blowExpression2.png"
    if blowExpression == 3:
        add "XXX/blow/blowjob2/blowExpression3.png" 
    if blowExpression == 4:
        add "XXX/blow/blowjob2/blowExpression4.png"
    if blowExpression == 5:
        add "XXX/blow/blowjob2/blowExpression5.png" 
    if blowExpression == 6:
        add "XXX/blow/blowjob2/blowExpression6.png" 
        
    if blowCum == 1:
        add "XXX/blow/blowjob2/blowCum.png"
    if blowCum == 2:
        add "XXX/blow/blowjob2/blowCum2.png"
    if blowCum == 3:
        add "XXX/blow/blowjob2/blowCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/blow/blowjob2/blowBlush.png" 
        
#################################### blow scene3 #################################################       
# see defines in previous blow scene

screen blow_scene3:
    add "XXX/blow/blowjob3/blowScene3.jpg"
        
    if blowExpression == 1:
        add "XXX/blow/blowjob3/blowExpression1.png"
    if blowExpression == 2:
        add "XXX/blow/blowjob3/blowExpression2.png"
    if blowExpression == 3:
        add "XXX/blow/blowjob3/blowExpression3.png" 
    if blowExpression == 4:
        add "XXX/blow/blowjob3/blowExpression4.png"
    if blowExpression == 5:
        add "XXX/blow/blowjob3/blowExpression5.png" 
    if blowExpression == 6:
        add "XXX/blow/blowjob3/blowExpression6.png" 
        
    if blowCum == 1:
        add "XXX/blow/blowjob3/blowCum1.png"
    if blowCum == 2:
        add "XXX/blow/blowjob3/blowCum2.png"
    if blowCum == 3:
        add "XXX/blow/blowjob3/blowCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/blow/blowjob3/blowBlush.png" 
        
#################################### blow scene4 #################################################       
# see defines in previous blow scene

screen blow_scene4:
    add "XXX/blow/blowjob4/blowScene4.jpg"
        
    if blowExpression == 1:
        add "XXX/blow/blowjob4/blowExpression1.png"
    if blowExpression == 2:
        add "XXX/blow/blowjob4/blowExpression2.png"
    if blowExpression == 3:
        add "XXX/blow/blowjob4/blowExpression3.png" 
    if blowExpression == 4:
        add "XXX/blow/blowjob4/blowExpression4.png"
    if blowExpression == 5:
        add "XXX/blow/blowjob4/blowExpression5.png" 
    if blowExpression == 6:
        add "XXX/blow/blowjob4/blowExpression6.png" 
        
    if blowCum == 1:
        add "XXX/blow/blowjob4/blowCum1.png"
    if blowCum == 2:
        add "XXX/blow/blowjob4/blowCum2.png"
    if blowCum == 3:
        add "XXX/blow/blowjob4/blowCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/blow/blowjob4/blowBlush.png" 
        
        
#################################### sex scene1 (Ahsoka) #################################################       
define sexExpression = 4
define sexCum = 0

screen sex_scene1:
    add "XXX/sex/sexAhsoka1/sexScene1.jpg"
        
    if sexExpression == 1:
        add "XXX/sex/sexAhsoka1/sexExpression1.png"                # Struggling
    if sexExpression == 2:
        add "XXX/sex/sexAhsoka1/sexExpression2.png"                # Orgasming
    if sexExpression == 3:
        add "XXX/sex/sexAhsoka1/sexExpression3.png"                # Upset
    if sexExpression == 4:
        add "XXX/sex/sexAhsoka1/sexExpression4.png"                # Moaning
        
    if sexCum == 1:
        add "XXX/sex/sexAhsoka1/sexCum1.png"
    if sexCum == 2:
        add "XXX/sex/sexAhsoka1/sexCum2.png"
    if sexCum == 3:
        add "XXX/sex/sexAhsoka1/sexCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/sex/sexAhsoka1/sexBlush.png" 
        
        
#################################### sex scene2 (Ahsoka) #################################################       

screen sex_scene2:
    add "XXX/sex/sexAhsoka2/sexScene2.jpg"
        
    if sexExpression == 1:
        add "XXX/sex/sexAhsoka2/sexExpression1.png"                # Unsure
    if sexExpression == 2:
        add "XXX/sex/sexAhsoka2/sexExpression2.png"                # Orgasming
    if sexExpression == 3:
        add "XXX/sex/sexAhsoka2/sexExpression3.png"                # Gritting teeth
    if sexExpression == 4:
        add "XXX/sex/sexAhsoka2/sexExpression4.png"                # Moaning
        
    if sexCum == 1:
        add "XXX/sex/sexAhsoka2/sexCum1.png"
    if sexCum == 2:
        add "XXX/sex/sexAhsoka2/sexCum2.png"
    if sexCum == 3:
        add "XXX/sex/sexAhsoka2/sexCum3.png"
        
    if ahsokaBlush == 1:
        add "XXX/sex/sexAhsoka2/sexBlush.png" 
        
#################################### sex scene3 (Ahsoka) #################################################       
define sex3Expression = 0
define sex3Cum = 0
define sexChain = 0
define sexChoke = 0
define cumOutside = 0

screen sex_scene3:
    if body == 2:
        add "XXX/sex/sexAhsoka3/sexScene3.1.jpg"
    else:
        add "XXX/sex/sexAhsoka3/sexScene3.jpg"
        
    if sex3Expression == 1:
        if body == 2:
            add "XXX/sex/sexAhsoka3/sexExpression1.1.png"
        else:
            add "XXX/sex/sexAhsoka3/sexExpression1.png"                # Struggling
    if sex3Expression == 2:
        if body == 2:
            add "XXX/sex/sexAhsoka3/sexExpression2.1.png"
        else:
            add "XXX/sex/sexAhsoka3/sexExpression2.png"                # Orgasming
    if sex3Expression == 3:
        if body == 2:
            add "XXX/sex/sexAhsoka3/sexExpression3.1.png"
        else:
            add "XXX/sex/sexAhsoka3/sexExpression3.png"                # Upset
            
    if sexChain == 1:
        add "XXX/sex/sexAhsoka3/chain.png"
        
    if sexChoke == 1:
        add "XXX/sex/sexAhsoka3/arm.png"
        
    if sex3Cum == 1:
        add "XXX/sex/sexAhsoka3/sexCum1.png"                            # inside
    if sex3Cum == 2:
        add "XXX/sex/sexAhsoka3/sexCum2.png"                            # outside
    if sex3Cum == 3:
        add "XXX/sex/sexAhsoka3/sexCum3.png"                            # outside
    if sex3Cum == 4:
        add "XXX/sex/sexAhsoka3/sexCum4.png"                            # outside
        
    if cumOutside == 1:
        add "XXX/sex/sexAhsoka3/penis.png"
        
    if ahsokaBlush == 1:
        add "XXX/sex/sexAhsoka3/sexBlush.png" 
        
#################################### anal scene1 (Ahsoka) #################################################       
define analExpression = 1
define analCum = 0
define analDildo = 0
define analStage = 0

screen anal_scene1:
    if analStage == 0:
        add "XXX/anal/1/analScene1.jpg"
        
        if analExpression == 1:
            add "XXX/anal/1/analExpression1.png"
        if analExpression == 2:
            add "XXX/anal/1/analExpression2.png"
        if analExpression == 3:
            add "XXX/anal/1/analExpression3.png"
            
        if analCum == 1:
            add "XXX/anal/1/analCum1.png"
        if analCum == 2:
            add "XXX/anal/1/analCum2.png"
        if analCum == 3:
            add "XXX/anal/1/analCum3.png"
            
        if analDildo == 1:
            add "XXX/anal/1/analDildo.png"
            
    if analStage == 1:
        add "XXX/anal/2/analScene1.jpg"
        
        if analExpression == 1:
            add "XXX/anal/2/analExpression1.png"
        if analExpression == 2:
            add "XXX/anal/2/analExpression2.png"
        if analExpression == 3:
            add "XXX/anal/2/analExpression3.png"
            
        if analCum == 1:
            add "XXX/anal/2/analCum1.png"
        if analCum == 2:
            add "XXX/anal/2/analCum2.png"
        if analCum == 3:
            add "XXX/anal/2/analCum3.png"
            
        if analDildo == 1:
            add "XXX/anal/2/analDildo.png"
        
#################################### sex scene1 (Shin) #################################################       
define sexExpression = 1
define sexCum = 0

screen shin_sex_scene:
    add "XXX/sex/sexShin1/sexScene1.jpg"
        
    if sexExpression == 1:
        add "XXX/sex/sexShin1/sexExpression1.png"                # Closed
    if sexExpression == 2:
        add "XXX/sex/sexShin1/sexExpression2.png"                # Moan
    if sexExpression == 3:
        add "XXX/sex/sexShin1/sexExpression3.png"                # Shy
    if sexExpression == 4:
        add "XXX/sex/sexShin1/sexExpression4.png"                # Excited
        
    if sexCum == 1:
        add "XXX/sex/sexShin1/sexCum1.png"
    if sexCum == 2:
        add "XXX/sex/sexShin1/sexCum2.png"
    if sexCum == 3:
        add "XXX/sex/sexShin1/sexCum3.png"
        
#################################### sex scene1 (Kit) #################################################       
define sexExpression = 1
define sexCum = 0

screen kit_sex_scene:
    add "XXX/sex/sexKit1/sexScene1.jpg"
        
    if sexCum == 1:
        add "XXX/sex/sexKit1/sexCum1.png"
    if sexCum == 2:
        add "XXX/sex/sexKit1/sexCum2.png"
    if sexCum == 3:
        add "XXX/sex/sexKit1/sexCum3.png"
        
#################################### lesb scene1 #################################################       
define lesbExpressionAhs = 1
define lesbExpressionShin = 1
define lesbCum = 0

screen lesb_scene1:
    add "XXX/lesb/lesbSceneBG.jpg"
    
    if lesbExpressionAhs == 1:
        add "XXX/lesb/lesbExpressionAhsoka1.png"
    if lesbExpressionAhs == 2:
        add "XXX/lesb/lesbExpressionAhsoka2.png"
    if lesbExpressionAhs == 3:
        add "XXX/lesb/lesbExpressionAhsoka3.png"
    if lesbExpressionAhs == 4:
        add "XXX/lesb/lesbExpressionAhsoka4.png"
        
    if lesbExpressionShin == 1:
        add "XXX/lesb/lesbExpressionShin1.png"
    if lesbExpressionShin == 2:
        add "XXX/lesb/lesbExpressionShin2.png"
    if lesbExpressionShin == 3:
        add "XXX/lesb/lesbExpressionShin3.png"
    if lesbExpressionShin == 4:
        add "XXX/lesb/lesbExpressionShin4.png"
        
    if lesbCum == 1:
        add "XXX/lesb/lesbCum1.png"
    if lesbCum == 2:
        add "XXX/lesb/lesbCum2.png"
    if lesbCum == 3:
        add "XXX/lesb/lesbCum3.png"
        
        
#################################### bondage scene1 #################################################       
define threesomeCum = 0

screen threeSome_scene1:
    add "XXX/3some/3someSceneBG.jpg"
    
    if threesomeCum == 4:
        add "XXX/3some/threesomeCum4.png"
    if threesomeCum == 5:
        add "XXX/3some/threesomeCum5.png"
    if threesomeCum == 6:
        add "XXX/3some/threesomeCum6.png"
    
#################################### foursome scene1 #################################################       
define foursomeCumShin = 0
define foursomeCumAhsoka = 0
define foursomeCumKit = 0

define foursomeToysShin = 0
define foursomeToysAhsoka = 0
define foursomeToysKit = 0

define foursomeCock = 0

screen fourSome_scene1:
    add "XXX/foursome/foursomeSceneBG.jpg"
    
    if foursomeCock == 1:
        add "XXX/foursome/foursomeDick1.png"
    if foursomeCock == 2:
        add "XXX/foursome/foursomeDick2.png"
    if foursomeCock == 3:
        add "XXX/foursome/foursomeDick3.png"
    if foursomeCock == 4:
        add "XXX/foursome/foursomeAnalDick1.png"
    if foursomeCock == 5:
        add "XXX/foursome/foursomeAnalDick2.png"
    if foursomeCock == 6:
        add "XXX/foursome/foursomeAnalDick3.png"
    
    if foursomeToysShin == 1:
        add "XXX/foursome/foursomeToyShin.png"
    if foursomeToysAhsoka == 1:
        add "XXX/foursome/foursomeToyAhsoka.png"
    if foursomeToysKit == 1:
        add "XXX/foursome/foursomeToyKit.png"
        
    if foursomeCumShin == 1:
        add "XXX/foursome/shinCum1.png"
    if foursomeCumShin == 2:
        add "XXX/foursome/shinCum2.png"
    if foursomeCumShin == 3:
        add "XXX/foursome/shinCum3.png"
        
    if foursomeCumAhsoka == 1:
        add "XXX/foursome/ahsokaCum1.png"
    if foursomeCumAhsoka == 2:
        add "XXX/foursome/ahsokaCum2.png"
    if foursomeCumAhsoka == 3:
        add "XXX/foursome/ahsokaCum3.png"
        
    if foursomeCumKit == 1:
        add "XXX/foursome/kitCum1.png"
    if foursomeCumKit == 2:
        add "XXX/foursome/kitCum2.png"
    if foursomeCumKit == 3:
        add "XXX/foursome/kitCum3.png"
        
        
#################################### Orgy scene1 #################################################       
define orgyCum = 0              # cum 0 internal, 1, 2, 3 are external
define orgyFuck = 0               # 1 Ahsoka, 2 Marieke, 3 kit, 4 human
define orgyAhsoka = 1          # Ahsoka face.
define orgyMar = 1                # Marieke face.
define orgyKit = 1                  # Kit face.
define orgyGirl = 1                # Girl face.
define orgyPenis = 1             # 1 is external, 1 is internal
define ewok = 0                    # Ewok easter egg
define orgyMask = 0             # 1 is blue, 2 is black, 3 is red
define orgyHand = 1             

screen orgy_scene1:
    add "XXX/orgy/orgySceneBG.jpg"
    
    if orgyFuck == 1:
           add "XXX/orgy/orgyAhsoka.png"
    if orgyFuck == 2:
           add "XXX/orgy/orgyMarieke.png"
    if orgyFuck == 3:
           add "XXX/orgy/orgyKit.png"
    if orgyFuck == 4:
           add "XXX/orgy/orgyGirl.png"
           
    if orgyAhsoka == 1:
       add "XXX/orgy/orgyAhsoka1.png"
    if orgyAhsoka == 2:
       add "XXX/orgy/orgyAhsoka2.png"
    if orgyAhsoka == 3:
       add "XXX/orgy/orgyAhsoka3.png"
       
    if orgyMar == 1:
       add "XXX/orgy/orgyMarieke1.png"
    if orgyMar == 2:
       add "XXX/orgy/orgyMarieke2.png"
    if orgyMar == 3:
       add "XXX/orgy/orgyMarieke3.png"
       
    if orgyKit == 1:
       add "XXX/orgy/orgyKit1.png"
    if orgyKit == 2:
       add "XXX/orgy/orgyKit2.png"
    if orgyKit == 3:
       add "XXX/orgy/orgyKit3.png"
       
    if orgyGirl == 1:
       add "XXX/orgy/orgyGirl1.png"
    if orgyGirl == 2:
       add "XXX/orgy/orgyGirl2.png"
    if orgyGirl == 3:
       add "XXX/orgy/orgyGirl3.png"
       
    if orgyHand == 1:
       add "XXX/orgy/dudeHand.png"
       
    if orgyPenis == 1:
       add "XXX/orgy/orgyPenis1.png"
    if orgyPenis == 2:
       add "XXX/orgy/orgyPenis2.png"
       
    if orgyCum == 1:
       add "XXX/orgy/orgyCumInternal.png"
    if orgyCum == 2:
       add "XXX/orgy/orgyCum1.png"
    if orgyCum == 3:
       add "XXX/orgy/orgyCum2.png"
    if orgyCum == 4:
       add "XXX/orgy/orgyCum3.png"
       
    if orgyMask == 1:
       add "XXX/orgy/maskBlue.png"
    if orgyMask == 2:
       add "XXX/orgy/maskBlack.png"
    if orgyMask == 3:
       add "XXX/orgy/maskRed.png"
       
    if ewok == 1:
       add "XXX/orgy/orgyEwok.png"
    
#################################### Kolto Tank scene #################################################       
define koltoExpression = 0
define koltoOutfit = 1
define koltoTentacle = 0

screen kolto_scene1: #Ahsoka ancor
    add "XXX/kolto/koltoSceneBg.jpg"
    
    if koltoExpression == 0:                                            # facial expressions
        add "XXX/kolto/koltoExpression0.png"
    if koltoExpression == 1:
        add "XXX/kolto/koltoExpression1.png"
    if koltoExpression == 2:
        add "XXX/kolto/koltoExpression2.png"
    if koltoExpression == 3:
        add "XXX/kolto/koltoExpression3.png"
        
    if koltoOutfit == 1:                                            # underwear or naked
        add "XXX/kolto/koltoOutfit1.png"
        
    if koltoTentacle == 1:                                            # Tentacles
        add "XXX/kolto/koltoTentacle1.png"
    if koltoTentacle == 2:
        add "XXX/kolto/koltoTentacle2.png"
        
#################################### Shin Body / clothing / emotions #################################################

#define shinUnderwear = 0
define shinOutfit = 0
define shinOutfitSet = 0
define shinExpression = 25
define shinSkin = 0
define shinBlush = 0
define shinVirgin = True
define shinTears = 0
define shinMod = 0
define shinSithSkin = False

#accessories
define shinMask = 0
define accessoriesShin = 0
define accessories2Shin = 0

screen shin_main:        
    if shinSkin == 0:
        if samActive == True:
            add "models/skin/sam/shinNude.png" at right
        else:
            add "models/skin/shinNude.png" at right
    if shinSkin == 1:
        add "models/skin/shinLocked.png" at right
    if shinSkin == 2:
        add "models/skin/sam/shinNudeSith.png" at right
        
    if shinMod == 1:
        add "models/mods/sam/shinTattoo.png" at right
        
    #Accessories 2 behind clothes
    if accessories2Shin == 4:
        add "models/accessories/sam/shinTail.png" at right
        
    if shinOutfit == 0:
        add "models/outfits/ahsokaNoOutfit.png" at right
    if shinOutfit == 1:
        if samActive == True:
            add "models/outfits/sam/shinBasic.png" at right
        else:
            add "models/outfits/shinBasic.png" at right
    if shinOutfit == 2:
        if samActive == True:
            add "models/outfits/sam/shinBasicNoShirt.png" at right
        else:
            add "models/outfits/shinBasicNoShirt.png" at right
    if shinOutfit == 3:
        if samActive == True:
            add "models/outfits/sam/shinDress.png" at right
        else:
            add "models/outfits/shinBasic.png" at right
    if shinOutfit == 4:
        if samActive == True:
            add "models/outfits/sam/shinBikini.png" at right
        else:
            add "models/outfits/shinBasic.png" at right
    if shinOutfit == 5:
        if samActive == True:
            add "models/outfits/sam/shinTrooper.png" at right
        else:
            add "models/outfits/shinBasic.png" at right
    if shinOutfit == 6:
        if samActive == True:
            add "models/outfits/sam/shinSlave.png" at right
        else:
            add "models/outfits/shinBasic.png" at right
    if shinOutfit == 15:
        if samActive == True:
            add "models/outfits/sam/shinChristmas.png" at right # Halloween outfit Shin
        else:
            add "models/outfits/shinChristmas.png" at right # Halloween outfit Shin
    if shinOutfit == 16:
        if samActive == True:
            add "models/outfits/sam/shinHalloween.png" at right # Halloween outfit Shin
        else:
            add "models/outfits/shinHalloween.png" at right # Halloween outfit Shin

    # Expressions
    if shinExpression == 0:
        add "models/emotions/none.png" at right #None
    if shinExpression == 1:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry1.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry1.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry1.png" at right
    if shinExpression == 2:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry2.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry2.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry2.png" at right
    if shinExpression == 3:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry3.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry3.png" at right
    if shinExpression == 4:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry4.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry4.png" at right
    if shinExpression == 5:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinAngry5.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry5.png" at right
    if shinExpression == 6:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinAngry6.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry6.png" at right
    if shinExpression == 7:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry7.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry7.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry7.png" at right
    if shinExpression == 8:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry8.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry8.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry8.png" at right
    if shinExpression == 9:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinAngry9.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry9.png" at right
    if shinExpression == 10:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinAngry10.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinAngry10.png" at right
        else:
            add "models/emotions/shinEmotes/shinAngry10.png" at right
    if shinExpression == 11:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinArrogant1.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinArrogant1.png" at right
        else:
            add "models/emotions/shinEmotes/shinArrogant1.png" at right
    if shinExpression == 12:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinHesitant1.png" at right
        else:
            add "models/emotions/shinEmotes/shinHesitant1.png" at right
    if shinExpression == 13:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinHesitant2.png" at right
        else:
            add "models/emotions/shinEmotes/shinHesitant2.png" at right
    if shinExpression == 14:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinHesitant3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinHesitant3.png" at right
        else:
            add "models/emotions/shinEmotes/shinHesitant3.png" at right
    if shinExpression == 15:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinHesitant4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinHesitant4.png" at right
        else:
            add "models/emotions/shinEmotes/shinHesitant4.png" at right
    if shinExpression == 16:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinNaughty1.png" at right
        else:
            add "models/emotions/shinEmotes/shinNaughty1.png" at right
    if shinExpression == 17:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinNaughty2.png" at right
        else:
            add "models/emotions/shinEmotes/shinNaughty2.png" at right
    if shinExpression == 18:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinNaughty3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinNaughty3.png" at right
        else:
            add "models/emotions/shinEmotes/shinNaughty3.png" at right
    if shinExpression == 19:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinNaughty4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinNaughty4.png" at right
        else:
            add "models/emotions/shinEmotes/shinNaughty4.png" at right
    if shinExpression == 20:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinNaughty5.png" at right
        else:
            add "models/emotions/shinEmotes/shinNaughty5.png" at right
    if shinExpression == 21:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinSad1.png" at right
        else:
            add "models/emotions/shinEmotes/shinSad1.png" at right
    if shinExpression == 22:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinSad2.png" at right
        else:
            add "models/emotions/shinEmotes/shinSad2.png" at right
    if shinExpression == 23:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSad3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSad3.png" at right
        else:
            add "models/emotions/shinEmotes/shinSad3.png" at right
    if shinExpression == 24:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSad4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSad4.png" at right
        else:
            add "models/emotions/shinEmotes/shinSad4.png" at right
    if shinExpression == 25:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinSmile1.png" at right
        else:
            add "models/emotions/shinEmotes/shinSmile1.png" at right
    if shinExpression == 26:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinSmile2.png" at right
        else:
            add "models/emotions/shinEmotes/shinSmile2.png" at right
    if shinExpression == 27:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSmile3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSmile3.png" at right
        else:
            add "models/emotions/shinEmotes/shinSmile3.png" at right
    if shinExpression == 28:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSmile4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSmile4.png" at right
        else:
            add "models/emotions/shinEmotes/shinSmile4.png" at right
    if shinExpression == 29:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinStern1.png" at right
        else:
            add "models/emotions/shinEmotes/shinStern1.png" at right
    if shinExpression == 30:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSurprised1.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSurprised1.png" at right
        else:
            add "models/emotions/shinEmotes/shinSurprised1.png" at right
    if shinExpression == 31:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinSurprised2.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinSurprised2.png" at right
        else:
            add "models/emotions/shinEmotes/shinSurprised2.png" at right
    if shinExpression == 32:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinUnsure1.png" at right
        else:
            add "models/emotions/shinEmotes/shinUnsure1.png" at right
    if shinExpression == 33:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinUnsure2.png" at right
        else:
            add "models/emotions/shinEmotes/shinUnsure2.png" at right
    if shinExpression == 34:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinUnsure3.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinUnsure3.png" at right
        else:
            add "models/emotions/shinEmotes/shinUnsure3.png" at right
    if shinExpression == 35:
        if samActive == True:
            if shinSkin == 0:
                add "models/emotions/shinEmotes/sam/shinUnsure4.png" at right
            if shinSkin == 2:
                add "models/emotions/shinEmotes/sam/sith/shinUnsure4.png" at right
        else:
            add "models/emotions/shinEmotes/shinUnsure4.png" at right
        
    if shinBlush == 1:
        if samActive == True:
            add "models/emotions/shinEmotes/sam/shinBlush.png" at right #Blush
        else:
            add "models/emotions/shinEmotes/shinBlush.png" at right #Blush
        
    if shinTears == 1:
        add "models/emotions/shinEmotes/shinTears.png" at right #Tears
        
    # Accessories
    if accessoriesShin == 1:
        if samActive == True:
            add "models/accessories/sam/shinHeadband.png" at right
        else:
            add "models/accessories/shinHeadband.png" at right
    if accessoriesShin == 2:
        if samActive == True:
            add "models/accessories/sam/shinHalloweenMask.png" at right
        else:
            add "models/accessories/shinHalloweenMask.png" at right
    if accessoriesShin == 3:
        if samActive == True:
            add "models/accessories/sam/shinBlindfold.png" at right
        else:
            add "models/accessories/shinBlindfold.png" at right
    if accessoriesShin == 15:
        if samActive == True:
            add "models/accessories/sam/shinXmasHat.png" at right
        else:
            add "models/accessories/shinXmasHat.png" at right
        
    #Accessories2
    if accessories2Shin == 5:
        add "models/accessories/sam/shinGag.png" at right
    if accessories2Shin == 6:
        if shinOutfit == 1:
            add "models/accessories/sam/shinNeckGagClothes.png" at right
        else:
            add "models/accessories/sam/shinNeckGag.png" at right
        
    if shinMask == 1:
        if samActive == True:
            add "models/accessories/sam/shinMask.png" at right
        else:
            add "models/accessories/shinMask.png" at right
        
#################################### Kit Body / clothing / emotions #################################################

#define kitUnderwear = 0
define kitOutfit = 1       
define kitOutfitSet = 0
define kitExpression = 1
define kitSkin = 0
define accessoriesKit = 0
define accessories2Kit = 0

screen kit_main:
    if kitSkin == 0:
        if samActive == True:
            add "models/skin/sam/kitNude.png" at right
        else:
            add "models/skin/kitNude.png" at right
    if kitSkin == 1:
        add "models/skin/kitLocked.png" at right

    if kitOutfit == 1:
        if samActive == True:
            add "models/outfits/sam/kitBasic.png" at right
        else:
            add "models/outfits/kitBasic.png" at right
    if kitOutfit == 2:
        add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 3:
        if samActive == True:
            add "models/outfits/sam/kitBasic3.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 4:
        if samActive == True:
            add "models/outfits/sam/kitDress.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 5:
        if samActive == True:
            add "models/outfits/sam/kitBasic4.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 6:
        if samActive == True:
            add "models/outfits/sam/kitBikini.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 7:
        if samActive == True:
            add "models/outfits/sam/kitTrooper.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 8:
        if samActive == True:
            add "models/outfits/sam/kitSlave.png" at right
        else:
            add "models/outfits/kitPlaceholder.png" at right
    if kitOutfit == 15:
        if samActive == True:
            add "models/outfits/sam/christmasKitOutfit.png" at right
        else:
            add "models/outfits/christmasKitOutfit.png" at right # Christmas outfit
    if kitOutfit == 16:
        if samActive == True:
            add "models/outfits/sam/halloweenKitOutfit.png" at right
        else:
            add "models/outfits/halloweenKitOutfit.png" at right # Halloween outfit
    if kitOutfit == 17:
       add "models/outfits/kitPlaceholder.png" at right 
        
    if kitExpression == 0:
        add "models/emotions/none.png" at right #None
    if kitExpression == 1:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/smile1.png" at right #Smile
        else:
                add "models/emotions/kitEmotes/smile1.png" at right #Smile
    if kitExpression == 2:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/closed1.png" at right #Closed
        else:
                add "models/emotions/kitEmotes/smile1.png" at right #Closed
    if kitExpression == 3:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/excited1.png" at right #Excited
        else:
                add "models/emotions/kitEmotes/smile1.png" at right #Excited
    if kitExpression == 4:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/grump1.png" at right #Grump
        else:
                add "models/emotions/kitEmotes/smile1.png" at right #Grump
    if kitExpression == 4:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/naughty1.png" at right #Naughty
        else:
                add "models/emotions/kitEmotes/smile1.png" at right #Naughty
        
    # Accessories
    if accessoriesKit == 1:
        if samActive == True:
            add "models/accessories/sam/halloweenKitHat.png" at right   #Halloween witch hat
        else:
            add "models/accessories/halloweenKitHat.png" at right   #Halloween witch hat
    if accessoriesKit == 3:
        if samActive == True:
            add "models/accessories/sam/kitBlindfold.png" at right 
        else:
            add "models/accessories/kitBlindfold.png" at right 
    if accessoriesKit == 15:
        if samActive == True:
            add "models/accessories/sam/kitXmasHat.png" at right   #Christmas hat
        else:
            add "models/accessories/kitXmasHat.png" at right   #Christmas hat
        
    #Accessories2
    if accessories2Kit == 4:
        add "models/accessories/sam/kitTail.png" at right
    if accessories2Kit == 5:
        add "models/accessories/sam/kitGag.png" at right
        
    ######## KIT MIDDLE HOLOGRAM ########
        
screen kit_main2:
    add "models/skin/sam/kitNude2.png" at center  
    add "models/emotions/kitEmotes/sam/smileHologram.png" at center

    if kitOutfit == 1:
        add "models/outfits/sam/kitBasic2.png" at center
    if kitOutfit == 2:
        add "models/outfits/sam/kitBasic2.png" at center
    if kitOutfit == 3:
        add "models/outfits/sam/kitBasic2Underwear1.png" at center
    if kitOutfit == 4:
        add "models/outfits/sam/kitBasic2Underwear2.png" at center
        
    if accessoriesKit == 1:
        add "models/accessories/kitHat.png" at right
        
        
    ######## KIT MIDDLE NORMAL ########
        
screen kit_main3:
    if kitSkin == 0:
        if samActive == True:
            add "models/skin/sam/kitNude.png"  at center 
        else:
            add "models/skin/kitNude.png"  at center 
        
    if kitOutfit == 1:
        if samActive == True:
            add "models/outfits/sam/kitBasic.png"  at center 
        else:
            add "models/outfits/kitBasic.png"  at center 
    if kitOutfit == 3:
        if samActive == True:
            add "models/outfits/sam/kitBasic3.png"  at center 
        else:
            add "models/outfits/kitBasic.png"  at center 
    if kitOutfit == 4:
        if samActive == True:
            add "models/outfits/sam/kitDress.png" at center
        else:
            add "models/outfits/kitPlaceholder.png" at center
    if kitOutfit == 5:
        if samActive == True:
            add "models/outfits/sam/kitBasic4.png" at center
        else:
            add "models/outfits/kitPlaceholder.png" at center
    if kitOutfit == 6:
        if samActive == True:
            add "models/outfits/sam/kitBikini.png" at center
        else:
            add "models/outfits/kitPlaceholder.png" at center
    if kitOutfit == 7:
        if samActive == True:
            add "models/outfits/sam/kitTrooper.png" at center
        else:
            add "models/outfits/shinBasic.png" at center
    if kitOutfit == 8:
        if samActive == True:
            add "models/outfits/sam/kitSlave.png" at center
        else:
            add "models/outfits/shinBasic.png" at center
    if kitOutfit == 15:
        if samActive == True:
            add "models/outfits/sam/christmasKitOutfit.png"  at center 
        else:
            add "models/outfits/christmasKitOutfit.png"  at center 
    if kitOutfit == 16:
        if samActive == True:
            add "models/outfits/sam/halloweenKitOutfit.png"  at center
        else:
            add "models/outfits/halloweenKitOutfit.png"  at center 
        
    if kitExpression == 0:
        add "models/emotions/none.png"  at center 
    if kitExpression == 1:
        if samActive == True:
            add "models/emotions/kitEmotes/sam/smile1.png"  at center 
        else:
            add "models/emotions/kitEmotes/smile1.png"  at center 
        
    # Accessories
    if accessoriesKit == 1:
        add "models/accessories/halloweenKitHat.png"  at center 
    if accessoriesKit == 3:
        if samActive == True:
            add "models/accessories/sam/kitBlindfold.png" at center 
    if accessoriesKit == 15:
        add "models/accessories/kitXmasHat.png"  at center 
        
    #Accessories2
    if accessories2Kit == 4:
        add "models/accessories/sam/kitTail.png" at center
    if accessories2Kit == 5:
        add "models/accessories/sam/kitGag.png" at center
        

        
#################################### Marieke Body / clothing / emotions #################################################

#define mariekeUnderwear = 0
define mariekeOutfit = 1       
define mariekeOutfitSet = 0
define mariekeExpression = 1
define mariekeSkin = 0
define accessoriesKit = 0
define accessories2Kit = 0

screen marieke_main:
    if mariekeSkin == 0:
        add "models/skin/mariekeNude.png" at right

    if mariekeOutfit == 1:
        add "models/outfits/mariekeBasic.png" at right
        
    if mariekeExpression == 1:
        add "models/emotions/mariekeEmotes/smile1.png" at right #Smile
        
    # Accessories
        

#################################### Player Body / emotions #################################################

define playerExpression = 0
define playerOutfit = 0

screen player_main: #Ahsoka ancor
    #Body
    if playerOutfit == 0:
        add "UI/models/masterModelPlayer2.png" at left
    if playerOutfit == 1:
        add "UI/models/masterModelPlayer.png" at left
        
    #Facial Expressions
    if playerExpression == 0:
        add "models/emotions/playerNeutral.png" at right #angry1
    if playerExpression == 1:
        add "models/emotions/playerAngry.png" at right #angry2
    if playerExpression == 2:
        add "models/emotions/playerHappy.png" at right #angry3



#################################### JASON Body / clothing / emotions #################################################

define jasonBody = 0
define jasonGuns = 0

screen jason_main:
    if samActive == True:
        if jasonBody == 0:
            add "models/skin/jasonbasicSam.png" at right
        if jasonGuns == 1:
            add "models/mods/jasonGunsSam.png" at right
    else:
        if jasonBody == 0:
            add "models/skin/jasonbasic.png" at right
        if jasonGuns == 1:
            add "models/mods/jasonGuns.png" at right
            
screen jason_main2:
    add "models/skin/jasonbasicSam.png" at center
    if jasonGuns == 1:
        add "models/mods/jasonGunsSam.png" at center
    if jasonGuns == 2:
        add "models/mods/jasonGunsSam2.png" at center

# Declare characters used by this game.
define a = Character('Ahsoka', color="#ffd953")

define y = Character('You', color="#ffd953",  window_background="UI/dialogueboxPlayer.png",
    window_left_padding=170,
    show_side_image=Image("models/masterModelPlayer.png", xpos=5, ypos=455)
    )

define yFlashback = Character('You', color="#ffd953",  window_background="UI/dialogueboxPlayerFlashback.png",
    window_left_padding=170,
    show_side_image=Image("models/masterModelPlayerFlashback.png", xpos=5, ypos=455)
    )

define flashbackBox = Character(window_background="UI/dialogueboxFlashback.png")

define jasonFlashback = Character('Mr. Jason', color="#ffd953",  window_background="UI/dialogueboxFlashback.png")

define yTut = Character('You', color="#ffd953",  window_background="UI/dialogueboxPlayer.png",
    window_left_padding=170,
    show_side_image=Image("models/masterModelPlayer2.png", xpos=5, ypos=455)
    )

#################################### Droid army endgame #################################################

screen droidArmy:
    add "models/skin/droidArmy1.png"
    
screen jasonVisor:
    add "models/skin/jasonVisor.png" at center

#################################### ENDGAME SCREEN #################################################
    
define endGameSpirit = 0
define endGameDroids = 0
define endGamePlayer = 0
define endGameAhsoka = 0
define endGameShin = 0
define endGameKit = 0
define endGameShots = 0
define endGameScreenEffect = 0
define endGameDestruction = 0
define endGamePlayerSitting = 0
define endGameAhsokaSitting = 0

screen endGame:
    if endGameSpirit == 1:
        add "bgs/endGameSpirit.png"
        
    if endGameDroids == 1:
        add "bgs/endGameDroids.png"
        
    if endGamePlayer == 1:
        add "bgs/endGamePlayer.png"
        
    if endGameAhsoka == 1:
        add "bgs/endGameAhsoka.png"
        
    if endGameShin == 1:
        add "bgs/endGameShin.png"

    if endGameKit == 1:
        add "bgs/endGameKit.png"
        
    if endGameShots == 1:
        add "bgs/endGameShots1.png"
    if endGameShots == 2:
        add "bgs/endGameShots2.png"
    if endGameShots == 3:
        add "bgs/endGameShots3.png"
        
    if endGameDestruction == 1:
        add "bgs/endGameDestruction.png"
        
    if endGameAhsokaSitting == 1:
        add "bgs/endGameAhsokaSitting.png"
        
    if endGamePlayerSitting == 1:
        add "bgs/endGamePlayerSitting.png"
        
    if endGameScreenEffect == 1:
        add "bgs/endGameScreenEffect.png"
            
#################################### EXTRA CHARACTERS #################################################

screen queen_main:
    add "models/skin/queen.png" at left
screen nehmek_main:
    add "models/skin/nehmekMain.png" at right
screen droid_main:
    add "models/skin/DroidMain.png" at right
screen devdroid_main:
    add "models/skin/devBot.png" at right
screen hondo_main:
    add "models/skin/hondoMain.png" at right
screen tailor:
    add "models/skin/tailor.png" at right
    
screen battle_droid1:
    add "models/skin/battleDroid1.png" at left
screen battle_droid2:
    add "models/skin/battleDroid2.png" at left
screen battle_droid3:
    add "models/skin/battleDroid3.png" at left
screen spirit:
    add "models/skin/sam/spirit.png" at center

#################################### STARSHIP #################################################
define shipStatus = 0

screen playerShip:
    if shipStatus == 0:
        add "UI/shipStatus0.png" at right
    if shipStatus == 1:
        add "UI/shipStatus1.png" at right
    if shipStatus == 2:
        add "UI/shipStatus2.png" at right
    if shipStatus == 3:
        add "UI/shipStatus3.png" at right
    if shipStatus == 4:
        add "UI/shipStatus4.png" at right
    if shipStatus == 5:
        add "UI/shipStatus5.png" at right
    if shipStatus == 6:
        add "UI/shipStatus6.png" at right
        
#################################### DROID EVENT #################################################
define droidActive = 0

screen droidEvent:
    add "UI/droids.png"
    if droidActive == 1:
        add "UI/droidHeart1.png"
    if droidActive == 2:
        add "UI/droidHeart2.png"
    if droidActive == 3:
        add "UI/droidHeart3.png"
    if droidActive == 4:
        add "UI/droidHeart4.png"
    if droidActive == 5:
        add "UI/droidHeart5.png"
    if droidActive == 6:
        add "UI/droidHeart1.png"
        add "UI/droidHeart2.png"
        add "UI/droidHeart3.png"
        add "UI/droidHeart4.png"
        add "UI/droidHeart5.png"
        
########################################## OUTPOST ################################################
screen outpost1:
    add "UI/outpostS1.png"
        
screen outpost2:
    add "UI/outpostS2.png"

#################################### Station Hologram #################################################       
screen stationHologram:
    add "bgs/stationHologram.png"
    
        
######################################### CELL ITEMS #################################################
        
screen cell_items:
    if republicPosters == True:
        add "bgs/cellItems/bgCellPosters.png"
    if magazine == True:
        add "bgs/cellItems/bgCellMag.png"
    if wallMountedDildo == True:
        add "bgs/cellItems/backgroundCellDildo.png"
    if dancingPole == True:
        add "bgs/cellItems/bgCellPole.png"
    if cuffs == True:
        add "bgs/cellItems/bgCellCuffs.png"
    if targetingDroid == True:
        add "bgs/cellItems/bgCellDroid.png"
    if convorPet == True:
        add "bgs/cellItems/bgCellBird.png"


##################################################################################################################

define s = Character("Shin'na", color="#ffd953")
define k = Character("Kit", color="#ffd953")
define sky = Character("Anakin Skywalker", color="#ffd953")
define mr = Character("Mr. Jason", color="#ffd953")
define tutJas = Character("J4-S-0N", color="#ffd953")
define man = Character("Mandora", color="#ffd953")
define j = Character("Lord Jomba", color="#ffd953")
define h = Character("Hondo", color="#ffd953")
define mar = Character("Marieke", color="#ffd953")

define asb = Character("Asbulra", color="#ffd953")

#Employers & vendors
define queen = Character("Stomach Queen", color="#ffd953")
define nem = Character("Nemthak Viraal", color="#ffd953")
define lok = Character("Lok", color="#ffd953")
define santa = Character("{color=#e01a1a}Santa{/color} {color=#22b613} Claus{/color}", color="#ffd953")
define gi = Character("Girl", color="#ffd953")

# declare misc. bodies
image maleDemo = "UI/models/maleDemo.png"

# misc.
define day = 0
define timeOfDay = 0
define hypermatter = 30                     #Currency
define shotAtJason = False
define dailySpy = 0
define newMessages = 1
define lightsaberReturned = False
#define ahsokaStartTraining = False
define tutorialActive = True
define productionBonus = 0
define rakghoulHuntActive = False
define republicWinning = 100             # How close the Republic is to winning to war (used for: screen ForgeMapBridge in StarForgeRooms.rpy )
define jobReport = 0                            # define what job Ahsoka's been on
define suppressorsDisabled = False
define prostituteStorage = 0               # Amount the working girls collect. Can be collected at the nightclub.
define CISwarning = False
define manual3Unlock = False
define republicVictories = 0                    # Amount of times Republic fought the CIS and won.
define orgyLockout = 0                          # after hosting an orgy, there will be a few day lockout.

# Station battle
define underAttack = False                      # Station gets attacked
define underAttackClearCells = False     # Clear Cell
define underAttackClearMed = False      # Clear medbay
define underAttackClearForge = False   # Clear forge
define underAttackClearExplore = False # final step

define mission1PlaceholderText = ""
define mission2PlaceholderText = ""
define mission3PlaceholderText = ""
define mission4PlaceholderText = ""
define mission5PlaceholderText = ""
define mission6PlaceholderText = ""
define mission7PlaceholderText = ""
define mission8PlaceholderText = ""
define mission9PlaceholderText = ""


# War efforts
define fleet = 1
define missionInProgress = False
define fightersAvailable = fleet
define missionSuccessChance = 0

#Define holidays - keywords: EVENTS HOLIDAYS HALLOWEEN CHRISTMAS
define halloween = False                         
define christmas = False
define christmasIntroduction = False


#Player Stats
define repairSkill = 0 # makes repairs cheaper. Earned by reading books, Ahsoka training and exploration
define gunslinger = 0
define playerHitPoints = 3      # Max hp is 3
define lightSidePoints = 10     # 10 is neutral. Above that is Light side.
define influence = 0    # A currency that's build up over time. Can be used to shorten quests, make special requests and get exclusive items.
define influenceIncome = 0  # How much extra influence you build each day. Amount is depending on your upgrades and population.
define smugglesAvailable = 0 # When upgrading the station you can start housing smugglers. These will count as a boost to the influenceIncome.

#Shin'na Stats
define shinActive = False
define shinSocial = 0
define shinIgnore = 0         #0 Shin available, 1 shin unavailable


#Shin'na Stats
define kitActive = False
define kitSocial = 0
define kitIgnore = 0         #This prevents Kit from talking when not available.


#Ahsoka Stats
define trainingActive = False   #Turned True in Quests.rpy Mission 4.
define ahsokaSlut = 0
define theLie = 0           # Lie about letting Ahsoka go free at the end of the game.
define ahsokaSocial = 0        #Social Points. High social will give a different ending
define mood = 75                  #Max mood 100
define ahsokaPower = 10         #Ahsoka max health.
define ahsokaHealth = 3         #Ahsoka current health.
define virgin = True
define analVirgin = True
define addictWine = 0           #Ahsoka becomes addicted to wine at 10.
define addictDrug = 0           #Ahsoka becomes addicted to Death Sticks at 5.
define ahsokaIgnore = 0         #This prevents Ahsoka from talking when not available. 0 is available, 1 is ignore, 2 is not at the station, 3 busy practising, 4 is mood too low.
define ahsokaFirstTime = True   # Ahsoka never has had sex before

#Ahsoka nicknames
define ahsokaName = "girl"
define breastsNickname = "breasts"
define playerName = "Lowlife"
define color = "orange"

#Ahsoka Jobs

#defines which  she went on and what debriefing she'll give you in the evening.
define Report = 0

define fastFoodJob = False
define timesWorkedSnackbar = 0

define entertainerJob = False
define timesWorkedPalace = 0

define pitGirlJob = False
define timesWorkedRacer = 0

define escortJob = False
define slaveJob = False

#Ahsoka Explore
define christophsisExplore = False
define telosExplore = False
define geonosisExplore = False

#Boss Active
define nehmtakActive = False

#Boss Social
define socialStomachQueen = 0

screen modelScreen:
    add "UI/modelScreen.jpg"
    
label samActive:
    show screen modelScreen
    with d3
    if samActive == True:
        "Toggle ingame models to alpha? This artstyle is no longer being supported."
        menu:
            "Yes":
                $ samActive = False
            "Never mind":
                pass
        hide screen modelScreen
        jump status
    if samActive == False:
        "Toggle ingame models to beta? This artstyle is still being supported and updated."
        menu:
            "Yes":
                $ samActive = True
            "Never mind":
                pass
        hide screen modelScreen
        jump status
        

# The game starts here.
label start:
    jump intro

return
