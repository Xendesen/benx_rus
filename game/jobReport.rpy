
label jobReport:
    $ hair = 0
    stop music fadeout 3.0
    hide screen warMeter
    with d3
    scene black with fade
    pause 1.0
    play music "audio/music/night.mp3"
    scene bgBedroom with fade
 
### SMUGGLE CALCULATE  ###
    if smuggleActive == True:
        $ smuggleActive = False
        "You return from your smuggling run."
        if 1 <= randomSmuggle <= 49: #Mission Failed
            "Unfortunately you were discovered and had to dump your cargo. You were not paid."
            pause 0.5
        if 50 <= randomSmuggle <= 100: #Mission Succes
            if day <= 30:
                $ randomSmuggleReward = renpy.random.randint(25, 50)
            if 31 <= day <= 60:
                $ randomSmuggleReward = renpy.random.randint(35, 70)
            if day >= 61:
                $ randomSmuggleReward = renpy.random.randint(50, 100)
            "It was a great success!"
            if storageActive == False:
                "You earned [randomSmuggleReward] Hypermatter."
                $ hypermatter += randomSmuggleReward
            if storageActive == True:
                "Do you wish to deliver your smuggled goods and collect your reward, or store them and wait for a better opportunity?"
                menu:
                    "Store them away":
                        $ storageContainer += randomSmuggleReward
                        "You decided to store the smuggled goods away for now."
                    "Deliver them (+Influence)":
                        $ influence += 10
                        $ hypermatter += randomSmuggleReward
                        "You decided to deliver the smuggled goods. You were paid [randomSmuggleReward] Hypermatter."
            pause 0.5
    else:
        pass
    
    # Lightsaber missions
    if 1 <= LSMission <= 4 and LSMissionJobReport >= 1:
        jump LSMissionJobReport
    else:
        pass
        
    # Dayjobs
    if jobReport == 0:
        $ jobReport = 0
        jump room
    if jobReport == 1:
        $ jobReport = 0 
        jump debriefCoruscant #Fastfood
    if jobReport == 2:
        $ jobReport = 0 
        jump debriefZygerria #Entertainer
    if jobReport == 3:
        $ jobReport = 0 
        jump debriefTatooine #Pit Girl
    if jobReport == 4:
        $ jobReport = 0
        jump debriefNaboo #Escort
    if jobReport == 5:
        $ jobReport = 0
        jump debriefMandalore #slave caretaker
    if jobReport == 6:
        $ jobReport = 0
        jump debriefKoltoHeal #Kolto Tank Heal
    if jobReport == 7:
        $ jobReport = 0
        jump debriefKoltoAddict #Kolto Tank Addict
    else:
        pass
    
    
    
################################################################################################
####################################### REPORT CORUSCANT #####################################
################################################################################################
            
label debriefCoruscant:
    if ahsokaSlut <= 10:
        $ outfit = 5
    if 11 <= ahsokaSlut <= 20:
        $ outfit = 6
    if ahsokaSlut >= 21:
        $ outfit = 7
    if 0 <= ahsokaSlut <= 5:
        $ ahsokaExpression = 12
    if 6 <= ahsokaSlut <= 11:
        $ ahsokaExpression = 12
    if ahsokaSlut >= 12:
        $ ahsokaExpression = 9
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    a "I'm back from working at the Lekka Lekku."
    $ timesWorkedSnackbar += 1
    $ ahsokaIgnore = 0
    if ahsokaSlut == 0:
        $ hypermatterRandom = renpy.random.randint(12, 17)
    if 1 <= ahsokaSlut <= 10:
        $ hypermatterRandom = renpy.random.randint(17, 23)
    if 11 <= ahsokaSlut <= 19:
        $ hypermatterRandom = renpy.random.randint(25, 27)
    if ahsokaSlut >= 20:
        $ hypermatterRandom = renpy.random.randint(29, 31)
    if 230 <= republicWinning <= 260:
        $ hypermatterRandom += 10
    if republicWinning >= 261:
        $ hypermatterRandom += 15
    #### Victory conditions    
    if republicVictories == -1:
        $ hypermatterRandom -= 10
    if republicVictories == -2:
        $ hypermatterRandom -= 20
    if republicVictories <= -3:
        $ hypermatterRandom -= 30
    if republicVictories == 1:
        $ hypermatterRandom += 10
    if republicVictories == 2:
        $ hypermatterRandom += 20
    if republicVictories == 3:
        $ hypermatterRandom += 30
    if republicVictories == 4:
        $ hypermatterRandom += 40
    if republicVictories >= 5:
        $ hypermatterRandom += 50
    $ hypermatter += hypermatterRandom
    a "I was paid [hypermatterRandom] Hypermatter."
        
    if timesWorkedSnackbar == 1:
        y "Welcome back, Padawan! How did it go?"
        $ ahsokaExpression = 12
        a "Well.... the work wasn't difficult, but the customers were ferocius. Make one small slip up and they all yell at you."
        y "Welcome to the service industry."
        $ ahsokaExpression = 10
        a "That's not what put me off the most though."
        a "A lot of the customers would leer and whistle at me as I walked by and the other waitresses acted like it was no big deal."
        y "Yeah well... the Stomach Queen's restaurants aren't exactly known for their class."
        y "Just ignore them. We'll keep going the way we are now and move on to another job if I find something for you."
        $ ahsokaExpression = 9
        a "I brought you back the Hypermatter you wanted. Will you send ships to help the Republic now?"
        y "I'm sorry to say this, but we're going to need a 'lot' more than [hypermatterRandom] Hypermatter before this station is operational again."
        $ ahsokaExpression = 14
        a "How much will we need?"
        y "I don't know. Like... a thousand times that?"
        $ ahsokaExpression = 11
        a "What?! But this will take forever!"
        y "Relax, I'm sure I'll think of something."
        $ ahsokaExpression = 10
        a "............"
        $ ahsokaExpression = 1
        a "You better not try to trick me, [playerName]."
        y "Don't worry, as soon as we've fixed up this station, we'll use it to help your Republic."
        $ ahsokaExpression = 10
        a "If there's still a Republic left to help...."
        a "If you need me, I'll be in my room."
        y "You mean cell."
        $ ahsokaExpression = 12
        a "Whatever."
        "Ahsoka leaves for her ro-...{w} cell."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        scene bgBedroom
        jump room
    
    if timesWorkedSnackbar == 2:
        a ".............."
        y "Anything happen at work today?"
        $ ahsokaExpression = 20
        a "It was all right. At first I had trouble keeping up with all the different orders people were placing, but with the help of my coworkers I quickly got used to it."
        a "The manager said I was the quickest learner he'd ever seen, but....."
        y "But...?"
        $ ahsokaExpression = 10
        a "Well he and I almost got into an argument. He mentioned that if I flirted with the customers, they'd spend more money."
        y "That's a good point. He might actually pay us extra!"
        $ ahsokaExpression = 1
        a "I'm {i}'not'{/i} flirting with the customers."
        y "Why not? It may make our lives easier."
        $ ahsokaExpression = 2
        a "I'm a Jedi! We don't flirt!"
        y "Why not?"
        $ ahsokaExpression = 5
        a "We're taught to control our emotions. We have to suppress any romantic thoughts."
        a "Flirting is greatly frowned upon and is seen as something that distracts from our teachings."
        y "That's silly. What harm can it do?"
        $ ahsokaExpression = 2
        stop music fadeout 1
        a "!!!"
        with hpunch
        a "SILLY?!"
        y "Huh....?"
        play music "audio/music/tension1.mp3"
        a "The Jedi teachings are not 'silly'! They keep us focused and guide us on our path!"
        a "A Jedi walks a fine line between the Dark and Light side! If we fall, it may spell out doom for thousands of innocents!"
        $ ahsokaExpression = 1
        if ahsokaSocial <= 7:
            a "If we fall, we turn into what you've become! A Sith! A dark and twisted form of what was once pure and good!"
        else:
            a "If we fall, we turn into these monsterous beings... Sith! A dark and twisted form of what was once pure and good!"
        a "The Sith are murderous blood thirsty monsters that throughout the millenia have brought nothing but pain and oppression to millions!"
        a "I follow the teachings of the Jedi, because I never want to turn into that!"
        y "Woah~.... okay calm down. {w} What was that about controlling your emotions?"
        $ ahsokaExpression = 2
        a "I am calm!"
        $ ahsokaExpression = 10
        a "................"
        y "Fine, no flirting. It was just a suggestion."
        $ ahsokaExpression = 12
        a "Good....."
        if ahsokaSocial >= 16:
            $ ahsokaExpression = 4
            a "I don't care if you're a Sith or not, but don't insult the Jedi."
        if ahsokaSocial <= 15:    
            $ ahsokaExpression = 5
            if ahsokaSocial >= 8:
                "You could learn something from the teachings, you know? Maybe instead of smuggling you'd find a more wholesome way to live."
            if ahsokaSocial <= 7:
                a "You'll do well to not mock the Jedi teachings. If you'd have stuck with them you too would've stayed a proud Jedi rather than what you are now."
            y "Yeah well, I'm doing just fine thank you."
        y "Head to your room, calm down a bit."
        $ ahsokaExpression = 4
        "Ahsoka grumbles under her breath and leaves for her cell."
        hide screen ahsoka_main
        with d3
        stop music fadeout 1.5
        pause 0.5
        y ".............."
        y "Touchy subject."
        hide screen scene_darkening
        with d3
        play music "audio/music/night.mp3"
        scene bgBedroom
        jump room
        
    if timesWorkedSnackbar == 6:
        $ mission4 = 1
        $ ahsokaExpression = 1
        "Ahsoka looks furious!"
        y "Uh oh... what happened?"
        "She clenches her fists in anger."
        a "Later! I need to meditate first!"
        y "..................?"
        "Ahsoka storms off to her cell."
        hide screen ahsoka_main
        with d2
        pause 1.0
        "After a while you decide to follow her."
        $ ahsokaExpression = 17
        scene bgCell01 with fade
        show screen scene_darkening
        show screen ahsoka_main
        with d3
        a "................."
        y "Feeling better?"
        a "Hmph~....."
        y "So what happened?"
        $ ahsokaExpression = 13
        a "One of the customers went too far today!"
        $ ahsokaExpression = 12
        a "I bend over one of the tables while serving a customer and he just walked by and grabbed my....{w} my.... {size=- 8}my butt.{/size}"
        y "Oh well that's not so-...."
        $ ahsokaExpression = 2
        a "I turned around, put him in an arm lock and shoved him up against the wall, of course."
        y "!!!"
        a "What?{w} Just because I don't have my lightsaber with me, doesn't mean I can't fight."
        $ ahsokaExpression = 1
        a "Then the manager comes over and instead of taking my side, he scolds me and cut my pay in half for today!"
        $ ahsokaExpression = 29
        a "At this rate we'll never save the Republic...."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        "Ahsoka seems upset. Perhaps you should give her some time."
        $ mission4Title = "{color=#f6b60a}Mission: 'Orange Flirt' (Stage: I){/color}"
        play sound "audio/sfx/quest.mp3"
        "{color=#f6b60a}Mission: 'Orange Flirt' (Stage: I){/color}"
        scene bgBedroom with longFade
        jump room
        
    define firstSlutWork = True
    if ahsokaSlut == 1 and firstSlutWork == True:
        $ firstSlutWork = False
        $ hypermatter += 10
        y "I'm all ears."
        $ ahsokaExpression = 22
        a "Huh?"
        $ ahsokaExpression = 10
        a "Oh right... the flirting thing."
        a "........................."
        $ ahsokaExpression = 12
        a "Turns out it worked...."
        y "That's great news!"
        $ ahsokaExpression = 2
        a "No it's not!"
        a "I mean... It is, but...!"
        $ ahsokaExpression = 10
        a "......................"
        y "All right, calm down. Tell me about your day."
        $ ahsokaExpression = 4
        a "{b}*Sighs*{/b} All right."
        a "So I tried being more flirty today."
        $ ahsokaExpression = 12
        a "It... didn't go well exactly. Erm..."
        a "It was awkward, but one of the patrons saw how hard I was trying, so he decided to give me a tip regardless."
        a "After that, more patrons called me over to order or simply to talk to me."
        a "................................."
        y "Go on...?"
        $ ahsokaExpression = 10
        a "Well.... I...~"
        a "......................."
        a "{size=-4}One of the patrons pinched my bum again today.{/size}"
        a "But I didn't get angry at him...."
        a "He... ordered big time and even left me a hundred credit tip."
        y "Your manager must've been happy with that!"
        $ ahsokaExpression = 4
        a "Yeah, I guess he was. He even encouraged me to keep doing what I was doing."
        a "I guess flirting a little bit does pay off. The manager even gave me this bonus."
        play sound "audio/sfx/itemGot.mp3"
        "Ahsoka hands you an extra {color=#FFE653}10 hypermatter.{/color}"
        y "See? It's working! We should continue your training!"
        $ ahsokaExpression = 11
        a "Continue? Haven't I learned enough yet?"
        y "You took one lesson and got flustered as soon as the manual mentioned groping."
        $ ahsokaExpression = 12
        a "Yeah, but! I mean... I won't need to learn anything other than flirting, right?"
        a "Groping seems to be taking it a little far and-...."
        y "Someone paid you a hundred credit tip for pinching your butt."
        y "Image how much you'll be paid if you keep that up."
        $ ahsokaExpression = 4
        a "..........................."
        a "Well.... it wasn't bad as I thought it'd be."
        a "And I {i}'am'{/i} helping the Republic."
        ".............................."
        $ ahsokaExpression = 17
        a "Fine... we can continue the training. I've survived much worse, I'm sure I can handle a bit of manhandling."
        y "That's the spirit. Go get some rest now."
        "Ahsoka nods and leaves for her cell."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        jump room
    else:
        pass
    if timesWorkedSnackbar >= 11 and mission5 == 0:
        $ mission5 = 1
        jump mission5
        
    else:
        menu:
            "Tell me about your day":
                if ahsokaSlut == 0:     # Refuses any kind of flirtation on the job
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        $ ahsokaExpression = 9
                        a "It was all right. The people were a little rude, but the manager said I was a big help."
                        $ ahsokaExpression = 11
                        a "I don't know what they put in those burgers, but the customers loved them."
                        a "Some ordered twice or even three times in a row."
                        y "Good work, keep it up and we'll have enough Hypermatter in no time."
                        "Ahsoka leaves for her room."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        $ ahsokaExpression = 9
                        a "Nothing special happened, really."
                        a "My coworkers are all really friendly and someone complimented me on my uniform today."
                        y "What did they say?"
                        $ ahsokaExpression = 12
                        a "Heh, she said I looked cute in it. That's a little strange, isn't it?"
                        menu:
                            "You do look cute in it":
                                $ ahsokaExpression = 16
                                a "It sounds wrong when you say it..."
                            "Yes, that is strange":
                                $ ahsokaExpression = 7
                                a "Yeah, that's what I thought."
                        $ ahsokaExpression = 5
                        a "Anyways, I even got a tip from one of the customers, but I had to hand it over to the manager."
                        $ ahsokaExpression = 12
                        a "It's not like we need credits, but still. I felt a bit cheated."
                        y "Alright good work [ahsokaName]. You may return to your room."
                        "Ahsoka leaves for her room."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 9
                        a "It was a busy day. People were lining up outside of the restaurant. It was insane."
                        a "We sold out half way through the day and the manager told us to go home early."
                        y "I hope you enjoyed your free time."
                        "Ahsoka leaves for her room."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 4:
                        $ ahsokaExpression = 12
                        a "What's there to say? It's a weird job."
                        a "There's pretty much only male customers, the staff is all female except for the manager and customers keep spilling their drinks on me by mistake."
                        y "Trust me, they're not doing that by mistake."
                        $ ahsokaExpression = 16
                        a "....?"
                        $ ahsokaExpression = 10
                        a "Anyways I have to go wash these clothes now.... 'again'."
                        "Ahsoka leaves to clean herself up."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                        
                if 1 <=  ahsokaSlut <= 5: # Hates being leered at
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        $ ahsokaExpression = 5
                        a "Well... I tried flirting with a customer today."
                        y "Really? How did it go?"
                        $ ahsokaExpression = 10
                        a "It was really awkward... for both of us."
                        $ ahsokaExpression = 9
                        a "It wasn't very busy, so he actually told me to sit down and we had a nice chat."
                        y "You had a friendly chat in a Lekka Lekku restaurant? {w}I guess you really do bring out the good in people."
                        a "{b}*Shrugs*{/b} It's certainly a lot easier than being flirty."
                        "You spend some time talking to Ahsoka before sending her back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        $ ahsokaExpression = 4
                        a "Ugh, do you have any idea how annoying it is to walk through a crowd while everyone whistles and catcalls you?"
                        y "But at least it's making you more Hypermatter, right?"
                        a "Well... yeah. A little more."
                        $ ahsokaExpression = 12
                        a "I'm actually suprised at how much more people are willing to pay when you just give them a little smile."
                        y "Or shake your hips a little."
                        $ ahsokaExpression = 4
                        a "..........."
                        y "You're gonna have to start doing it sooner or later."
                        "Ahsoka shrugs and you send her back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 10
                        a "Same as always. Customers whistle at you, spill their drinks on you and want you to come sit on their lap."
                        $ ahsokaExpression = 12
                        a "It's disgusting. I don't know how the other girls are doing it."
                        y "Probably because they're confident and are determined to get money?"
                        $ ahsokaExpression = 2
                        a "I am both of those things!"
                        y "No you're not."
                        $ ahsokaExpression = 5
                        a "I have a ton of confidence!"
                        y "I've seen you flirt."
                        y "You might be focussed and determined on the battlefield, but once you have to start flirting you don't know what to do with yourself."
                        $ ahsokaExpression = 1
                        a "Tsk! Don't act as if you know me [playerName]."
                        "You tell Ahsoka to head back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 4:
                        $ hypermatterRandom = renpy.random.randint(3, 5)
                        $ hypermatter += hypermatterRandom
                        $ ahsokaExpression = 10
                        a "It was annoying. A group of Arcona visited and kept staring at my chest and making crude jokes."
                        a "One of my coworkers took pity on me and decided to waiter their table instead, but they kept asking for me to come back!"
                        $ ahsokaExpression = 2
                        a "They must've spent at least a hundred credits on food and drinks. Half of which... of course 'accidentally' got spilled over my shirt."
                        $ ahsokaExpression = 4
                        play sound "audio/sfx/itemGot.mp3"
                        a "{b}*Groans*{/b} At least they were generous tippers. Here's the extra {color=#FFE653}[hypermatterRandom] Hypermatter{/color} I've made today."
                        y "See! If people like you they'll be willing to tip more!"
                        $ ahsokaExpression = 10
                        a "You mean if they like my body."
                        y "Is there a difference?"
                        "Ahsoka rolls her eyes and heads back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                        
                if 6 <=  ahsokaSlut <= 11: # Reluctantly accepts being groped and cat called
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        $ ahsokaExpression = 12
                        a "It went all right."
                        a "I guess I've sorta gotten used to being ogled at."
                        a "And... being groped I guess."
                        y "You're getting used to being groped?"
                        $ ahsokaExpression = 10
                        a "Don't get me wrong! I still hate that the customers can't keep their hands to themselves!"
                        $ ahsokaExpression = 12
                        a "But I've been working there for a while..."
                        a "I guess it's just part of the job."
                        "Ahsoka leaves for her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        label shirttest:
                            pass
                        hide screen ahsoka_main
                        with d2
                        $ outfit = 17
                        show screen ahsoka_main
                        with d2
                        y "!!!"
                        $ ahsokaExpression = 12
                        a "I guess it wasn't that special. Same old really."
                        y "Oh really?"
                        $ ahsokaExpression = 22
                        a "...?"
                        y "No, it's okay. Just stand there for a moment."
                        $ ahsokaExpression = 14
                        a "What are you up to, [playerName]?"
                        $ ahsokaExpression = 11
                        $ ahsokaBlush = 1
                        a "Oh!!!"
                        "Ahsoka quickly covers herself up."
                        y "Got more drinks spilled on you today?"
                        $ ahsokaExpression = 6
                        "Without another word, Ahsoka rushes off to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        $ ahsokaBlush = 0
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 4
                        a "They asked me to dance today...."
                        y "Oh, well that isn't so bad, is it?"
                        $ ahsokaExpression = 10
                        a "It wouldn't be, but they kept wanting me to take my shirt off."
                        $ ahsokaExpression = 2
                        a "As if I would take off my clothes in a sleazy place like that!"
                        y "So you're more of a high class stripper?"
                        $ ahsokaExpression = 1
                        a "Very funny...."
                        "Ahsoka heads off to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 4:
                        $ ahsokaExpression = 4
                        a "That place is so freaking gross."
                        y "I never saw you as such a girly-girl. Little bit of filth putting you off?"
                        a "[playerName]... I've spend my years on army bases, tankardships and on the front line."
                        a "I've seen filth, lived in filth and slept in filth."
                        a "But I have 'never' seen a place as filthy as the Lekku."
                        y "............................"
                        y "Okay... That's good to know for the next time I visit there."
                        "Ahsoka takes her leave back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                        
                if 12 <= ahsokaSlut <= 22: # Enjoys being cat called
                    $ randomWork = renpy.random.randint(1, 3)
                    if randomWork == 1:
                        $ ahsokaExpression = 6
                        a "Today went terrible!"
                        a "I kept dropping plates and I kept having to make up for it by sitting on customer's laps and feeding them."
                        $ ahsokaExpression = 12
                        a "Come to think of it... people may have been tripping me on purpose."
                        $ ahsokaExpression = 7
                        a "The customers seemed happy however, so my manager didn't mind."
                        "Ahsoka heads back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        $ ahsokaExpression = 9
                        a "It went all right actually. Some regular customers showed up and asked me to dance."
                        $ ahsokaExpression = 7
                        a "I was a little hesitant at first, but they come by so often, I wanted to do something back for them."
                        $ ahsokaExpression = 9
                        a "So the other girls joined in as well! It was fun for a while!"
                        y "For a while?"
                        $ ahsokaExpression = 12
                        a "Well... one of the girls got a little too excited and took her top off."
                        a "The other girls all looked at each other, wondering if they should do the same."
                        a "But the manager broke things up. Something about getting in trouble if we did."
                        y "Coruscant has very specific rules about stripping in diners. All right, head back to your room."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 9
                        a "I had a good time at work today!"
                        a "A group of regular customers showed up and they asked specifically for me!"
                        $ ahsokaExpression = 7
                        a "I mean sure... they just wanted to grope me, and make me feed them, but still, I felt flattered."
                        a "More and more customers ask for me to waiter them and..."
                        y "And...?"
                        $ ahsokaExpression = 23
                        a "Heh heh~... and I don't mind being felt up a bit. Really makes me feel wanted."
                        a "Also the manager sure enjoys having me around. He's considering naming a burger after me!"
                        a "The Orange Burger."
                        $ ahsokaExpression = 20
                        a "............................"
                        $ ahsokaExpression = 12
                        a "Don't order it. I've seen the thing it's made out of. Just a fair warning."
                        y "Don't order the Orange Burder. Got it."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        jump room

                        
                if ahsokaSlut >= 23: # Loves working at the Lekku
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        $ hypermatterRandom = renpy.random.randint(8, 15)
                        $ hypermatter += hypermatterRandom
                        $ ahsokaExpression = 9
                        a "With a butt like mine? It was great!"
                        y "Woah! You look excited."
                        a "Yup! Got a big customer who tipped like no tomorrow!"
                        $ ahsokaExpression = 7
                        a "I mean... of course we had to sit on his lap and our rears got slapped whenever we walked by."
                        $ ahsokaExpression = 9
                        a "But he gave such good tips! I was his favorite waitress so the manager gave me an extra [hypermatterRandom] hypermatter."
                        y "By the sounds of it, it sounds like you started enjoying working at the Lekku."
                        $ ahsokaExpression = 12
                        a "Well~..."
                        $ ahsokaExpression = 9
                        a "I'm not losing track of my original mission of course, but I could think of worst ways of collecting Hypermatter."
                        "You and Ahsoka talk for a bit before she heads back to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        $ ahsokaExpression = 20
                        a "........................"
                        y "What...?"
                        $ ahsokaExpression = 7
                        a "Can you keep a secret...?"
                        y "Oh this is gonna be good."
                        a "So you know there are rules against stripping in public places that aren't clubs, right?"
                        y "On Coruscant. Sure."
                        $ ahsokaExpression = 8
                        a "Well...~"
                        a "We convinced the manager to serve the customers in the nude today~...."
                        y "Damn! And I missed it? I miss all the good stuff being locked up on this station!"
                        $ ahsokaExpression = 23
                        a "{b}*Giggles*{/b} It was fun!"
                        a "Making your way through a busy restaurant completely naked. Having all the patrons look at you and grope you~.... ♥"
                        a "Maybe if we're lucky we can do it again in the future."
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 5
                        a "Some customers can be really grabby!"
                        a "...................."
                        $ ahsokaExpression = 23
                        a "I mean... I don't mind of course. {w}I actually kind of like it."
                        $ ahsokaExpression = 12
                        a "But it does mean I don't get to do my job, and I feel a bit bad about it."
                        a "The manager assured me that I was doing just fine, but people still had to wait a long time for their food."
                        y "Trust me. They don't go to the Lekka Lekku for the food."
                        y "They go there for the girls and are more than willing to wait for one to come serve them."
                        a "Oh, right..."
                        $ ahsokaExpression = 20
                        a "................."
                        $ ahsokaExpression = 8
                        a "Speaking from experience?"
                        y "Go to your cell."
                        "Grinning, Ahsoka takes her leave."
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 4:
                        $ ahsokaExpression = 7
                        a "Today was pretty fun."
                        a "The manager closed up shop near the end of the day and invited over some regulars to eat after closing time."
                        $ ahsokaExpression = 8
                        a "We er... We actually allow a little bit of gambling in the restaurant. Not for money of course, that'd be illigal."
                        y  "Then what do you bet with?"
                        a "Well obviously I don't gamble. But the patrons do."
                        a "They play strip-pazaak. Ever heard of it?"
                        y "Yeah, you play pazaak and the loser has to take off a piece of clothing."
                        a "Yeah exactly! But because most patrons are men, they instead bet with their favorite waitresses."
                        a "That way, even if they lose. They still get to see their favorite girls in their underwear."
                        a "Or... y'know... When the manager isn't looking... {w}{size=-6}In the nude~....{/size}"
                        y "Sounds like you don't mind losing those games."
                        a "{b}*Smirks*{/b} I don't!"
                        "The two of you spend a little more time chatting before Ahsoka leaves for her cell."
                        hide screen ahsoka_main
                        with d3
                        jump room
                   
            "Alright, you can go to your room.":
                   "Ahsoka nods and returns back to her cell."
                   hide screen scene_darkening
                   hide screen ahsoka_main
                   with d3
                   jump room

                   
                   
                   
################################################################################################
####################################### REPORT ZYGERRIA #######################################
################################################################################################
                   
label debriefZygerria:
    $ outfit = 8
    $ accessories = 1
    $ accessories3 = 0
    if timesWorkedPalace == 0:
        $ ahsokaExpression = 1
    else:
        $ ahsokaExpression = 21
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    a "I'm back from working at Mistress Nemtek's palace."
    $ timesWorkedPalace += 1
    $ ahsokaIgnore = 0
    if 6 <= ahsokaSlut <= 15:
        $ hypermatterRandom = renpy.random.randint(30, 35)
    if 16 <= ahsokaSlut <= 24:
        $ hypermatterRandom = renpy.random.randint(35, 39)
    if ahsokaSlut >= 25:
        $ hypermatterRandom = renpy.random.randint(39, 43)
    if 230 <= republicWinning <= 260:
        $ hypermatterRandom += 10
    if republicWinning >= 261:
        $ hypermatterRandom += 15
    $ hypermatter += hypermatterRandom
    a "I was paid [hypermatterRandom] Hypermatter."
        
    if timesWorkedPalace == 1:
        y "Welcome back, Ahs-"
        $ ahsokaExpression = 2
        a "I'm not going back!"
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        "Ahsoka rushes off to her cell."
        y "Well.... here we go again."
        scene black with fade
        scene bgCell01 with fade
        $ ahsokaExpression = 17
        show screen scene_darkening
        show screen ahsoka_main
        with d3
        y "So what happened?"
        $ ahsokaExpression = 4
        a "I was wrong. I am 'not' ready for this! It's-.... It's....!"
        a "This whole plan of yours is never going to work! Why did I even think we could pull this off?!"
        a "..................."
        y "Okay calm down. One step at a time."
        $ ahsokaExpression = 17
        a "{b}*Deep Breath*{/b} Okay...."
        $ ahsokaExpression = 1
        a "I thought the Lekka Lekku was bad with all the leering customers, but this is worse."
        a "I'm constantly surrounded by half naked girls, there are illigal narcotics and everyone just lays around doing nothing."
        y "(I like the sound of this place!)"
        $ ahsokaExpression = 12
        a "Not only that but the slavers are mean and unfair to the slaves."
        a "And for what? A tiny handful of Hypermatter?"
        $ ahsokaExpression = 4
        a "I really am trying, but I cannot control my emotions in a place like this."
        a "I see them shoving the slave girls around and I get angry. I get groped and can't help but get flustered."
        $ ahsokaExpression = 2
        a "Nemthak's palace is a den is sin and debauchery!"
        y "But is it making you more Hypermatter?"
        a "Well technically yes, but.....!"
        $ ahsokaExpression = 4
        a ".................."
        y "I know it's not much Ahsoka, but we're making some real progress here."
        y "You'll get used to it over time."
        $ ahsokaExpression = 13
        a "Hmph~...."
        hide screen ahsoka_main
        hide screen scene_darkening
        with d3
        "Ahsoka looks in a foul mood. You decide to leave her alone for now."
        scene black with fade
        scene bgBedroom with fade
        jump room
        
    if timesWorkedPalace == 5:
        $ ahsokaExpression = 1
        show screen ahsoka_main
        a "This is ridiculous!"
        a "Nemthak wanted me to entertain a Trandoshan guest of hers."
        $ ahsokaExpression = 5
        a "So I walk up to him. Bring him some wine and sit down on his lap."
        $ ahsokaExpression = 4
        a "As I lean into him to feed him his grapes, I feel his.... his-....!"
        a "{size=-6}His manhood{/size}."
        y "I'm sorry?"
        $ ahsokaExpression = 13
        a "I... I felt his manhood through his pants."
        y "I guess you must've been doing a good job then."
        $ ahsokaExpression = 2
        a "But then when I got up to bring a new jar of wine he suddenly pulled me back down and pulled out his.... his member!"
        a "It was disgusting! I panicked and pushed myself away, falling to the ground."
        a "Then the whole room turned to me and started laughing....."
        $ ahsokaExpression = 5
        a "I'm done doing this work, [playerName]. I'm not going back."
        y "Because of one slip up? You only just got started!"
        $ ahsokaExpression = 13
        a "But surely there's another job that can get us Hypermatter, right?"
        y "Ahsoka, Hypermatter is being rationed by the Republic and the Separatists. The only way we're going to make Hypermatter is by doing favors."
        y "And guess what. Painting somebody's fence or fixing some roof tiles isn't going to make us enough Hypermatter to repair the station."
        y "This job is just a stepping stone. See it as a training for your future jobs."
        $ ahsokaExpression = 16
        a "Future jobs?"
        y "Well look at how much they're paying to see you dance and fondle you a bit."
        y "Imagine what they'd pay if you went a step further!"
        $ ahsokaExpression = 6
        a "!!!"
        a "Further?! I'm not going further than this!"
        $ ahsokaExpression = 2
        a "Listen here, [playerName]. I've already betrayed the Jedi Code and I don't plan to disgrace myself or the Jedi order anymore than I already have!"
        y "But you're doing so well!"
        y "We made a whole bunch of Hypermatter and its finally starting to look as if we can repair some sectors."
        $ ahsokaExpression = 4
        a "But I hate doing this sort of work...."
        a "Can't we raid some Separatist ships and steal their Hypermatter or something?"
        y "Stealing? That doesn't sound very Jedi like."
        $ ahsokaExpression = 11
        a "Only from Separatist ships!"
        y "That doesn't matter. If we start raiding, then either we run into authorities or we risk alienating potential contacts."
        y "Like it or not, doing this sort of work will make us the most Hypermatter."
        $ ahsokaExpression = 20
        a "............"
        a "Okay....{w} I'll continue working there. For the Republic."
        y "Good girl."
        "Ahsoka takes her leave and returns to her cell."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        jump room
        
    if timesWorkedPalace == 10:
        show screen ahsoka_main
        with d3
        y "Enjoyed yourself at Nemthak's palace?"
        $ ahsokaExpression = 20
        a "Very funny...."
        y "Is it really that bad?"
        a "...................."
        $ ahsokaExpression = 12
        a "I guess not~..."
        a "Despite the slaves being treated badly, I've actually seen different households where they're treated even worse."
        a "All things considering, Nemthak actually takes pretty good care of her slaves."
        a "Still... {w}I never expected to end up here... in a slave harem...."
        y "I guess we'll continue looking around for something new then."
        y "But remember that Nemthak pays pretty well."
        $ ahsokaExpression = 20
        a "I know, I know~...."
        y "Also, I want you to keep training. The better you get, the more we earn from your jobs."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        jump room
        
    if timesWorkedPalace >= 12 and mission6 == 0 and ahsokaSlut >= 15:                                 #Start mission6
        show screen ahsoka_main
        with d3
        $ mission6 = 1
        $ pitGirlJob = True
        $ ahsokaExpression = 22
        $ mission6PlaceholderText = "Ahsoka has found a new pet-project. She's helping a podracer out on Tatooine, but she will need a Pit Girl Uniform first. Visit the tailor on Geonosis and send her to work on Tatooine."
        a "Have you ever been to Tatooine?"
        y "Tatooine? Yeah, more times than I care to admit. Why?"
        $ ahsokaExpression = 12
        a "Well Nemthak had me deliver a package there today."
        a "When I arrived in Mos Isley I heard about these races that are being held there."
        y "The Pod Races?"
        $ ahsokaExpression = 9
        a "That's right!"
        y "I didn't know you were into those."
        a "Yes, my Master used to race those when he was still a slave, but that's not why I brought it up!"
        a "I met someone named Meehra, a racer who needs a mechanic."
        a "The racers do a lot of betting and he said he would share half of his earnings with me if I became his pit-girl!"
        $ ahsokaExpression = 29
        a "Can we please do that instead of working at the Lekku or at Nemthak's palace next time?"
        y "You know that if he loses we won't get anything right?"
        $ ahsokaExpression = 9
        a "I do... but he seems to know what he's doing."
        y "It sounds to me that you just don't want to do your other jobs anymore."
        $ ahsokaExpression = 7
        a "..................."
        y "But all right, I'm willing to give it a shot. If you stop making Hypermatter however, I'm gonna start sending you back to your other jobs."
        $ ahsokaExpression = 9
        a "Deal!"
        $ ahsokaExpression = 21
        a "..................."
        y "Yes~....?"
        $ ahsokaExpression = 18
        a "Well I am suppose to wear a Pit Girl Uniform. Could you buy one for me?"
        y "I'll see what I can do."
        $ mission6Title = "{color=#f6b60a}Mission: 'Grease Monkey' (Stage: I){/color}"
        $ mission6PlaceholderText = "Ahsoka found a new job working at a Pitgirl on Tatooine. You're willing to give it a shot to see if she can turn it profitable.\n \nDon't forget to buy her a Pitgirl outfit from Geonosis."
        play sound "audio/sfx/quest.mp3"
        "{color=#f6b60a}Mission: 'Grease Monkey' (Stage: I){/color}"
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        jump room
        
        
    else:
        menu:
            "Tell me about your day":
                if 6 <= ahsokaSlut <= 15:     # Hates working for Nemthak
                    $ randomWork = renpy.random.randint(1, 5)
                    if randomWork == 1:
                        show screen ahsoka_main
                        a "I don't understand why people use narcotics."
                        a "They smoke these weird pipes at Nemthak's palace. After they do, they become all mellow and relaxed."
                        y "And that is a bad thing?"
                        a "Of course it is! How can you stay focused and alert if you're lying half dead on a couch?"
                        y "Not everyone is a Jedi. A lot of people like to lay back and relax. Not having to worry about anything."
                        a "Even if it means wasting an entire day doing nothing?"
                        y "Especially if it means wasting an entire day doing nothing!"
                        a "............"
                        "Ahsoka takes her leave and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        show screen ahsoka_main
                        a "I... I don't want to talk about it."
                        y "What happened?"
                        a ".................."
                        a "Nemthak told me to entertain her guards today."
                        a "I had to stand up on a podium and dance as they cheered. At first everything went all right."
                        a "But then they started shouting for me to strip!"
                        y "And did you?"
                        a "What?! No! Of course not!"
                        y "............."
                        a "I refuse to strip naked for a group of barbarians like that! I still have some dignity you know!"
                        y "Yes. Half naked table dancers are known for their dignity."
                        a "Like it or not, I am still a Jedi and I refuse to dance naked for some savages to gawk at!"
                        a "Eventually they just got bored and left."
                        a "{b}*Shrugs*{/b}"
                        "Ahsoka turns around and leaves for her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 3:
                        show screen ahsoka_main
                        $ ahsokaExpression = 13
                        a "Erm...."
                        $ ahsokaExpression = 5
                        a "Today I had to entertain Nemthak."
                        y "What did she ask you to do?"
                        a "She wanted me and another slave girl to dance together...."
                        $ ahsokaExpression = 13
                        a "During the dancing the girl slipped her hands underneath my dress and starting gropping me."
                        y "Now we're talking! Tell me more."
                        $ ahsokaExpression = 6
                        a "More? Oh erm.... well I kinda backed off quickly."
                        y "................."
                        a "It took me a little by surprise.... I didn't think she'd put her hands underneath my dress....."
                        y "People are going to start fonding you underneath your clothes sooner or later, y'know."
                        $ ahsokaExpression = 2
                        a "They better not! Groping me is one thing, but I draw the line there!"
                        $ ahsokaExpression = 5
                        a "Don't worry, it won't effect my job preformance. Next time someone tries, I'll politely, but firmly explain them to respect my personal boundries."
                        y "....................."
                        "Ahsoka takes her leave and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 4:
                        show screen ahsoka_main
                        $ ahsokaExpression = 12
                        a "Nemthak asked me for a lapdance today."
                        y "Really? How did that go?"
                        $ ahsokaExpression = 20
                        a "I guess it went pretty well.... I did the usual rolling of my hips and grinding into her."
                        a "Apperantly she had never has a Togruta dance for her before."
                        a "She seemed to really enjoy it and...."
                        a "................"
                        y "And?"
                        $ ahsokaExpression = 12
                        a "When I turned around she slapped my behind...."
                        a "I was going to protest, but after what I went through at the Lekka Lekku I decided to just ignore it."
                        $ ahsokaExpression = 5
                        a "She seemed pleased by that and demanded that I'd join her on her bed."
                        a "Luckily one of her guests showed up and I was told to go make tea."
                        y "She really seems to like you, doesn't she?"
                        $ ahsokaExpression = 19
                        a "......................"
                        "Ahsoka takes her leave and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 5:
                        $ mood -= 5
                        $ ahsokaExpression = 4
                        show screen ahsoka_main
                        a "Nemthak paraded me around like a trophy today...."
                        a "Pretty much as soon as I arrived on the planet, she grabbed my leash and janked me through the city."
                        a "I hate having to wear such a revealing dress, but having to wear such a revealing dress {i}'and'{/i} be seen by thousands of people was horrible!"
                        a "It felt as if every person on Zygerria was looking at me. It was a nightmare!"
                        a "I tried to protest, but she said she'd pay me extra today and.... and...."
                        a "And for some reason I said okay!"
                        a "I must've been seen by at least a hundred people. They pointed at me and..... {b}*sniff*{/b}"
                        a "And Nemthak said I should enjoy all the attention I was getting."
                        "Ahsoka looks incredibly upset."
                        a "Here's the extra Hypermatter.... {b}*sniff*{/b} I'm going back to my cell."
                        $ mood -= 10
                        $ hypermatterRandom = renpy.random.randint(10, 15)
                        $ hypermatter += hypermatterRandom
                        "Ahsoka hands you an extra [hypermatterRandom] Hypermatter."
                        "Ahsoka sulks out of the room and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 6:
                        show screen ahsoka_main
                        a "Nemthak asked me to flirt with one of her guests today, which was actually kinda fun."
                        a "I placed my hand on my hip and gave him a naughty smile just like in the holovids."
                        a "And he just looks at me and goes: 'Have you read {i}'That a Lightsaber in Your Pocket by Kit Brinny'{/i}?"
                        a "And I'm just dumb founded! We both started laughing and actually had a really good time."
                        a "Did you know that she wrote a total of three holotapes? I sudder to think what she recorded in those other two."
                        "Ahsoka takes her leave and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                        
                if 16 <= ahsokaSlut <= 22:     # Neutral working for Nemthak
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        a "Today wasn't too bad."
                        a "Lady Nemthak asked me to repair some of her dresses that had gotten torn."
                        a "Her clothes are almost all made out of expensive fabrics."
                        a "The Jedi don't pay us so we almost never get to buy nice clothes for ourselves."
                        "Ahsoka takes her leave and returns to her cell."
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        jump room
                    if randomWork == 2:
                        a "Nemthak had friends over today."
                        a "Apperantly she wanted to show me off and told me to feed and entertain them."
                        y "And you did?"
                        a "Well yes. I fed them grapes and served Zeltros Wine. One of them was nice enough to offer me a glass as well."
                        a "It actually tasted really nice."
                        $ mood += 2
                        hide screen scene_darkening
                        hide screen ahsoka_main
                        with d3
                        "Ahsoka takes her leave and returns to her cell."
                        jump room
                    if randomWork == 3:
                        $ mood += 5
                        $ ahsokaExpression = 12
                        a "Today was... {i}'acceptable...'{/i}"
                        y "Acceptable?"
                        $ ahsokaExpression = 20
                        a "................."
                        $ ahsokaExpression = 27
                        a "{size=-6}It was kind of fun...{/size}"
                        y "Fun? What did you guys do?"
                        a "We ehrm... we went to a spa."
                        y "A spa?"
                        $ ahsokaExpression = 9
                        a "Yes... she said that she was very happy how her slaves had been behaving and decided to bring everyone for a day of relaxation."
                        $ ahsokaExpression = 27
                        a "I... I never went to a spa before."
                        y "Let me guess, there is a dumb Jedi rule about not going to spas?"
                        $ ahsokaExpression = 12
                        a  "Of course not! It's just not something I've ever done."
                        a "Lady Nemthak is strange... Sometimes she is so nice to us, while other time she's a huge pain... sometimes literally."
                        y "She's a unique character for sure. All right, head back to your room. You spend all day relaxing, so I'll make sure to work you extra hard tomorrow."
                        $ ahsokaExpression = 9
                        a "{b}*Laughs*{/b}"
                        $ ahsokaExpression = 21
                        a "....................."
                        $ ahsokaExpression = 14
                        a "Wait, are you serious?"
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                    if randomWork == 4:
                        y "You look tired."
                        $ ahsokaExpression = 18
                        a "Mhm? It shows...?"
                        a "{b}*Yawns*{/b} Nemthak had a party and two of her slave girls had gotten sick. Meaning the rest of us had to work twice as hard."
                        a "And as you'd expect at one of {i}'her'{/i} parties, we didn't stayed robed for very long."
                        y "Oh! Tell me more!"
                        $ ahsokaExpression = 13
                        a "Do I have to....? I kinda just wanna head to bed."
                        menu:
                            "Tell me":
                                $ ahsokaExpression = 17
                                a "............."
                                a "{b}*Sighs*{/b} Fine. The party was named 'Orange Pillow Party' which ought to give away its theme..."
                                $ ahsokaExpression = 4
                                a "And its star player..."
                                y "Oh boy."
                                $ ahsokaExpression = 12
                                a "Yeah... so I spend most of the day dancing center stage. After the crowd was nice and rowed up, she made me serve drinks to the guests."
                                $ ahsokaExpression = 4
                                a "I can still feel their hands all over my body! I must have been groped by a hundred men and women!"
                                y "Sounds like you had fun."
                                $ ahsokaExpression = 19
                                a "..................."
                                y "Correction. Sounds like the guests had fun."
                                $ ahsokaExpression = 4
                                a "{b}*Groans*{/b} Can I please be excused now?"
                                "You grin and send her off to bed."
                            "Fine, good night":
                                $ ahsokaExpression = 12
                                a "Thanks... Good night."
                                pass
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                        
                if ahsokaSlut >= 23:     # Happy working for Nemthak
                    $ randomWork = renpy.random.randint(1, 4)
                    if randomWork == 1:
                        $ ahsokaExpression = 11
                        a "Did you know she has a menagerie?!"
                        y "Lady Nemthak?"
                        a "Yes! I had to shovel hay, mud and who knows what else all day!"
                        y "Nobody said being a slave would be fun, Ahso-...."
                        $ ahsokaExpression = 9
                        a "Are you kidding me? I had a great time!"
                        y "Oh... you had?"
                        a "I'd rather work hard and get dirty doing some actual work, rather than serve drinks and lie around doing nothing."
                        a "Plus, I got to see all these majestic animals. I'm sorta hoping I get to do it again next time."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                    if randomWork == 2:
                        a "It was very pleasantly warm on Zygerria today. "
                        a "We got to lie on the large balcony overlooking the city, while the male slaves waved large palmleafs at us for breeze."
                        y "And lady Nemthak pays you for that?"
                        a "Well... I mean we had to do {i}'some'{/i} work. We danced and served her drinks. Other than that not much happened."
                        y "Well as long as she keeps paying us Hypermatter, you won't hear me complaining."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                    if randomWork == 3:
                        $ ahsokaExpression = 9
                        a "Today was fun! I got to give a striptease for a very influential entertainer on Zygerria."
                        a "She was scouting for new and exotic dance moves and Lady Nemthak volunteered her slaves to show off what they got."
                        $ ahsokaExpression = 23
                        a "She was very impressed with me and after the preformance took me aside and er..."
                        $ ahsokaExpression = 7
                        a "I may have been a bit too enthousiastic."
                        y "Okay...?"
                        a "During my striptease, I ended up naked on her lap as she nervously looked up at me and asked if she could touch me."
                        a "She was so shy! I gave her permission and she gently explored me all over."
                        a "When the show was over, she left with the biggest smile on her face and Lady Nemthak told me I did a good job."
                        y "I see that you get up to all kinda of fun out there. I'm glad to hear that things are working out."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                    if randomWork == 4:
                        $ ahsokaExpression = 20
                        a "Lady Nemthak has a new slave girl and I had to teach her how to behave."
                        a "At first I felt bad seeing another girl pulled from her home and forced into slavery, but...."
                        y "But...?"
                        $ ahsokaExpression = 18
                        a "This girl was something else... She was taken from a very religious household where any kind of fun or luxory was strictly forbidden."
                        $ ahsokaExpression = 9
                        a "Despite being a slave, she'd never been this free before and seemed to love every minute of it."
                        a "We danced together, shopped with Lady Nemthak for clothes and even got to sample some of her wine."
                        $ ahsokaExpression = 5
                        a "The hard part was getting her naked."
                        y "I imagine so, if she came from such a sheltered household."
                        $ ahsokaExpression = 27
                        a "She reminded me a bit of myself when I first arrived there~..."
                        hide screen ahsoka_main
                        hide screen scene_darkening
                        with d3
                        jump room
                        
            "Alright, you can go to your room.":
                "Ahsoka takes her leave and returns to her cell."
                hide screen scene_darkening
                hide screen ahsoka_main
                with d3
                jump room
        
        
################################################################################################
####################################### REPORT TATOOINE #######################################
################################################################################################

label debriefTatooine:
    $ ahsokaExpression = 21
    $ outfit = 11
    a "I'm back from working at the Tatooine race track."
    $ timesWorkedRacer += 1
    $ ahsokaIgnore = 0
    
    if ahsokaSlut <= 15:
        $ hypermatterRandom = renpy.random.randint(1, 1)
    if 16 <= ahsokaSlut <= 24:
        $ hypermatterRandom = renpy.random.randint(43, 53)
    if 25 <= ahsokaSlut <= 35:
        $ hypermatterRandom = renpy.random.randint(53, 63)
    if ahsokaSlut >= 35:
        $ hypermatterRandom = renpy.random.randint(63, 73)
    if 230 <= republicWinning <= 260:
        $ hypermatterRandom += 10
    if republicWinning >= 261:
        $ hypermatterRandom += 15
    $ hypermatter += hypermatterRandom

    
    if 1 <= mission6 <= 6:
        jump mission6                                                   #quest 6 in quest.rpy.
        
    if mission6 == 7:
        $ mission6 = 8
        $ mission6PlaceholderText = "Ahsoka has begun applying her training on the Tatooine race track."
        $ hypermatter += 150
        $ ahsokaExpression = 17
        show screen scene_darkening 
        show screen ahsoka_main
        with d3
        y "Welcome back, how did things go on Tatooine?"
        $ ahsokaExpression = 5
        a "Well...."
        a "I... started applying my training...."
        $ ahsokaExpression = 17
        a "..............................."
        y "And?"
        $ ahsokaExpression = 4
        a "Meehra came third...."
        y "He did? That's great news! How much did you make?"
        $ ahsokaExpression = 12
        a "150 hypermatter."
        y "I guess I was wrong about this job after all. It seems there is money to be made here."
        a "I know it seems like a lot, but Meehra said something about new girls always bringing in a high profit.."
        a "Future races probably won't make quite as much."
        a "More than working at the other jobs though."
        y "So what did you have to do to get it?"
        $ ahsokaExpression = 17
        a "................................."
        a "{b}*Sighs*{/b}"
        $ ahsokaExpression = 13
        a "I had to work topless today...."
        y "That doesn't sound too bad."
        $ ahsokaExpression = 18
        a "......................"
        if ahsokaSocial <= 23:
            $ ahsokaExpression = 13
            a "And later I...."
            y "Yes?"
            $ ahsokaExpression = 18
            a "....................."
            $ ahsokaExpression = 20
            a "I don't really wanna talk about it...."
            $ ahsokaExpression = 4
            a "If you need me, I'll be in my cell."
            "Ahsoka hangs her head and leaves for the detention area."
        if ahsokaSocial >= 24:
            $ ahsokaExpression = 13
            a "I...."
            a "I also had to jerk off two of the other racers."
            if ahsokaSlut >= 21:
                y "Well we practiced that.  How did it go?"
                $ ahsokaExpression = 20
                a "{b}*Shrugs*{/b} I dunno. It's pretty much what I expected. I tried my best, but I don't think they were too impressed."
                $ ahsokaExpression = 12
                a "One of them didn't even cum and they both sorta laughed at me while I was sitting on the floor."
                $ ahsokaExpression = 20
                a ".............................."
                $ ahsokaExpression = 12
                a "But they did let Meehra pass in the race, so I guess they were happy enough."
                "Ahsoka looks down at her hands with a disappointed frown on her face."
                a "If you need me, I'll be in my cell."
            if ahsokaSlut <= 20:
                y "We haven't gotten around to practising that yet.... how did it go?"
                $ ahsokaExpression = 4
                a "What do you think? It went terribly!"
                $ ahsokaExpression = 2
                a "I didn't know what I was doing and all the while they were laughing at me!"
                y "Then what happened?"
                $ ahsokaExpression = 4
                a "......................................"
                $ ahsokaExpression = 17
                a "I was thinking of just getting up and walking out, but...."
                a "I knew I couldn't just run from my problems, so I suffered through it. In the end neither of them came."
                $ ahsokaExpression = 18
                a "But I guess they might've liked me because I saw them let Meehra pass on the race track."
                $ ahsokaExpression = 4
                a "I'll just add this one to my growing list of horrible shameful moments and deal with it."
                y "It's a bit of a rough start, but you'll get better at it."
                $ ahsokaExpression = 20
                "Ahsoka looks a little unsure, but shrugs."
                a "I'm heading back to my cell. Night."
        "Ahsoka is slowly taking things to the next step. She will now apply her training to her job as a Pit Girl."
        $ mission6Title = "{color=#f6b60a}Mission: 'Grease Monkey' (Complete){/color}"
        $ mission6PlaceholderText = "{color=#f6b60a}Mission: 'Grease Monkey' (Complete):{/color} \nAhsoka has starter her new job as a Pitgirl! She can now be send out to Tatooine to work."
        play sound "audio/sfx/questComplete.mp3"
        "{color=#f6b60a}Mission: 'Grease Monkey' (Complete){/color}"
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        jump room
            
    if mission6 == 8:
        if timesWorkedRacer == 25 and ahsokaSlut >= 28:
            show screen scene_darkening
            show screen ahsoka_main
            with d3
            $ ahsokaExpression = 9
            a "There was a parade celebrating the podracers today called the Balaan Voot"
            y "Oh that was today?! I love watching those."
            $ ahsokaExpression = 8
            a "I bet you do! {b}*Chuckles*{/b} Well as you know, it's customairy for a pitgirl to accompany their racer topless as they move through the city."
            a "So that's what we did. All the girls took off their tops and sat on the podracers as they glided through the city."
            a "Tickets to podraces usually cost two golden peggats so most slaves never get to see them. Whole crowds of slaves, townfolk and tourists came out to see the spectacle."
            y "And you enjoyed it?"
            $ ahsokaExpression = 20
            a "Well... I mean...."
            $ ahsokaExpression = 12
            a "I... I guess exposing myself isn't as big of a deal anymore now a days... with all the other stuff going on."
            y "I'm glad to hear it. Shame I missed the parade though."
            $ ahsokaExpression = 9
            a "Well.... there's always next year!"
            "You and Ahsoka talk about the parade for a bit longer before she heads back to her cell."
            hide screen ahsoka_main
            hide screen scene_darkening
            with d3
            jump room
        else:
            $ ahsokaExpression = 20
            show screen scene_darkening
            show screen ahsoka_main
            with d3
            a "Today we won! I got [hypermatterRandom] Hypermatter."
            menu:
                "Tell me about your day":
                    if 16 <= ahsokaSlut <= 26:     # Hates working for Meehra
                        $ randomWork = renpy.random.randint(1, 5)
                        if randomWork == 1:
                            $ ahsokaExpression = 4
                            a "This is awful, it's even worse then working at Nemthak's palace!"
                            a "I have to do the most embarrassing things and even then Meehra usually only comes third or second."
                            $ ahsokaExpression = 2
                            a "Today I had to go show myself off to some new racers, completely naked!"
                            y "Could be worse I suppose."
                            $ ahsokaExpression = 10
                            a "Yeah, but still!"
                            a  "I had to strip down naked in the middle of the hangerbay and walk over to their workshop, all the while people were whistling at me."
                            a "If they knew I was a Jedi....!"
                            y "But they don't."
                            $ ahsokaExpression = 4
                            a "No... at least that's good. {w}Every race I worry that someone in the audience will recognise me. Luckily they're all too focussed on the races."
                            $ ahsokaExpression = 5
                            a "They don't allow the pit-girls to {i}'distract'{/i} the races outside of the workshops to avoid races crashing into the spectators."
                            y "Just think of-...."
                            $ ahsokaExpression = 20
                            a "The fate of the Republic. I know, I know."
                            a "I'm gonna head back to my cell now."
                            "Ahsoka takes her leave."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                            
                        if randomWork == 2:
                            $ ahsokaExpression = 20
                            a "Another day working my butt off in that disgusting workshop."
                            y "The racers giving you trouble?"
                            $ ahsokaExpression = 12
                            a "Hm? Oh no. Not today."
                            a "I meant it's just a filthy place. There's sand and engine spills everywhere. I don't think that place has been cleaned since it was built."
                            y "Not much point to it. The engine exhausts alone would smudge the place each time they'd come back from a race."
                            a "I guess so. Still, I could really use a shower right about now."
                            "Ahsoka leaves for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 3:
                            $ ahsokaExpression = 6
                            a "Ew ew ew! I had to give a handjob today!"
                            y "Gotta practise those aswell."
                            $ ahsokaExpression = 11
                            a "No you don't understand. I don't like giving handjobs to strangers, but I especially don't like giving handjobs to strangers who leave a mess!"
                            a "This guy was hung like a bantha! Apperantly they call him 'The Hoze'."
                            y "I think I can see where this is going."
                            a "Yeah...~ Luckily he came quickly but...{w}THERE WAS SO MUCH!"
                            $ ahsokaExpression = 13
                            a "It was sticky and... and...!"
                            "Ahsoka shudders."
                            a "I'm going to grab another shower. I'm pretty sure it's still in places I didn't even knew I had."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 4:
                            $ ahsokaExpression = 29
                            $ ahsokaBlush = 1
                            with d3
                            a "..........................."
                            y "Yes~.....?"
                            a "I had to masturbate in front of a client today....."
                            y "Really? How did that go?"
                            $ ahsokaExpression = 28
                            a "Well... I don't know. How'd you expect I guess."
                            a "I pulled down my pants, sat down on my butt and split my legs."
                            a "He seemed really impressed and started jerking himself off."
                            $ ahsokaExpression = 10
                            a "It... it wouldn't have been so bad if it hadn't been for those girls..."
                            y "Girls?"
                            $ ahsokaExpression = 12
                            a "Yeah... while I was busy, two pit-girls walked by and when they saw me they started giggling and pointing."
                            $ ahsokaExpression = 29
                            a "Do I.... do I look... weird down there?"
                            y "Weird?"
                            a "You know... different from other girls?"
                            y "You've never seen what other girls look like naked?"
                            a "........................"
                            y "Don't worry. You don't look different from other girls."
                            a "Okay...."
                            "Ahsoka turns around and heads back to her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            $ ahsokaBlush = 0
                            jump room
                        if randomWork == 5:
                            $ ahsokaExpression = 20
                            a "Today was strange...."
                            y "How so?"
                            $ ahsokaExpression = 12
                            a "Well I accidentally walked in on two pit-girls.... erm... {w} making out with each other."
                            a "I figured they were doing a favor for another racer or something, but there was no one else around."
                            y "Maybe they just liked each other?"
                            $ ahsokaExpression = 15
                            a "What...? Don't be silly, two girls can't like each other in that way."
                            a "After I caught them, they just started blushing and quickly ran outside."
                            y "Sounds to me like those two had a thing for each other and you crashed their party."
                            $ ahsokaExpression = 19
                            a "Stop saying that. Girls can't have feelings for one another."
                            y "Why not?"
                            $ ahsokaExpression = 6
                            a "Huh..? Oh, I don't know..."
                            a "I mean, that's just not something that happens, right?"
                            y "There are no lesbian Jedi?"
                            $ ahsokaExpression = 14
                            a "What's a lesbian?"
                            y "........................."
                            y "I'll take that as a no."
                            $ ahsokaExpression = 12
                            a "{b}*Shrugs*{/b} Can I go back to my cell now?"
                            "You nod and Ahsoka turns around and leaves for her room."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                            
                    if 26 <= ahsokaSlut <= 36:     # Neutral working for Meehra
                        $ randomWork = renpy.random.randint(1, 6)
                        if randomWork == 1:
                            $ ahsokaExpression = 10
                            a "I... I got into a fight with another pit-girl today."
                            y "What happened?"
                            $ ahsokaExpression = 12
                            a "Well... turns out this one racer that I was bribing. His pit-girl is also his wife."
                            $ ahsokaExpression = 4
                            a "Someone made a slip up and told her that I had been making moves on him and she got angry."
                            $ ahsokaExpression = 11
                            a "Walked over to our workshop and tried hitting me with an exaust pipe!"
                            y "She didn't hurt you, did she?"
                            $ ahsokaExpression = 20
                            a "Pfff, are you kidding? I've been trained by the best Jedi in the galaxy, she didn't even get close."
                            $ ahsokaExpression = 12
                            a "Although... I didn't want to blow my cover either, so I just pretended to run away."
                            a "When I got back I saw her fighting with her husband. She only threw me glares after that."
                            y "Sounds like quite the eventful day."
                            $ ahsokaExpression = 4
                            a "Yeah... I'm heading back to my room now."
                            "After saying goodbye, Ahsoka leaves for her room."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 2:
                            $ ahsokaBlush = 1
                            $ ahsokaExpression = 20
                            a "I had to do something really embarrassing today...."
                            y "Even more embarrassing than usual?"
                            a "Well...."
                            a "A group of Sand People had blocked off the track with boulders and guards were send out to clear the rubble."
                            $ ahsokaExpression = 12
                            a "But in the meantime the spectators started getting bored, so they had all pit-girls come outside to entertain the crowd."
                            y "Entertain them how?"
                            $ ahsokaExpression = 18
                            a "Do you really need to ask?"
                            $ ahsokaExpression = 12
                            a "First off they had us take our tops off which the audience loved."
                            a "Then they brought out the firehozes and sprayed the track, creating a mud bath on the track."
                            y "I like where this is going...."
                            $ ahsokaExpression = 5
                            a "Then all the girls took turns mud wrestling with each other! It was crazy!"
                            y "I hope you didn't destroy them too hard."
                            $ ahsokaExpression = 8
                            a "{b}*Chuckles*{/b} Well I did get really into it. I won my first two fights and the crowd started cheering me on."
                            a "You'd win the wrestle match if you managed to steal the other girl's pants and I wasn't going to expose myself to five-thousand people."
                            $ ahsokaExpression = 7
                            a "So I won all matches until the girls started teaming up against me."
                            a "Luckily the Sand People had been cleared out by that point and the race resumed so we all had to rush back inside the hangers."
                            y "That sounds like fun. I wish I could've seen it!"
                            $ ahsokaExpression = 20
                            a ".............................................."
                            $ ahsokaExpression = 8
                            a "I guess.... today was kinda fun."
                            "Ahsoka smiles and heads back to her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 3:
                            $ ahsokaExpression = 4
                            a "I got covered in mud today."
                            y "Oh?"
                            a "Yeah... a racer had used a high pressure hoze to clean his dirty pod and caused a puddle of mud to form around his pod-racer."
                            a "When I walked by he revved up his engines and the blast wave caused the mud to shoot upwards, covering me."
                            $ ahsokaExpression = 19
                            a "He of course started laughing."
                            a "Besides that... not much happened today."
                            $ ahsokaExpression = 12
                            "Ahsoka leaves for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 4:
                            $ ahsokaExpression = 11
                            a "I'm exhausted."
                            $ ahsokaExpression = 4
                            a "This one racer invited a whole group of his friends to the workshops and had me jerk them off."
                            $ ahsokaExpression = 13
                            a "After that a cartel showed up, something about it being one of the guys' birthday, and the track manager wanted me to give them a blowjob."
                            $ ahsokaExpression = 17
                            a "Once that was done and I wanted to start working on the pod, another one of the racers came up to me."
                            $ ahsokaExpression = 12
                            a "He complaining about how Meehra was suppose to have taken a fall in the last race, but hadn't."
                            a "So I had to strip naked and let him grope me to even the score."
                            a "Finally, Meehra seems to have made some debts and a loanshark showed up at our workshop."
                            $ ahsokaExpression = 6
                            a "I had to flirt with him and let him feel me up so he wouldn't break Meehra's legs. After that I could finally begin with the pod maintenance."
                            a "I'm pooped!"
                            y "Sounds like you had a rough day. Head back to your room and turn in early tonight."
                            "Ahsoka nods and leaves for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 5:
                            $ ahsokaExpression = 11
                            a "I was send home early today!"
                            a "Some big shot new racer showed up with a dozen pit-girls and convinced pretty much every other racer to take a fall."
                            $ ahsokaExpression = 20
                            a "Meehra came third so we still won some Hypermatter, but other than that. It was a very boring race."
                            y "A new racer? That could cause competition..."
                            $ ahsokaExpression = 21
                            a "Nah it's fine. He's a rich kid and will probably get bored with racing sooner or later."
                            a "Not only that, but after the race, one of the other pilots came up to me and said he still preferred me over the new girls."
                            $ ahsokaExpression = 9
                            a "It was actually kind of nice to hear him say that."
                            y "Seems like you're making a reputation for yourself out there."
                            "Ahsoka nods and turns around, leaving for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 6:
                            $ ahsokaExpression = 6
                            a "They made me entertain one of their sponsors!"
                            a "This one racer kept losing on purpose in return for favors from the pit-girls. His manager got fed up with him and demanded an explanation."
                            a "So the racer introduced him the girls and asked me to..."
                            y "To...?"
                            $ ahsokaExpression = 20
                            a "To give him a buttjob...."
                            y "And you did?"
                            $ ahsokaExpression = 12
                            a "Yes.... I did...."
                            y "And was he pleased?"
                            $ ahsokaExpression = 19
                            a "Well {i}'he'{/i} was having a whale of a time."
                            a "Told his racer that he completely understood why he took the falls and even mentioned coming back to keep an eye on his investment in the future."
                            y "Sounds like that worked out well then!"
                            $ ahsokaExpression = 14
                            a "And there I was.... sitting there, with my butt in the air...."
                            a "I tried to keep cool, but I didn't know what to do! I just... I just sorta...."
                            a "I just sorta tried my best... I guess...."
                            $ ahsokaExpression = 29
                            a "I just felt very used afterwards...."
                            a "There's a good side to the story though."
                            y "Oh?"
                            $ ahsokaExpression = 9
                            a "Yeah, the racer was really happy that I saved his skin and came to look me up after the race. He gave me some of his spare Hypermatter as a 'thank you'."
                            $ hypermatter += 25
                            play sound "audio/sfx/itemGot.mp3"
                            "Ahsoka hands you an extra {color=#FFE653}25 hypermatter{/color}."
                            y "See! Your hard work is paying off!"
                            a "I guess.... {w}I'm going to grab a shower now..."
                            "Ahsoka leaves for her room."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                            
                    if ahsokaSlut >= 37:            # Happy working for Meehra
                        $ randomWork = renpy.random.randint(1, 6)
                        if randomWork == 1:
                            $ ahsokaExpression = 15
                            a "Well something new happened today."
                            a "Someone is making a holo calender with all the pit-girls posing in suggestive poses."
                            a "I was asked to bend over Meehra's podracer, exposing my naked butt."
                            y "And did you?"
                            $ ahsokaExpression = 9
                            a "Of course. He promised us extra credits, which I used to buy Hypermatter. Here it is..."
                            play sound "audio/sfx/itemGot.mp3"
                            $ hypermatter += 10
                            "Ahsoka hands you an extra  {color=#FFE653}10 Hypermatter!{/color}"
                            y "But I mean... aren't you afraid someone is going to see this calender and recognise you?"
                            $ ahsokaExpression = 11
                            a "!!!"
                            a "I hadn't even thought about that!"
                            $ ahsokaExpression = 4
                            a "Oh crap, I shouldn't have done that. I'm an idiot!"
                            y "Calm down, pod-racing is illigal in the Republic. That calender probably won't be read by any Jedi anytime soon."
                            $ ahsokaExpression = 10
                            a "Yeah... I guess you're right~..."
                            $ ahsokaExpression = 12
                            a "That would've been embarrassing. Anyways that's about all that happened today."
                            "Ahsoka leaves for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 2:
                            $ ahsokaExpression = 9
                            a "Oh! Yes, I have to tell you! I gave {i}'the'{/i} best blowjob today!"
                            y "Woah, you sound excited about that."
                            a "No, no wait listen. It gets better."
                            a "While I was giving the blowjob, another man came up to me and had me do a buttjob as well!"
                            y "Multitasking. I like it."
                            a "Uhuh! It went really well! Some of the girls showed up to take notes."
                            $ ahsokaBlush = 1
                            $ ahsokaExpression = 7
                            with d3
                            a "They actually thought I was doing great heh... heh~..."
                            "With a blush on her face, Ahsoka returns to her room."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            $ ahsokaBlush = 0
                            jump room
                        if randomWork == 3:                                                                                                                     # covered in cum.
                            $ ahsokaExpression = 9
                            hide screen ahsoka_main
                            with d2
                            $ ahsokaBodyCum = 2
                            show screen ahsoka_main
                            with d3
                            a "Today was great! Meehra finished first place!"
                            $ ahsokaExpression = 7
                            a "Granted... I had to do a 'lot' of slutting around for it..."
                            y "I can see that!"
                            $ ahsokaExpression = 9
                            a "He was super happy and said I was the best pit-girl he ever had! Said he'd buy me something nice sometime."
                            $ ahsokaExpression = 7
                            a "I mean... I doubt he will. He'll probably gamble all his money away, but it's the thought that counts."
                            y "Next time ask him for more Hypermatter."
                            $ ahsokaExpression = 23
                            a "Hah! Yeah right, like that's going to happen."
                            "Ahsoka notices the cum stains on her uniform."
                            $ ahsokaExpression = 11
                            a "Oh...! I should probably go clean up!"
                            y "Probably."
                            "Ahsoka leaves for her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            $ ahsokaBodyCum = 0
                            jump room
                        if randomWork == 4:
                            $ ahsokaExpression = 11
                            a "I actually had to give a blowjob today...."
                            $ ahsokaExpression = 9
                            a "Which was no problem, of course."
                            a "He was a big shot, with a very fancy podracer. It could easily beat all other racers and most got real nervous."
                            a "Luckily he wasn't above being bribed and most girls rushed over to entertain him, but he demanded that two of us gave him a blowjob...."
                            a "Most girls refused... except for this one human girl.... and Meehra said I didn't have to, but...."
                            $ ahsokaExpression = 8
                            a "I remembered I said I'd do anything for the Republic so... {w}I decided to volunteer."
                            $ ahsokaExpression = 7
                            a "He seemed very pleased with that.... He pushed us to our knees and held on to my horns as he.... well y'know."
                            y "This is your first double blowjob, isn't it? How did it go?"
                            $ ahsokaExpression = 12
                            a "Well... he was super rough with us. Taking turns between me and her."
                            a "When he was done, I had gotten all light headed, much to his amusement."
                            $ ahsokaExpression = 9
                            a "But he stayed true to his word and actually allowed Meehra to finish first place. Which was a big deal."
                            a "So many people had bet on the new guy that the pay-out rate sky rocketed."
                            y "Really? And did you get to see any of that pay-out?"
                            $ ahsokaExpression = 20
                            a "No.... some important part of Meehra's engine broke during the track and he said he had to replace it."
                            y "....................."
                            $ ahsokaExpression = 12
                            a "Yeah.... I felt a little bitter about that too."
                            y "Well, better luck next time. Head to your room, you deserve your rest."
                            "Ahsoka nods and walks off to her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 5:
                            $ ahsokaExpression = 9
                            a "Nothing special happened today."
                            y "Nothing at all?"
                            $ ahsokaExpression = 20
                            a "{b}*Ponders*{/b} Hmmm.... nope. Same old really."
                            $ ahsokaExpression = 9
                            a "Didn't even have to bribe anyone today, Meehra won by himself."
                            y "It's nice to have a calm day every now and again."
                            "Ahsoka nods and makes her way back to her cell."
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                        if randomWork == 6:
                            $ ahsokaExpression = 9
                            a "{b}*Chew* *Chew*{/b}"
                            y "Whatcha got there?"
                            a "Mhm... Bubblegum."
                            a "Mandora says it can be used to repair the podracers, which doesn't sound too safe to me."
                            a "But apperantly this stuff will stick to anything. Naboo  pilots use it aswell. That's why they call it a Naboo Repair Kit."
                            a "Here have some."
                            $ gum += 1
                            $ foundryItem6 = "Naboo Repair Kit - 25 Hypermatter"
                            play sound "audio/sfx/itemGot.mp3"
                            "You found {color=#FFE653}Naboo Repair Kit{/color} x1!"
                            hide screen ahsoka_main
                            hide screen scene_darkening
                            with d3
                            jump room
                            
                "Alright, you can go to your room":
                   "Ahsoka takes her leave and returns to her cell."
                   hide screen ahsoka_main
                   hide screen scene_darkening
                   with d3
                   jump room
                   
                   
################################################################################################
#################################### LIGHTSABER MISSIONS ######################################
################################################################################################
define LSMissionJobReport = 0
define LSMissionsJobReport = 0

label LSMissionJobReport:
    pause 0.5
    show screen scene_darkening
    with d3
    $ LSMission = 0
    if LSMissionJobReport == 1:
        $ ahsokaExpression = 21
        show screen ahsoka_main
        with d3
        a "I'm back from hunting the Exogorth on Geonosis."
        $ randomExogroth = renpy.random.randint(1, 2)
        if randomExogroth == 1:
            $ ahsokaExpression = 9
            $ hypermatter += 200
            play sound "audio/sfx/itemGot.mp3"
            a "It went really well! I got the beast and sold the pearls for {color=#FFE653}200 hypermatter{/color}!"
            y "Well done. You can return to your cell."
        if randomExogroth == 2:
            $ ahsokaHealth -= 1
            $ ahsokaExpression = 13
            $ hypermatter += 200
            y "You okay there?"
            a "Yeah I'm fine... I got a nasty bite and it's been hurting ever since."
            play sound "audio/sfx/itemGot.mp3"
            $ ahsokaExpression = 9
            a "However I did manage to slay it in the end and sold the pearls for {color=#FFE653}200 hypermatter{/color}."
            y "All right, well done padawan."
            "(Ahsoka looks hurt. Maybe you should put her in the Kolto tank or buy her a health stim.)"
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        $ LSMissionJobReport = 0
        jump room
        
    if LSMissionJobReport == 2:
        $ ahsokaExpression = 21
        show screen ahsoka_main
        with d3
        a "I'm back from Christophsis."
        $ randomDepot = renpy.random.randint(1, 2)
        if randomDepot == 1:
            $ mood += 5
            $ ahsokaExpression = 9
            $ hypermatter += 200
            a "It was a great success! I got to beat up some clankers and managed to extract a few containers before reinforcements showed up."
            play sound "audio/sfx/itemGot.mp3"
            a "All in all, I earned a good {color=#FFE653}200 hypermatter{/color}!"
            y "Not bad at all! All right, head back to your room for now."
        if randomDepot == 2:
            $ ahsokaHealth -= 1
            $ ahsokaExpression = 13
            $ mood += 5
            $ hypermatter += 200
            a "The depot was better guarded than I thought."
            y "You look hurt!"
            a "Heh~... Yeah I got hit by a blaster shot. Worth it though, it's been too long since I fought the CIS."
            play sound "audio/sfx/itemGot.mp3"
            $ ahsokaExpression = 9
            a "And all things considering, I still got away with {color=#FFE653}200 hypermatter{/color}!"
            y "Not bad Ahsoka, head back to your cell for now. You've earned your rest."
            "(Ahsoka looks hurt. Maybe you should put her in the Kolto tank or buy her a health stim.)"
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        $ LSMissionJobReport = 0
        jump room
        
    if LSMissionJobReport == 3:
        $ ahsokaExpression = 21
        show screen ahsoka_main
        with d3
        a "I'm back from Zygerria."
        $ randomZygerria = renpy.random.randint(1, 2)
        if randomZygerria == 1:
            $ ahsokaExpression = 9
            $ hypermatter += 200
            a "Guy never knew what hit him! Managed to swoop in, steal some expensive looking equipment and got out before the guard arrived!"
            play sound "audio/sfx/itemGot.mp3"
            a "All in all, I earned a good {color=#FFE653}200 hypermatter{/color}!"
        if randomZygerria == 2:
            $ ahsokaHealth -= 1
            $ ahsokaExpression = 13
            $ hypermatter += 200
            a "H-hey..."
            y "Ahsoka?"
            a "Sorry... Got hurt while on the mission. But! Still got the goods, so mission success!"
            play sound "audio/sfx/itemGot.mp3"
            a "I manage to sell the guy's tools for {color=#FFE653}200 hypermatter{/color}."
            y "Well done Ahsoka. Head to bed, it looks like you could use the rest."
            "(Ahsoka looks hurt. Maybe you should put her in the Kolto tank or buy her a health stim.)"
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        $ LSMissionJobReport = 0
        jump room
        
    if LSMissionJobReport == 4:
        $ ahsokaExpression = 21
        show screen ahsoka_main
        with d3
        a "I'm back from Mandalore."
        $ randomMandalore = renpy.random.randint(1, 2)
        if randomMandalore == 1:
            $ ahsokaExpression = 9
            $ hypermatter += 200
            a "Wow, these guys are extreme! I only just managed to get everyone out of there alive, but it was a close one."
            play sound "audio/sfx/itemGot.mp3"
            a "Luckily, I still managed to smuggle {color=#FFE653}200 hypermatter{/color} out!"
            $ droidRandomChance = renpy.random.randint(1, 2)
            if droidRandomChance == 1:
                $ actionFigure += 1
                a "Not only that, but I found some interesting Mandalorian artifacts that I traded with a museum owner on Mandalore."
                a "He gave me this."
                play sound "audio/sfx/itemGot.mp3"
                "Ahsoka hands you a {color=#FFE653}Astromech Action Figure{/color}."
            else:
                pass
            y "Well done! Let's hope all missions are such a success in the future."
            "Ahsoka nods and takes her leave."
        if randomMandalore == 2:
            $ ahsokaExpression = 13
            $ ahsokaHealth -= 1
            $ hypermatter += 200
            a "Death Watch is crazy..."
            y "Hm?"
            a "These guys don't kid around. They're as extreme as extremists go. I only barely got all the hostages out savely."
            y "Did you get hurt?"
            a "........."
            a "A little, but not before smuggling out some goods."
            play sound "audio/sfx/itemGot.mp3"
            a "I'd say I got about {color=#FFE653}200 hypermatter{/color} out of there before anyone noticed."
            y "All right, head back to your room for now."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        $ LSMissionJobReport = 0
        jump room

                       
                       
################################################################################################
######################################## REPORT KOLTO ########################################
################################################################################################
define koltoDays = 0

label debriefKoltoHeal:
    if 0 <= koltoDays <= 3:
        $ koltoDays += 1
        show screen scene_darkening
        with d3
        "Ahsoka is still recovering in the Kolto Tank."
        pause 0.5
        hide screen scene_darkening
        with d3
        scene bgBedroom
        jump room
        
    if koltoDays == 4:    
        show screen scene_darkening
        with d3
        show screen ahsoka_main
        with d3
        $ mood += 5
        $ ahsokaIgnore = 0
        $ koltoDays = 0
        a "I'm back from the Kolto Tank."
        y "How are you feeling?"
        a "Refreshed! I feel much better now."
        y "I'm glad to hear it."
        "Ahsoka returns to her cell."
        hide screen scene_darkening
        hide screen ahsoka_main
        with d3
        scene bgBedroom
        jump room
        
label debriefKoltoAddict:
    show screen scene_darkening
    with d3
    show screen ahsoka_main
    with d3
    $ ahsokaIgnore = 0
    a "I'm back from the Kolto Tank."
    a "I feel much better."
    "Addictions have been removed."
    pause 0.1
    "Ahsoka returns to her cell."
    hide screen scene_darkening
    hide screen ahsoka_main
    with d3
    scene bgBedroom
    jump room
